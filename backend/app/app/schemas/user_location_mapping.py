from typing import Optional,List
from fastapi.param_functions import File
from pydantic import  EmailStr, BaseModel, ValidationError, validator

from fastapi import UploadFile, HTTPException
from datetime import datetime

# from schemas.location import ViewLocation

class MappingBase(BaseModel):
    user_id:int
    location_id:List[int]
    
    @validator('location_id')
    def user_type_validate(cls, v):
        if v == [] or v == [0]:
            raise HTTPException(
                status_code=400,
                detail="Location is required!",
            )
        return v
    access_type:int
    @validator('access_type')
    def access_type_validate(cls, v):
        if v == 1 or v == 2:
            return v
        else:
            raise HTTPException(
                status_code=400,
                detail="select valid type",
            )

class CreateMapping(MappingBase):
    pass
class CreateMappingLocation(BaseModel):
    user_id:int
    access_type:int
class ListMapping(BaseModel):
    mapping_id:int
    location_id:int
    location_code:str
    location_name:str
    username:str
    mobile_no:int
    status:int
    user_id:int
    access_type:int



class UpdateMapping(MappingBase):
    pass
