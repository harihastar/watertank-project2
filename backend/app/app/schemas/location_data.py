from typing import Optional, List
from pydantic import BaseModel, EmailStr
# import datetime.datetime
from datetime import date, datetime, time


class LocationData(BaseModel):
    location_data_id:str
    location_id: str
    power_voltage:float
    power_status: str
    waterlevel_meter: float
    watervolume_liter: float
    pump_status: int
    valve_status: str
    battery_voltage: str
    battery_status: str
    battery_alarm: str
    pump_running_hrs: float
    pump_running_hrs_day: int
    discharged_water_volume: float
    signal_strength: float
    pump1_failure_status: str
    pump2_failure_status: str
    pump1_failure_alarm: str
    pump2_failure_alarm: str
    valve_failure_status: str
    valve_alarm: str
    mode: str
    running_beyond_time_alarm: str
    created_at:str
    status:int

    
class ListLocationData(BaseModel):

    location_data_id:int
    location_id:int
    location_code:str
    location_name:str
    power_voltage_l1:str
    power_voltage_l2:str
    power_voltage_l3:str
    power_status:int
    water_level_meter:str
    water_volume_liter:str
    pump_status:int
    battery_voltage:str
    valve_status:int
    pump_running_hrs:str
    pump_running_hrs_day:str
    running_pump:int
    discharged_water_volume:str
    discharged_water_volume_in_day:str
    signal_strength:int
    pump1_failure_status:int
    pump2_failure_status:int
    valve_open_fail:int
    valve_close_fail:int
    mode:int
    battery_status:int
    running_beyond_set_time:str
    pump1_waterflow_per_min:str
    pump2_waterflow_per_min:str
    allowed_running_hrs:str
    low_water_level:str
    high_water_level:str
    preset_water_level:str
    pump1_failure_alarm:int
    pump2_failure_alarm:int
    valve_open_failure_alarm:int
    valve_close_failure_alarm:int
    battery_low_alarm:int
    pump_run_beyond_running_hours_alarm:int
    created_at:str
    data_date:str
    data_time:str
    status:int



    

