from typing import Optional, List
from pydantic import BaseModel, EmailStr
# import datetime.datetime
from datetime import datetime


class LocationSetting(BaseModel):
    location_id: int
    pump1_waterflow_per_minutes: int
    pump2_waterflow_per_minutes: int
    allowed_running_hours:str
    low_water_level:str
    high_water_level:str
    preset_water_level:str
    remote_pump_contact_no:str
    email:Optional[str]
    
class ListLocationSetting(LocationSetting):
    id:int
    created_at:str
    status:int

