from typing import Optional, List
from pydantic import BaseModel, EmailStr
# import datetime.datetime
from datetime import datetime


class Login(BaseModel):
    access_token: str
    token_type: str
    user_type: int
    username: Optional[str]
    user_id: int
    
    # popup_message: Optional[str]


class OtpMsg(BaseModel):
    msg: str
    reset_key: str


class VerifyOtp(BaseModel):
    otp: int
    reset_key: str


class Flash(BaseModel):
    popup_message: Optional[str]
    flash_message: Optional[str]


class ResetPassword(VerifyOtp):
    password:str