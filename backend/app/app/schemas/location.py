from typing import Mapping, Optional, List
from pydantic import BaseModel, EmailStr,validator
from datetime import datetime
from fastapi import HTTPException
from .user_location_mapping import CreateMappingLocation
from .location_data import LocationData,ListLocationData

class CreateLocation(BaseModel):
    location_code: str
    name: str
    time_frequency:int
    tank_type: int
    radius:Optional[str]
    height:Optional[str]
    length:Optional[str]
    width:Optional[str]
    scheme_name:Optional[str]
    district:Optional[str]
    block:Optional[str]
    panchayat:Optional[str]
    ear_marked_qty:Optional[int]
    field_1:Optional[str]
    field_2:Optional[str]
    field_3:Optional[str]



    # mapping:Optional[CreateMappingLocation]

    
    @validator('radius')
    def radius_validate(cls, v, values, **kwargs):

        if 'tank_type' in values and values['tank_type'] == 1 and (v == 0 or v == ""):
            raise HTTPException(
                status_code=400,
                detail="Radius field is required",
            )

        return v
    
    @validator('height')
    def height_validate(cls, v, values, **kwargs):

        if 'tank_type' in values and (values['tank_type'] == 1 or values['tank_type'] == 2) and (v == 0 or v == None):
            raise HTTPException(
                status_code=400,
                detail="Height field is required",
            )
            
        return v

    
    @validator('length')
    def length_validate(cls, v, values, **kwargs):

        if 'tank_type' in values and values['tank_type'] == 2 and (v == 0 or v == None):
            raise HTTPException(
                status_code=400,
                detail="Length field is required",
            )

        return v
    
    @validator('width')
    def width_validate(cls, v, values, **kwargs):

        if 'tank_type' in values and values['tank_type'] == 2 and (v == 0 or v == None):
            raise HTTPException(
                status_code=400,
                detail="Width field is required",
            )

        return v

class UpdateLocation(CreateLocation):
    pass

class ListLocationDrop(BaseModel):
    location_id:int
    location_code: str
    name: str

class ListLocation(BaseModel):
    location_id:int
    location_code: str
    name: str
    time_frequency:str
    access_type:Optional[int]
    tank_type: int
    radius:Optional[float]
    height:Optional[float]
    length:Optional[float] 
    width:Optional[float] 
    last_data:Optional[str]
    capacity:Optional[float]
    created_at:str
    status:int
    scheme_name:Optional[str]
    district:Optional[str]
    block:Optional[str]
    panchayat:Optional[str]
    ear_marked_qty:Optional[int]
    field_1:Optional[str]
    field_2:Optional[str]
    field_3:Optional[str]



class ViewLocationUser(BaseModel):
    mapping_id:int
    user_id:int
    username:str
    user_type:int
    access_type:int
    created_at:str

class ViewLocation(CreateLocation):
    location_id:int
    last_data:Optional[str]
    capacity:Optional[float]
    created_at:str
    status:int
    access_type:Optional[int]
    tank_capacity:Optional[str]
    last_data:Optional[str]
    capacity:Optional[int]
    users:List[ViewLocationUser]
    pump_control_data:Optional[ListLocationData]
    time_diff:Optional[str]
class ViewByLocationUser(CreateLocation):
    location_id:int
    created_at:str
    status:int

class ViewByMap(ViewByLocationUser):
    mapping_id:Optional[int]

class ViewByLocation(ViewByMap):
    access_type:Optional[int]


class ViewLocationUser(BaseModel):
    user_id:int
    mapping_id:int
    username:str
    mobile_no:str
    email:Optional[str]
    user_type:int
    access_type:int
    created_at:str
    status:int

class ViewByLocationUser(ViewLocation):
    User:Optional[List[ViewLocationUser]]

    class Config:
        orm_mode=True



    