from typing import Optional,List
from fastapi.param_functions import File
from pydantic import  EmailStr, BaseModel, ValidationError, validator

from fastapi import UploadFile, HTTPException
from datetime import datetime
import re
from .location import ViewLocation,ViewByLocation

class UserBase(BaseModel):
    username:str
    email:Optional[str]
    mobile_no:str
    user_type:int

    @validator('user_type')
    def user_type_validate(cls, v):
    
        if v != 2 and v != 3:
            raise HTTPException(
                status_code=400,
                detail="Invalid User Type",
            )
        return v

    @validator('mobile_no')
    def mobile_no_validate(cls, v):
        try:
            isinstance(int(v), int)
        except:
            raise HTTPException(
                status_code=400,
                detail="Please enter a valid mobile number",
            )
        
        if len(v) != 10:
            raise HTTPException(
                status_code=400,
                detail="Mobile number must be 10 digit",
            )
                
        return v

    @validator('email')
    def email_validate(cls, v):
        regex = r'\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b'               
        if v =="":
            return None
        
        else: 
            if(re.fullmatch(regex, v)):
                return v
 
            else:
                raise HTTPException(
                status_code=400,
                detail="Invalid email",
            )


# Properties to receive via API on creation
class UserCreate(UserBase):
   password: str



class UserUpdate(UserBase):
    pass
    

class ListUserDrop(BaseModel):
    user_id:int
    username:str

class ListUser(BaseModel):
    user_id:int
    username:str
    mobile_no:str
    user_type:int
    status:int


class ViewUser(BaseModel):
    user_id:int
    username:str
    mobile_no:str
    email:Optional[str]
    user_type:int
    created_at:str
    status:int

class ViewUserLocation(ViewUser):
    access_type:int
    
class ViewByUser(ViewUser):
    Location:Optional[List[ViewByLocation]]

    class Config:
        orm_mode=True

class ChangePassword(BaseModel):
    current_password: str
    new_password:str
    confirm_password: str


    @validator('confirm_password')
    def passwords_match(cls, v, values, **kwargs):
        if 'new_password' in values and v != values['new_password']:
            raise HTTPException(
                status_code=400,
                detail="Password doesn't match",
            )
            
        return v

    @validator('new_password')
    def new_password_validate(cls, v, values, **kwargs):
        if 'current_password' in values and v == values['current_password']:
            raise HTTPException(
                status_code=400,
                detail="Current password and new password cannot be same",
            )
            
            
        return v

