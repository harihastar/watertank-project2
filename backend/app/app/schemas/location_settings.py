from typing import Optional, List
from fastapi.openapi.models import Operation
from pydantic import BaseModel, validator
from fastapi import HTTPException

from datetime import datetime, time




class TimeSettings(BaseModel):
    from_time:str
    to_time:str

class UpdateTimeSetting(BaseModel):
    time_setting_id:Optional[int]
    from_time:str
    to_time:str

class Setting(BaseModel):
    pump1_waterflow_per_min:float
    pump2_waterflow_per_min: float
    allowed_running_hrs: time
    

    low_water_level: float
    preset_water_level: float
    high_water_level: float
    remote_contact_no: str
    lpp:Optional[float]
    amps1:Optional[float]
    amps2:Optional[float]
    contact_email: Optional[str]

class CreateSetting(Setting):
    time_setting: List[TimeSettings]

    @validator('allowed_running_hrs')
    def allowed_running_hrs_validate(cls, v):
        
        if v =="00:00:00":
            raise HTTPException(
                status_code=400,
                detail="Enter Allowed running hrs",
            )
        
        else: 
            return v
    @validator('remote_contact_no')
    def mobile_no_validate(cls, v):
        try:
            isinstance(int(v), int)
        except:
            raise HTTPException(
                status_code=400,
                detail="Please enter a valid contact no.",
            )
        
        if len(v) != 10:
            raise HTTPException(
                status_code=400,
                detail="Contact no. must be 10 digit",
            )
                
        return v

    @validator('contact_email')
    def email_validate(cls, v):
        
        if v =="":
            return None
        
        else: 
            return v

    @validator('low_water_level')
    def low_water_level_validate(cls, v, values, **kwargs):

        if 'tank_type' in values and values['tank_type'] == 1 and (v == 0 or v == ""):
            raise HTTPException(
                status_code=400,
                detail="Radius field is required",
            )

        return v
    
    @validator('preset_water_level')
    def preset_water_level_validate(cls, v, values, **kwargs):

        if 'low_water_level' in values and values['low_water_level'] > v:
            raise HTTPException(
                status_code=400,
                detail="Preset_water_level should be greater than low water level",
            )
            
        return v

    
    @validator('high_water_level')
    def high_water_level_validate(cls, v, values, **kwargs):

        if 'low_water_level' in values and 'preset_water_level' in values and values['low_water_level'] < values['preset_water_level']  < v:
            return v

        else:
            raise HTTPException(
                status_code=400,
                detail="High water level should be greater than preset water level",
            )

    

class UpdateSetting(Setting):
    time_setting: List[UpdateTimeSetting]
    @validator('allowed_running_hrs')
    def allowed_running_hrs_validate(cls, v):
        print(str(v))
        if str(v) =="00:00:00":
            raise HTTPException(
                status_code=400,
                detail="Enter Allowed running hrs",
            )
        
        else: 
            return v
    @validator('remote_contact_no')
    def mobile_no_validate(cls, v):
        try:
            isinstance(int(v), int)
        except:
            raise HTTPException(
                status_code=400,
                detail="Please enter a valid contact no.",
            )
        
        if len(v) != 10:
            raise HTTPException(
                status_code=400,
                detail="Contact no. must be 10 digit",
            )
                
        return v

    @validator('contact_email')
    def email_validate(cls, v):
        
        if v =="":
            return None
        
        else: 
            return v

    @validator('low_water_level')
    def low_water_level_validate(cls, v, values, **kwargs):

        if 'tank_type' in values and values['tank_type'] == 1 and (v == 0 or v == ""):
            raise HTTPException(
                status_code=400,
                detail="Radius field is required",
            )

        return v
    
    @validator('preset_water_level')
    def preset_water_level_validate(cls, v, values, **kwargs):

        if 'low_water_level' in values and values['low_water_level'] > v:
            raise HTTPException(
                status_code=400,
                detail="Preset_water_level should be greater than low water level",
            )
            
        return v

    
    @validator('high_water_level')
    def high_water_level_validate(cls, v, values, **kwargs):

        if 'low_water_level' in values and 'preset_water_level' in values and values['low_water_level'] < values['preset_water_level']  < v:
            return v

        else:
            raise HTTPException(
                status_code=400,
                detail="High water level should be greater than preset water level",
            )
