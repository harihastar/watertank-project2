from datetime import datetime
from typing import Any, Dict, Optional, Union

from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import update
from fastapi import HTTPException
from app.core.security import get_password_hash, verify_password
from app.crud.base import CRUDBase
from app.models import User
from app.core.config import settings
from app.schemas.user import UserCreate, UserUpdate
from app.utils import profile_image_upload
import random


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    def get_by_email(self, db: Session, *, email: str):
        return db.query(User).filter(User.email == email, User.status !=-1,User.email!=None).first()

    def get_by_username(self, db: Session, *, username: str):
        return db.query(User).filter(User.username == username, User.status !=-1).first()

    def get_by_mobile(self, db: Session, *, mobile_no: str):
        return db.query(User).filter(User.mobile_no == mobile_no, User.status !=-1).first()
    
    def get_by_email_update(self, db: Session, *, email: str, user_id:int):
        return db.query(User).filter(User.email != None, User.email == email, User.status !=-1, User.id != user_id).first()


    def get_by_mobile_update(self, db: Session, *, mobile_no: str, user_id:int):
        return db.query(User).filter(User.mobile_no == mobile_no, User.status !=-1, User.id != user_id).first()


    def create(self, db: Session, *, obj_in: UserCreate):
        db_obj = User(
            user_type=obj_in['user_type'],
            username=obj_in['username'],
            email=obj_in['email'],
            mobile_no=obj_in['mobile_no'],
            password=get_password_hash(obj_in['password']),
            created_at=datetime.now(settings.tz_NY),
            status=1
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)


        
        return db_obj

    def update(
        self, db: Session, *, db_obj: User, obj_in: Union[UserUpdate, Dict[str, Any]]
    ) -> User:
        if isinstance(obj_in, dict):
            update_data = obj_in
        else: 
            update_data = obj_in.dict(exclude_unset=True)


        user = super().update(db, db_obj=db_obj, obj_in=update_data)


        db.commit()

        return user

    def authenticate(self, db: Session, *, mobile: str, password: str) -> Optional[User]:
        user = self.get_by_email(db, mobile=mobile)
        return user
        if not user:
            return None
        if not verify_password(password, user.password):
            return None
        return user

    def is_active(self, user: User):
        if user.status != -1:
            return "Invalid user"

    def is_superuser(self, user: User):
        if user.user_type == 1:
            return user
        else:
            return "Invalid request"
        


user = CRUDUser(User)
