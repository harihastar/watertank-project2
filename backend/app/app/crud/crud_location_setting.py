from typing import Any, Dict, Optional, Union
from sqlalchemy.orm import Session
from fastapi import HTTPException
from fastapi.encoders import jsonable_encoder
from app.crud.base import CRUDBase
from app import models
from app.models import LocationSetting, LocationSettingLog, TimeSetting, TimeSettingLog
from app.schemas import CreateSetting, TimeSettings
from datetime import datetime, time
from app.core.config import settings


class CRUDReading(CRUDBase[models.LocationSetting, LocationSetting, CreateSetting]):  
    def create(self, db: Session, *, location_id: int,  current_user:int ,location_data: CreateSetting, time_data: Any):
        
        location_in = jsonable_encoder(location_data)
        # Insert into Location Setting
        db_obj = self.model(**location_in,created_by =current_user, location_id = location_id, status = 1, created_at=datetime.now(settings.tz_NY))
        db.add(db_obj)  
        db.commit() 
        # db.refresh(db_obj)


        # Insert into Location Setting Log
        if db_obj:
            setting_log = LocationSettingLog(new_low_water_level=db_obj.low_water_level,
                                            new_high_water_level=db_obj.high_water_level,
                                            new_allowed_running_hrs=db_obj.allowed_running_hrs,
                                            new_preset_water_level=db_obj.preset_water_level,
                                            new_remote_contact_no=db_obj.remote_contact_no,
                                            new_contact_email=db_obj.contact_email,
                                            new_pump1_waterflow_per_min=db_obj.pump1_waterflow_per_min,
                                            new_pump2_waterflow_per_min=db_obj.pump2_waterflow_per_min,
                                            created_by=current_user,location_id = location_id, location_setting_id = db_obj.id, status = 1, created_at=datetime.now(settings.tz_NY))
            db.add(setting_log)        
            db.commit()

            for i in time_data:
                time_obj = TimeSetting(location_id=location_id, **i, created_by=current_user,created_at=datetime.now(settings.tz_NY), status = 1)
                db.add(time_obj)
                db.commit() 
                # db.refresh(time_obj)   

                if time_obj:  
                    time_set_log = TimeSettingLog(new_from_time=time_obj.from_time,
                                                    new_to_time=time_obj.to_time,

                                                created_by=current_user,created_at=datetime.now(settings.tz_NY), status = 1, location_id = location_id, time_setting_id  = time_obj.id)
                    db.add(time_set_log)
                    db.commit() 

                else:
                    raise HTTPException(
                        status_code=400,
                        detail="Something went wrong",
                    )

            db.refresh(db_obj)
        else:
            raise HTTPException(
                status_code=400,
                detail="Something went wrong",
            )

        return db_obj


    # def log_create(self, db: Session, *, location_id: int,  location_data: CreateSetting, time_data: TimeSetting):
        
    #     location_in = jsonable_encoder(location_data)
    #     db_obj = self.model(**location_in, location_id = location_id, status = 1, created_at=datetime.now(settings.tz_NY))

    #     # time_set = jsonable_encoder(time_data)
    #     for i in time_set:
    #         time_obj = models.TimeSetting(**i, created_at=datetime.now(settings.tz_NY), status = 1, location_id = 1)
    #         db.add(time_obj)
    #         db.commit()        
    #     db.add(db_obj)        
    #     db.commit()
    #     db.refresh(db_obj)
    #     return db_obj

location_setting = CRUDReading(LocationSetting)