from typing import Any, Dict, Optional, Union

from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import update
from fastapi import HTTPException
from app.core.security import get_password_hash, verify_password
from app.crud.base import CRUDBase
from app.models import Customer,User
from app.schemas.customer import CustomerCreate,CustomerUpdate
from app.utils import profile_image_upload
from app.api import deps
import random
import datetime
from app.core.config import settings

class CRUDCustomer(CRUDBase[Customer, CustomerCreate, CustomerUpdate]):
    def get_by_email(self, db: Session, *, email: str):
        return db.query(User).filter(User.email != None, User.email == email, User.status !=-1).first()

    # def get_by_username(self, db: Session, *, username: str):
    #     return db.query(User).filter(User.username == username, User.status !=-1).first()

    def get_by_mobile(self, db: Session, *, mobile_no: str):
        return db.query(User).filter(User.mobile_no == mobile_no, User.status !=-1).first()


    def get_by_email_update(self, db: Session, *, email: str, user_id:int):
        return db.query(User).filter(User.email != None, User.email == email, User.status !=-1, User.id != user_id).first()


    def get_by_mobile_update(self, db: Session, *, mobile_no: str, user_id:int):
        return db.query(User).filter(User.mobile_no == mobile_no, User.status !=-1, User.id != user_id).first()


    def create(self, db: Session, *, obj_in: CustomerCreate, mobile):
        otp = None
        otp2 = None
        created_at = None
        expire_at = None
        message = ""
        reset_key =None
        otp_verified_status = 1
        status = -1
        if mobile == 1:
            otp_verified_status = 0
            status = -1
            otp2 = ""
            otp, reset , created_at, expire_time, expire_at, otp_valid_upto = deps.get_otp()

            otp2 = "1234"
            message = f"""Hi, \n\n{otp2} is the OTP for mobile verification. \n\n Note : This OTP is valid upto {otp_valid_upto} \n\n Regards, \n Administration team, \n Water level Montitoring."""
            reset_key = f'{reset}H3SfAO4jR87uI@'

        db_obj = User(
            user_type=2,
            username=obj_in['customer_name'],
            email=obj_in['email'],
            mobile_no=obj_in['mobile_no'],
            password=get_password_hash(obj_in['password']),
            created_at=datetime.datetime.now(settings.tz_NY),
            otp_created_at= created_at,
            otp_expired_at= expire_at,
            reset_key=reset_key,
            otp=otp2,
            status=-1,
            otp_verified_status =otp_verified_status
        )
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)
        
        db_obj_customer = Customer(
            customer_name=obj_in['customer_name'],
            user_id=db_obj.id,
            email=obj_in['email'],
            mobile_no=obj_in['mobile_no'],
            created_at=datetime.datetime.now(settings.tz_NY),
            status=-1
        )
        db.add(db_obj_customer)
        db.commit()
        db.query(User).filter(User.id == db_obj.id).update({'status' : 1})
        if mobile == 0:
            db.query(Customer).filter(Customer.user_id == db_obj.id).update({'status' : 1})

        
        db.commit()
        
        return db_obj_customer, message, reset_key


    def resend_otp(self,db:Session,id:int):
        otp = ''
        reset = ""
        characters = '0123456789'
        reset_character = 'qwertyuioplkjhgfdsazxcvbnm0123456789QWERTYUIOPLKJHGFDSAZXCVBNM'
        
        for i in range(0, 4):
            otp += characters[random.randint(0, len(characters) - 1)]
        for j in range(0, 20):
            reset += reset_character[random.randint(
                0, len(reset_character) - 1)]

        current_time = datetime.datetime.now(settings.tz_NY)
        created_at = current_time.strftime("%Y-%m-%d %H:%M:%S")
        expire_time = current_time + datetime.timedelta(minutes=5)
        expire_at = expire_time.strftime("%Y-%m-%d %H:%M:%S")
        otp_valid_upto = expire_time.strftime("%d-%m-%Y %I:%M %p")
        message = f"""Hi, \n\n{otp} is the OTP for mobile verification. \n\n Note : This OTP is valid upto {otp_valid_upto} \n\n Regards, \n Administration team, \n Water level Montitoring."""
        reset_key = f'{reset}H3SfAO4jR87uI@'
        
        user = db.query(User).filter(User.id == id).first()
        # Update value 
        user.otp=otp
        user.reset_key=reset_key
        user.otp_created_at=created_at
        user.otp_expired_at=expire_at
        db.commit()
        return user, message, reset_key
        # return "Resend-OTP send to your registered mobile number"


    # def update(
    #     self, db: Session, *, db_obj: User, obj_in: Union[UserUpdate, Dict[str, Any]]
    # ) -> User:
    #     if isinstance(obj_in, dict):
    #         update_data = obj_in
    #     else: 
    #         update_data = obj_in.dict(exclude_unset=True)


    #     user = super().update(db, db_obj=db_obj, obj_in=update_data)


    #     db.commit()

    #     return user

    # def authenticate(self, db: Session, *, mobile: str, password: str) -> Optional[User]:
    #     user = self.get_by_email(db, mobile=mobile)
    #     return user
    #     if not user:
    #         return None
    #     if not verify_password(password, user.password):
    #         return None
    #     return user

    # def is_active(self, user: User):
    #     if user.status != -1:
    #         return "Invalid user"

    # def is_superuser(self, user: User):
    #     if user.user_type == 1:
    #         return user
    #     else:
    #         return "Invalid request"
        


customer = CRUDCustomer(Customer)
