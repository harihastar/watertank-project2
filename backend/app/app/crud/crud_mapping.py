from typing import Any, Dict, Optional, Union
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.core.security import get_password_hash, verify_password
from app.crud.base import CRUDBase
from app.models import UserLocationMapping,UserLocationMappingLog
from datetime import datetime
from app.schemas.user_location_mapping import CreateMapping,UpdateMapping
import random
from app.core.config import settings

class CRUDMapping(CRUDBase[UserLocationMapping, CreateMapping, UpdateMapping]):  
    

    def create(self, db: Session, *, obj_in: CreateMapping,current_user:int):
        # print(obj_in)
        location=obj_in["location_id"]
        for row in location:

            db_obj = UserLocationMapping(
                user_id=obj_in['user_id'],
                location_id=row,
                access_type=obj_in['access_type'],
                created_at=datetime.now(settings.tz_NY),
                created_by=current_user,
                status=1
            )
            
            db.add(db_obj)
            db.commit()
            db.refresh(db_obj)
            db_obj_log = UserLocationMappingLog(
                user_location_mapping_id=db_obj.id,
                new_user_id=obj_in['user_id'],
                new_location_id=row,
                new_access_type=obj_in['access_type'],
                created_at=datetime.now(settings.tz_NY),
                created_by=current_user,
                message="created",
                status=1
            )
            db.add(db_obj_log)
            db.commit()
            db.refresh(db_obj_log)

        return db_obj

user_location_mapping = CRUDMapping(UserLocationMapping)