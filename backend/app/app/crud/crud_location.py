from typing import Any, Dict, Optional, Union
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app.core.security import get_password_hash, verify_password
from app.crud.base import CRUDBase
from app.models import Location
from datetime import datetime
from app.schemas.location import CreateLocation, UpdateLocation
import random
from app.core.config import settings

class CRUDLocation(CRUDBase[Location, CreateLocation, UpdateLocation]):  
    def check_location(self,db: Session,code:str):
        return db.query(Location).filter(Location.location_code == code,Location.status != -1).first()
    
    def update(self,db: Session,*,
        db_obj: Location,
        obj_in: Union[UpdateLocation, Dict[str, Any]],current_user:int):

        obj_data = jsonable_encoder(db_obj)
        
        
        if isinstance(obj_in, dict):
            update_data = obj_in
        
        else:
            update_data = obj_in.dict(exclude_unset=True)
        
        update_data["updated_by"]=current_user
        update_data["updated_at"]=datetime.now(settings.tz_NY)
        
        update_location = super().update(db, db_obj=db_obj, obj_in=update_data)
       
        db.commit()
        
        
        return update_location

location = CRUDLocation(Location)