import secrets
from typing import Any, Dict, List, Optional, Union

from pydantic import AnyHttpUrl, BaseSettings, EmailStr, HttpUrl, PostgresDsn, validator
from typing import TypeVar, Generic
from fastapi import Query
from fastapi_pagination.default import Page as BasePage, Params as BaseParams
import pytz 


T = TypeVar("T")

class Params(BaseParams):
    page: Optional[int] = Query(1, ge=1, description="Page number")
    size: Optional[int] = Query(500, gt=0, le=1_000, description="Page size")

class Page(BasePage[T], Generic[T]):
    __params_type__ = Params

# For Dev
# base_domain = "https://bharatpowerautomation.com/"
# base_url_segment = "/dev/watertank"
# base_upload_folder = "water_tank_dev_uploads"
# data_base = "mysql+pymysql://amr_db_usr:amr_DBpass123!@172.31.10.0/dev_db"
# api_doc_path = "/devdocs"
# supporting_files_base_path = "support_files/"

# For Live
# base_domain = "https://bharatpowerautomation.com/"
# base_url_segment = "/watertank"
# base_upload_folder = "/var/www/html/water_tank_prod_uploads"
# base_upload_folder_live = "water_tank_prod_uploads"
# data_base = "mysql+pymysql://amr_db_usr:amr_DBpass123!@172.31.10.0/local_db_2"
# api_doc_path = "/prod-docs"
# supporting_files_base_path = "support_files/"


# For Local
base_domain = "https://watertank.co.in/"
base_url_segment = "/watertank"
base_upload_folder = "local_uploads"
base_upload_folder_live = "local_uploads"

# data_base = "mysql+pymysql://MaeAmrtech:AmrTech@123!@192.168.1.125/water_tank_new"
# data_base = "mysql+pymysql://amr_db_usr:amr_DBpass123!@172.31.10.0/dev_db"
data_base = "mysql+pymysql://root:W3solutions!@localhost/local_db"
# data_base = "mysql+pymysql://amr_db_usr:amr_DBpass123!@172.31.10.0/dev_db"
# data_base = "mysql+pymysql://MaeAmrtech:AmrTech@123!@192.168.1.109/dev_db"
api_doc_path = "/docs"
supporting_files_base_path = "../support_files/"


class Settings(BaseSettings):
    API_V1_STR: str = base_url_segment
    BASE_UPLOAD_FOLDER: str = base_upload_folder
    BASE_UPLOAD_FOLDER_LIVE: str = base_upload_folder_live
    SALT_KEY: str = "MaeWatert3Kd@PkE*L7!eY4&8Tank"
    SECRET_KEY: str = "W3solutions!12345"
    DATA_BASE: str = data_base
    BASE_DOMAIN: str = base_domain
    API_DOC_PATH: str = api_doc_path
    TWILIO_ACCOUNT_SID="AC6e5b0341b2b875c8b5b6f2eb3cac66e8"
    TWILIO_AUTH_TOKEN="91f8e7c073450d971a4eaae43f26901d"
    TWILIO_PHONE_NUMBER="16787218649"
    SUPPORTING_FILES_BASE_PATH: str = supporting_files_base_path
    tz_NY = pytz.timezone('Asia/Kolkata')  


    # 60 minu
    ACCESS_TOKEN_EXPIRE_MINUTES: int = 60 * 24 * 8
    SERVER_NAME: str = 'localhost'
    SERVER_HOST: AnyHttpUrl = "http://localhost:8000"
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = ["http://localhost:8000",  "http://localhost:8080", "http://localhost:3000",
                                              "http://localhost:3001", "http://localhost:3002", "https://cbe.themaestro.in", "http://cbe.themaestro.in",
                                              ]
    # BACKEND_CORS_ORIGINS is a JSON-formatted list of origins
    # e.g: '["http://localhost", "http://localhost:4200", "http://localhost:3000", \
    # "http://localhost:8080", "http://local.dockertoolbox.tiangolo.com"]'

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    PROJECT_NAME: str = 'WaterTank'
    # SENTRY_DSN: Optional[HttpUrl] = None

    # @validator("SENTRY_DSN", pre=True)
    # def sentry_dsn_can_be_blank(cls, v: str) -> Optional[str]:
    #     if len(v) == 0:
    #         return None
    #     return v

    # POSTGRES_SERVER: str
    # POSTGRES_USER: str
    # POSTGRES_PASSWORD: str
    # POSTGRES_DB: str
    SQLALCHEMY_DATABASE_URI: Optional[str] = None

    @validator("SQLALCHEMY_DATABASE_URI", pre=True)
    def assemble_db_connection(cls, v: Optional[str], values: Dict[str, Any]) -> Any:
        if isinstance(v, str):
            return v
        # return PostgresDsn.build(
        #     scheme="postgresql",
        #     user=values.get("POSTGRES_USER"),
        #     password=values.get("POSTGRES_PASSWORD"),
        #     host=values.get("POSTGRES_SERVER"),
        #     path=f"/{values.get('POSTGRES_DB') or ''}",
        # )

        return data_base

    # SMTP_TLS: bool = True
    # SMTP_PORT: Optional[int] = None
    # SMTP_HOST: Optional[str] = None
    # SMTP_USER: Optional[str] = None
    # SMTP_PASSWORD: Optional[str] = None
    # EMAILS_FROM_EMAIL: Optional[EmailStr] = None
    # EMAILS_FROM_NAME: Optional[str] = None

    # @validator("EMAILS_FROM_NAME")
    # def get_project_name(cls, v: Optional[str], values: Dict[str, Any]) -> str:
    #     if not v:
    #         return values["PROJECT_NAME"]
    #     return v

    EMAIL_RESET_TOKEN_EXPIRE_HOURS: int = 48
    EMAIL_TEMPLATES_DIR: str = "/app/app/email-templates/build"
    EMAILS_ENABLED: bool = False

    @validator("EMAILS_ENABLED", pre=True)
    def get_emails_enabled(cls, v: bool, values: Dict[str, Any]) -> bool:
        return bool(
            values.get("SMTP_HOST")
            and values.get("SMTP_PORT")
            and values.get("EMAILS_FROM_EMAIL")
        )

    EMAIL_TEST_USER: EmailStr = "test@example.com"  # type: ignore
    # FIRST_SUPERUSER: EmailStr
    # FIRST_SUPERUSER_PASSWORD: str
    USERS_OPEN_REGISTRATION: bool = False

    class Config:
        case_sensitive = True


settings = Settings()
