from fastapi import APIRouter


from app.api.endpoints import login,user,mqtt_publish,wttest,readings,server_to_device,location,location_setting,location_data,user_location_mapping,dashboard,report_page,mqtt

api_router = APIRouter()
 
api_router.include_router(login.router, tags=["Login"])

api_router.include_router(user.router, prefix="/user", tags=["User"])

api_router.include_router(location.router, prefix="/location", tags=["Location"])

api_router.include_router(user_location_mapping.router, prefix="/user_location_mapping", tags=["UserLocationMapping"])

api_router.include_router(location_setting.router, prefix="/location_setting", tags=["Location Setting"])

api_router.include_router(location_data.router, prefix="/location_data", tags=["Location Data"])

api_router.include_router(dashboard.router, prefix="/dashboard", tags=["Dashboard"])

api_router.include_router(report_page.router, prefix="/report_page", tags=["Report Page"])

# api_router.include_router(mqtt.router, prefix="/mqtttest", tags=["MQTT Test"])
api_router.include_router(mqtt_publish.router, prefix="/mqtt_publish", tags=["MQTT Publish"])


api_router.include_router(server_to_device.router, prefix="/sendcommands", tags=["Mqtt Send Commands"])

api_router.include_router(readings.router, prefix="/readings", tags=["Readings"])

