from logging import error
from typing import Generator, Any, Dict, Optional, Union

from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import jwt
import json
import random
import datetime
import requests
from pydantic import ValidationError
from sqlalchemy.orm import Session
from twilio.rest import Client
import hashlib
from sqlalchemy import or_,and_

from app import models, schemas
from app.api.endpoints.report import report_pump_run_time,water_supplied_based_on_pump_capacity,report_pump_failed_to_run,report_failure_analysis
from app.models import WebhookCallHistory,Location,UserLocationMapping,LocationData,LocationSetting,LocationDataTimeSettingLog,TimeSetting,ReportPumpRunTime

from app.core import security
from app.core.config import settings
from app.db.session import SessionLocal
from app import crud
from app.utils import check_pump_running
from datetime import datetime,timedelta

import pdfkit
from prettytable import PrettyTable
import pandas as pd
import re

reusable_oauth2 = OAuth2PasswordBearer(
    tokenUrl=f"{settings.API_V1_STR}/login/access-token"
)


def get_db() -> Generator:
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()



# def get_by_username(db: Session, *, username: str) -> Optional[models.User]:
#         return db.query(models.User).filter(models.User.username != None, models.User.username == username).first()



def send_sms(to_number, body):
    client = Client(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)
    return client.messages.create(from_=settings.TWILIO_PHONE_NUMBER,
                                  to=to_number, body=body)

def send_msg(message, mobile_no):
    requestsurl = "https://www.fast2sms.com/dev/bulk"
    payload = f"sender_id=FSTSMS&message={message}&language=english&route=p&numbers={mobile_no}"
    headers = {'authorization': "lUM6aZzQLx2untDmvGe7Ef8od1FIw3K5ABkNTrgJ0ihCjYPcSyeQHPRYICV1yLpclNrUSBdkmz6fGEKW",'Content-Type': "application/x-www-form-urlencoded",'Cache-Control': "no-cache",}
    response = requests.request("POST", requestsurl, data=payload, headers=headers)
    return json.loads(response.text)


def get_by_mobile(db: Session, *, mobile: str) -> Optional[models.User]:
        return db.query(models.User).filter(models.User.mobile_no != None, models.User.mobile_no == mobile, models.User.status != -1).first()

def authenticate(db: Session, *, mobile: str, password: str) -> Optional[models.User]:
        user = get_by_mobile(db, mobile=mobile)
        
        if not user:
            return None
        if not security.verify_password(password, user.password):
            return None
        return user

def is_active(user: models.User):
        return user.status
        

def get_current_user(
    db: Session = Depends(get_db), token: str = Depends(reusable_oauth2)) -> models.User:
    try:
        payload = jwt.decode(
            token, settings.SECRET_KEY, algorithms=[security.ALGORITHM]
        )
        
        token_data = schemas.TokenPayload(**payload)
    except (jwt.JWTError, ValidationError):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
        )
    user = get(db, id=token_data.sub)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user

def get_current_active_user(
    current_user: models.User = Depends(get_current_user)):
    
    if not crud.user.is_active(current_user):
        raise HTTPException(status_code=400, detail="Inactive user")
    return current_user


 # user = db.query(models.User.id, models.User.username, models.User.user_type, models.User.mobile_no,models.User.password, models.User.email, models.User.profile_img, models.User.status, models.Security.id.label("security_id"), models.Employee.id.label("employee_id"), models.Security.gate_id).filter(models.User.id == id).outerjoin(models.Security, models.Security.user_id==models.User.id).outerjoin(models.Employee, models.Employee.user_id==models.User.id).first()

def get(db: Session, id: Any) -> Optional[models.User]:
    user = db.query(models.User.id, models.User.username, models.User.user_type, models.User.mobile_no,models.User.password, models.User.email, models.User.status).filter(models.User.id == id, models.User.status != -1).first()
    return user

def get_user(db:Session, user_id:Any):
    if user_id:
        user=db.query(models.User).filter(models.User.id == user_id, models.User.status != -1).first()
        if user:
            return user.username
        else:
            return None
    else:
        return None

def get_user_type(user_type: Any):
    if user_type == 1:
        return "Admin"

    elif user_type == 2:
        return "Customer"

    else:
        return ""

def get_tank_type(tank_type: Any):
    if tank_type == 1:
        return "Circle"

    elif tank_type == 2:
        return "Rectangle"

    else:
        return ""
        
def sec_to_hours(sec):
    hr = sec // (60 * 60)
    min = (sec - (hr * 60 * 60)) // 60 
    secs = sec - ((hr * 60 * 60) + (min * 60))
    return (f"{hr}:{min}:{secs}")

def get_otp():
    otp = ''
    reset = ""
    characters = '0123456789'
    reset_character = 'qwertyuioplkjhgfdsazxcvbnm0123456789QWERTYUIOPLKJHGFDSAZXCVBNM'
    
    for i in range(0, 4):
        otp += characters[random.randint(0, len(characters) - 1)]
    for j in range(0, 20):
        reset += reset_character[random.randint(
            0, len(reset_character) - 1)]

    current_time = datetime.now(settings.tz_NY)
    created_at = current_time.strftime("%Y-%m-%d %H:%M:%S")
    expire_time = current_time +timedelta(minutes=5)
    expire_at = expire_time.strftime("%Y-%m-%d %H:%M:%S")
    otp_valid_upto = expire_time.strftime("%d-%m-%Y %I:%M %p")

    return [otp, reset , created_at, expire_time, expire_at, otp_valid_upto] 


def hms_to_s(s):
    t = 0
    for u in s.split(':'):
        t = 60 * t + int(u)
    return t


def check_keys(key,dict_data):
    if key in dict_data:
        if dict_data[f'{key}']:
            error=0
            msg="success"
            return error,msg
    else:
        error=1
        msg=f"{key} is missing"
        return error,msg
        


def location_data_readings(db,location_code,table_id,table_data_id,power_voltage_l1,power_voltage_l2,power_voltage_l3
                                    ,power_status,water_level_meter,pump_status,battery_voltage,valve_status,
                                    pump_running_hrs,pump_running_hrs_day,running_pump,discharged_water_volume,
                                    signal_strength,pump1_failure_status,pump2_failure_status,valve_open_fail,valve_close_fail,
                                    mode,battery_status,running_beyond_set_time,error_codes,description):
    error=0
    msg=""
    today = datetime.now(settings.tz_NY).date()

    from_date=today.strftime("%Y-%m-%d 00:00:00")
    to_date=today.strftime("%Y-%m-%d 23:59:59")
    if location_code == None:
        error=1
        msg="Location id is missing"
    
    if water_level_meter == None:
        error=1
        msg="Water level is missing"
    
    # error,msg,pump_running_hrs=check_pump_running(pump_running_hrs)
    # if error==0:
    #     error,msg,pump_running_hrs_day=check_pump_running(pump_running_hrs_day)
    
    # Check Mapping Location
    check_location=crud.location.check_location(db, code=location_code)

    if not check_location:
        error=1
        msg="Location id not found"
    
    else:
        check_mapping_location=db.query(UserLocationMapping).filter(UserLocationMapping.location_id == check_location.id,UserLocationMapping.status != -1).first()
       
        if not check_mapping_location:
            error=1
            msg="Location not mapped with user"
        else:
            
            check_location_setting=db.query(LocationSetting).filter(LocationSetting.location_id == check_location.id,LocationSetting.status == 1).first()        

            if check_location_setting:
               
                # convert meter to liter
                if check_location.tank_type == 1:

                    calc_water_liter = 3.14 * float(check_location.radius) ** 2 * float(water_level_meter)

                if check_location.tank_type == 2:
                    calc_water_liter = float(water_level_meter) * float(check_location.length) * float(check_location.width)
                
                if error == 0:
                    # last_dis_water=db.query(LocationData).filter(LocationData.location_id==check_location.id,LocationData.status==1,from_date <= Location.created_at ,Location.created_at<= to_date).order_by(LocationData.created_at.desc()).first()
                    # if last_dis_water:
                    #     if last_dis_water.discharged_water_volume_in_day:
                    #         last_dis_water_day=last_dis_water.discharged_water_volume_in_day
                    #     else:
                    #         last_dis_water_day=0
                    # else:
                    #     last_dis_water_day=0

                    # discharged_water_volume_in_day=(last_dis_water_day+discharged_water_volume)
                    if pump1_failure_status==0 and pump2_failure_status==0:
                        get_last_data=db.query(LocationData).filter(LocationData.location_id==check_location.id,LocationData.running_pump==running_pump,
                        # LocationData.pump1_failure_status==0,LocationData.pump2_failure_status==0,
                        LocationData.created_at>=from_date,).order_by(LocationData.created_at.desc()).first()
                        # if pump_status==0 or pump_status==1:   /
                        if get_last_data:
                            duration=0
                            discharged_water_volume=0

                            if get_last_data.pump_status==1:   
                                
                                if pump_status==0:
                                    print("***************************************************")
                                    # print("t1:",datetime.now(settings.tz_NY).replace(microsecond=0,tzinfo=None).timestamp(),datetime.now(settings.tz_NY),"t2:",get_last_data.created_at.timestamp(),get_last_data.created_at,get_last_data.id)
                                    duration=(datetime.now(settings.tz_NY).replace(microsecond=0,tzinfo=None)).timestamp() - (get_last_data.pump_on_time).timestamp()
                                    # print("diff:",duration)
                                    pump_on_time=get_last_data.pump_on_time
                                    pump_off_time=datetime.now(settings.tz_NY)
                                    # if get_last_data.pump_running_hrs_day:
                                    #     pump_running_hrs_day=duration+get_last_data.pump_running_hrs_day
                                    # else:
                                    #     pump_running_hrs_day=duration
                                else:
                                    # pump_running_hrs_day=get_last_data.pump_running_hrs_day
                                    duration=0
                                    discharged_water_volume=0
                                    discharged_water_volume_in_day=get_last_data.discharged_water_volume_in_day
                                    pump_on_time=get_last_data.pump_on_time
                                    pump_off_time=None
                            if get_last_data.pump_status==0:
                                
                                if pump_status==1:
                                    pump_on_time=datetime.now(settings.tz_NY)
                                    pump_off_time=None
                                    discharged_water_volume=0
                                    discharged_water_volume_in_day=get_last_data.discharged_water_volume_in_day
                                    
                                    duration=0
                                    # if get_last_data.pump_running_hrs_day:
                                    #     pump_running_hrs_day=duration+get_last_data.pump_running_hrs_day
                                    # else:
                                    #     pump_running_hrs_day=duration
                                else:
                                    # pump_running_hrs_day=get_last_data.pump_running_hrs_day
                                    duration=0
                                    discharged_water_volume=0
                                    discharged_water_volume_in_day=get_last_data.discharged_water_volume_in_day
                                    pump_on_time=get_last_data.pump_on_time
                                    pump_off_time=get_last_data.pump_off_time
                            get_water_flow=db.query(LocationSetting).filter(LocationSetting.location_id==check_location.id,LocationSetting.status==1).first()
                            if running_pump==1:
                                water_flow=get_water_flow.pump1_waterflow_per_min
                            if running_pump==2:
                                water_flow=get_water_flow.pump2_waterflow_per_min
                            if duration !=0:
                                discharged_water_volume = float(float(duration/60) * float(water_flow))
                            
                            # discharged_water_volume_in_day=float(get_last_data.discharged_water_volume)+float(discharged_water_volume)

                            # get_all_data=db.query(LocationData).filter(LocationData.location_id==check_location.id,LocationData.running_pump==running_pump,
                            # LocationData.pump1_failure_status==0,LocationData.pump2_failure_status==0,
                            # LocationData.created_at>=from_date,).order_by(LocationData.created_at.desc()).all()
                            # run_day=0
                            # water_day=0
                            # if get_all_data:
                            #     for row in get_all_data:
                            #         run_day+=row.pump_running_hrs
                            #         water_day+=row.discharged_water_volume
                            # discharged_water_volume_in_day=float(water_day)+float(discharged_water_volume)
                            # pump_running_hrs_day=run_day+duration
                        else:
                            pump_running_hrs_day=0
                            duration=0
                            discharged_water_volume=0
                            discharged_water_volume_in_day=0
                            pump_on_time=datetime.now(settings.tz_NY)
                            pump_off_time=None
                            pump_running_hrs=0
                    else:
                        pump_running_hrs_day=0
                        duration=0
                        discharged_water_volume=0
                        discharged_water_volume_in_day=0
                        pump_on_time=datetime.now(settings.tz_NY)
                        pump_off_time=None
                        pump_running_hrs=0
                    get_all_data=db.query(LocationData).filter(LocationData.location_id==check_location.id,LocationData.running_pump==running_pump,
                    # LocationData.pump1_failure_status==0,LocationData.pump2_failure_status==0,
                    LocationData.created_at>=from_date,).order_by(LocationData.created_at.desc()).all()
                    run_day=0
                    water_day=0
                    if get_all_data:
                        for row in get_all_data:
                            run_day+=row.pump_running_hrs
                            water_day+=row.discharged_water_volume
                    discharged_water_volume_in_day=float(water_day)+float(discharged_water_volume)
                    pump_running_hrs_day=run_day+duration

                    
                        # get_pre_data=db.query(LocationData).filter(LocationData.location_id==check_location.id,LocationData.running_pump==running_pump,
                        # LocationData.created_at<from_date,).order_by(LocationData.created_at.desc()).first()

                        # if get_pre_data:
                        #     if get_pre_data.pump_status==1:
                        #         if pump_status==1:
                                    



                                
                                
                                # t1 = datetime.strptime(datetime.strftime(datetime.now(settings.tz_NY), "%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S")
                                # t2 = datetime.strptime(datetime.strftime(get_last_data.created_at, "%Y-%m-%d %H:%M:%S"),"%Y-%m-%d %H:%M:%S")                
                                # diff = t1 - t2
                                # days, seconds = diff.days, diff.seconds
                                # hours = days * 24 + seconds // 3600
                                # minutes = (seconds % 3600) // 60
                                # seconds = seconds % 60   
                                
                                # duration = datetime.strptime(f"{hours}:{minutes}:{seconds}", '%H:%M:%S')


                    check_duplicate_data=db.query(LocationData).filter(LocationData.location_id==check_location.id,LocationData.created_at==datetime.now(settings.tz_NY),
                                            LocationData.status==1,LocationData.table_id==table_id).first()
                    if not check_duplicate_data:

                        insert_data_location=LocationData(location_id=check_location.id,
                                                            table_id=table_id,
                                                            table_data_id=table_data_id,
                                                            power_status=power_status,
                                                            water_level_meter=water_level_meter,
                                                            pump_status=pump_status,
                                                            valve_status=valve_status,
                                                            battery_voltage=battery_voltage,
                                                            water_volume_liter=calc_water_liter,
                                                            pump1_waterflow_per_min=check_location_setting.pump1_waterflow_per_min,
                                                            pump2_waterflow_per_min=check_location_setting.pump2_waterflow_per_min,
                                                            allowed_running_hrs=check_location_setting.allowed_running_hrs,
                                                            low_water_level=check_location_setting.low_water_level,
                                                            high_water_level=check_location_setting.high_water_level,
                                                            preset_water_level=check_location_setting.preset_water_level,
                                                            
                                                            pump_on_time=pump_on_time,
                                                            pump_off_time=pump_off_time,

                                                            pump_running_hrs=duration,
                                                            pump_running_hrs_day=pump_running_hrs_day,
                                                            
                                                            discharged_water_volume=discharged_water_volume,
                                                            discharged_water_volume_in_day=discharged_water_volume_in_day,
                                                            
                                                            running_pump=running_pump,
                                                            signal_strength=signal_strength,
                                                            
                                                            pump1_failure_status=pump1_failure_status,
                                                            pump2_failure_status=pump2_failure_status,
                                                            # pump1_failure_status=0,
                                                            # pump2_failure_status=0,
                                                            
                                                            battery_status=battery_status,
                                                            power_voltage_l1=power_voltage_l1,
                                                            power_voltage_l2=power_voltage_l2,
                                                            power_voltage_l3=power_voltage_l3,
                                                            
                                                            valve_open_fail=valve_open_fail,
                                                            valve_close_fail=valve_close_fail,
                                                            # valve_open_fail=0,
                                                            # valve_close_fail=0,

                                                            valve_open_failure_alarm=valve_open_fail,
                                                            valve_close_failure_alarm=valve_close_fail,
                                                            battery_low_alarm=battery_status,
                                                            pump_run_beyond_running_hours_alarm=0,
                                                            pump1_failure_alarm=pump1_failure_status,
                                                            pump2_failure_alarm=pump2_failure_status,

                                                            mode=mode,running_beyond_set_time=running_beyond_set_time,
                                                            error_codes=error_codes,
                                                            description=description,
                                                            status=1,created_at=datetime.now(settings.tz_NY)
                                                        )
                            
                        db.add(insert_data_location)
                        db.commit()

                        # dis_water_in_day=(last_dis_water_day+insert_data_location.discharged_water_volume)
                        
                        # insert_data_location.discharged_water_volume_in_day=dis_water_in_day
                        # db.add(insert_data_location)
                        # db.commit()

                        data=db.query(LocationData).filter(LocationData.id==insert_data_location.id).scalar()
                        
                        # R1 Report
                        report_for_pump_run_time=report_pump_run_time(db,data,from_date,to_date)

                        # R2 report
                        report_pump_failed_to_run_table=report_pump_failed_to_run(db,data,from_date,to_date)
                        
                        # R3 report
                        report_failure_analysis_table=report_failure_analysis(db,data,from_date,to_date)

                        # R4 Report
                        report_supplied_water=water_supplied_based_on_pump_capacity(db,data,from_date,to_date)
                        pre_10_day=((datetime.now(settings.tz_NY))-timedelta(days=15)).replace(microsecond=0,tzinfo=None)
                        # R5 Estimation of Water Distributed based on Geography
                        # report_distributed_water=water_distributed_based_on_geography(db,data,from_date,to_date)
                        print("reading :date",pre_10_day)
                        delete_data=db.query(LocationData).filter(LocationData.location_id==check_location.id,LocationData.created_at<=pre_10_day).delete()
                        db.commit()
                        reply="Success!!!"
                    else:
                        reply="Duplicate Entry found"
            else:
                error=1
                msg="Location setting not set"
    
    if error==1:
        reply=msg
    return reply






def send_device_request(topic,auth_code,device_id,message):

    # message=f"f1:{device_id},f100:{my_lst_str},f101:{al_run[0]},f102:{f102},f103:{f103},f104:{f104},f105:{f105},f106:{f106}"
    # data={'topic':topic,'auth_code':auth_code,'device_id':device_id,'message':message}
    # baseurl = f'http://192.168.1.125:1004/publishtodevice?topic={topic}&auth_code={auth_code}&device_id={device_id}&message={message}'
    baseurl = f'http://bharatpowerautomation.com:3000/publishtodevice?auth_code={auth_code}&message={message}'
    
    # baseurl = 'http://cbe.themaestro.in:1004/publishtodevice'
    print(baseurl)
    # try:
    response=requests.request("GET", baseurl)
    res = json.loads(response.text)
    print(res)
    print("publishtodevice done")
    # except requests.exceptions.ConnectionError:
    #     res = "Connection refused"
    # if res and res['status'] and res['status'] == 1:
    return True
# location setting call
# @router.get("/setting_call")
def setting_call(db,location_id,operation):
    if operation=='getdevicesettings':
        check_location=db.query(Location).filter(Location.id==location_id,Location.status==1).first()
        if check_location:
            locatn_setting = db.query(LocationSetting).filter_by(status = 1, location_id = check_location.id).scalar()
            time_setting = db.query(TimeSetting).filter_by(status = 1, location_id = check_location.id).all()

            al_run=[]
            if locatn_setting or time_setting:
                val7,val8,val9=(str(locatn_setting.allowed_running_hrs)).split(':')
                al_run.append(f'{val7}{val8}')
                
                location_setting={}
                time_slot=[]

                for slot in time_setting:
                    val1,val2,val3=(str(slot.from_time)).split(':')
                    val4,val5,val6=(str(slot.to_time)).split(':')


                    time_slot.append(f'{val1}{val2}{val4}{val5}')

                my_lst_str = ''+':'.join(map(str, time_slot))
                
                f102=locatn_setting.low_water_level if locatn_setting.low_water_level else 0
                f103=locatn_setting.high_water_level if locatn_setting.high_water_level else 0
                f104=locatn_setting.preset_water_level if locatn_setting.preset_water_level else 0
                f105=locatn_setting.pump1_waterflow_per_min if locatn_setting.pump1_waterflow_per_min else 0
                f106=locatn_setting.pump2_waterflow_per_min if locatn_setting.pump2_waterflow_per_min else 0
                f107=locatn_setting.lpp if locatn_setting.lpp else 0
                f108=locatn_setting.amps1 if locatn_setting.amps1 else 0
                f109=locatn_setting.amps2 if locatn_setting.amps2 else 0
                f110=check_location.time_frequency if check_location else 0

                topic='devicesetting'
                device_id=check_location.location_code
                message=f"f0={topic},f1={device_id},f100={my_lst_str},f101={al_run[0]},f102={f102},f103={f103},f104={f104},f105={f105},f106={f106},f107={f107},f108={f108},f109={f109},f110={f110}"
                auth_code=hashlib.sha1((message+settings.SALT_KEY).encode()).hexdigest()
                
                send_device_request(topic,auth_code,device_id,message)
                # msg= response
                msg="Success"


def xls_to_pdf(path,FileName,title,report_no):
    path=path

    df = pd.read_excel(f'{path}{FileName}.xls',header=0,index_col=0)

    df.fillna('', inplace = True) 
    f = open(f'{path}{FileName}.csv','w')
    a = df.to_csv(index=False,header=False)
    f.write(a)
    f.close()

    a = open(f'{path}{FileName}.csv', 'r')

    # read the csv file
    a = a.readlines()
    aa=[]
    for i in range(0,len(a)):
        aa.append(a[i].split(','))
    # Separating the Headers
    l1 = aa[1]

    t = PrettyTable(header=False,border=True)

    t.title=title
    headers=[]

    sr_no=[]
    date=[]
    pump_on_time=[]
    pump_off_time=[]
    running_time=[]
    run_pump=[]
    mode=[]
    title1=[]
    title2=[]
    title3=[]
    title4=[]
    title5=[]
    title6=[]
    title7=[]
    title8=[]
    title9=[]
    title10=[]
    title11=[]
    title12=[]
    title13=[]
    title14=[]
    title15=[]
    title16=[]



    
    level_meter = []
    vol_ltr=[]
    off_level_meter=[]
    off_level_ltr=[]
    
    level_meter2 = []

    low_level=[]
    
    duration=[]
    bat_vol_sts=[]
    water_mtr=[]
    bat_vol=[]
    ac_power=[]

    on_mtr=[]
    on_ltr=[]
    pump_off_mtr=[]
    pump_off_ltr=[]
    run_hrs=[]
    water_vol=[]

    sr_no=[]
    period=[]
    # print(aa)
    run_hour = []

    for i in range(1, len(aa)) :
        if report_no==1:
            # headers.append(aa[0])
            # print(a[1])
            # if i>1:
            if i<len(aa)-1:
                

                # headers.append(aa[i][0])
                if i==1:
                    title1.append(aa[i][0])
                    aa[i][0] = f"title1{i-1}"

                    title2.append(aa[i][1])
                    aa[i][1] = f"title2{i-1}"  

                    title3.append(aa[i][2])
                    aa[i][2] = f"title3{i-1}"  

                    title4.append(aa[i][5])
                    aa[i][5] = f"title4{i-1}" 

                    title5.append(aa[i][8])
                    aa[i][8] = f"title5{i-1}"  
                    
                    title6.append(aa[i][9])
                    aa[i][9] = f"title6{i-1}"  

                    title7.append(aa[i][10])
                    aa[i][10] = f"title7{i-1}" 

                if aa[i][3]:
                    level_meter.append(aa[i][3])
                    aa[i][3] = f"level_meter{i-1}" 
                if aa[i][4]:
                    vol_ltr.append(aa[i][4])
                    aa[i][4] = f"vol_ltr{i-1}" 
                if aa[i][6]:
                    off_level_meter.append(aa[i][6])
                    aa[i][6] = f"off_level_meter{i-1}" 
                if aa[i][7]:
                    off_level_ltr.append(aa[i][7])
                    aa[i][7] = f"off_level_ltr{i-1}"  
            else:
                for ii in range(0,10):
                    if ii==0:
                        run_hour.append(aa[i][ii])
       
        if report_no==2:
            # print("111111111111111111111111111111111111111",aa)


            if i<len(aa)-2:
                if i==1:
                    title1.append(aa[i][0])
                    aa[i][0] = f"title1{i-1}"

                    title2.append(aa[i][1])
                    aa[i][1] = f"title2{i-1}"  

                    title3.append(aa[i][2])
                    aa[i][2] = f"title3{i-1}"  
                    
                    title4.append(aa[i][3])
                    aa[i][3] = f"title4{i-1}"
                    
                    title5.append(aa[i][5])
                    aa[i][5] = f"title5{i-1}" 

                if aa[i][4]:
                    level_meter2.append(aa[i][4])
                    aa[i][4] = f"level_meter{i-1}"
                
            else:
                for ii in range(0,5):
                    if ii==0:
                        run_hour.append(aa[i][ii])

        if report_no==3:
            if i<len(aa)-4:
                # return aa
                if i==1:
                    title1.append(aa[i][0])
                    aa[i][0] = f"title1{i-1}"

                    title2.append(aa[i][1])
                    aa[i][1] = f"title2{i-1}"  

                    title3.append(aa[i][2])
                    aa[i][2] = f"title3{i-1}"  
                    
                    title4.append(aa[i][3])
                    aa[i][3] = f"title4{i-1}"
                    
                    title5.append(aa[i][4])
                    aa[i][4] = f"title5{i-1}" 

                    title6.append(aa[i][5])
                    aa[i][5] = f"title6{i-1}"  
                    
                    title7.append(aa[i][6])
                    aa[i][6] = f"title7{i-1}"  

                    title8.append(aa[i][7])
                    aa[i][7] = f"title8{i-1}" 
                    
                    title9.append(aa[i][8])
                    aa[i][8] = f"title9{i-1}" 

                    title10.append(aa[i][12])
                    aa[i][12] = f"title10{i-1}" 

                    # title11.append(aa[i][15])
                    # aa[i][15] = f"title11{i-1}" 

                
                # if aa[i][4]:
                #     duration.append(aa[i][4])
                #     aa[i][4] = f"duration{i-1}"
                # if aa[i][9]:
                #     bat_vol_sts.append(aa[i][9])
                #     aa[i][9] = f"battery_voltage_status{i-1}"
                if aa[i][9]:
                    water_mtr.append(aa[i][9])
                    aa[i][9] = f"water_level_meter{i-1}"
                if aa[i][10]:
                    bat_vol.append(aa[i][10])
                    aa[i][10] = f"battery_voltage{i-1}"
                if aa[i][11]:
                    ac_power.append(aa[i][11])
                    aa[i][11] = f"ac_power{i-1}"
            else:
                for ii in range(0,12):
                    if ii==0:
                        run_hour.append(aa[i][ii])
        if report_no==4:
            if i<len(aa)-1:
                if i==1:
                    title1.append(aa[i][0])
                    aa[i][0] = f"title1{i-1}"

                    title2.append(aa[i][1])
                    aa[i][1] = f"title2{i-1}"  

                    title3.append(aa[i][2])
                    aa[i][2] = f"title3{i-1}"  
                    
                    title4.append(aa[i][5])
                    aa[i][5] = f"title4{i-1}"
                    
                    title5.append(aa[i][9])
                    aa[i][9] = f"title5{i-1}" 

                    title6.append(aa[i][11])
                    aa[i][11] = f"title6{i-1}"  
                    
                    
                if aa[i][3]:
                    on_mtr.append(aa[i][3])
                    aa[i][3] = f"on_mtr{i-1}"
                if aa[i][4]:
                    on_ltr.append(aa[i][4])
                    aa[i][4] = f"on_ltr{i-1}"
                if aa[i][6]:
                    pump_off_mtr.append(aa[i][6])
                    aa[i][6] = f"pump_off_mtr{i-1}"
                if aa[i][7]:
                    pump_off_ltr.append(aa[i][7])
                    aa[i][7] = f"pump_off_ltr{i-1}"
                if aa[i][8]:
                    run_hrs.append(aa[i][8])
                    aa[i][8] = f"run_hrs{i-1}"
                if aa[i][10]:
                    water_vol.append(aa[i][10])
                    aa[i][10] = f"water_vol{i-1}"
            else:
                for ii in range(0,11):
                    if ii==0:
                        run_hour.append(aa[i][ii])
        if report_no==6:
            if i==1:
                title1.append(aa[i][0])
                aa[i][0] = f"title1{i-1}"

                title2.append(aa[i][1])
                aa[i][1] = f"title2{i-1}"  

                title3.append(aa[i][2])
                aa[i][2] = f"title3{i-1}"  
                
                title4.append(aa[i][4])
                aa[i][4] = f"title4{i-1}"
                
                title5.append(aa[i][5])
                aa[i][5] = f"title5{i-1}" 

                title6.append(aa[i][6])
                aa[i][6] = f"title6{i-1}"  

                title7.append(aa[i][7])
                aa[i][7] = f"title7{i-1}" 

                # title8.append(aa[i][8])
                # aa[i][8] = f"title8{i-1}" 

                # title9.append(aa[i][9])
                # aa[i][9] = f"title9{i-1}" 
            if aa[i][3]:
                period.append(aa[i][3])
                aa[i][3] = f"period{i-1}"
        
        t.add_row(aa[i])

    # print(aa,"kjdfhiukfgh",len(on_mtr),on_mtr,on_ltr,pump_off_mtr,pump_off_ltr,run_hrs,water_vol)
    
    code = t.get_html_string(attributes={
            'border': 1,
            'style': 'padding: 30px;width: 100%;border-width: 2px;border-style: solid; border-collapse: collapse;font-size: 22px;text-align:center;font-family: Arial, Helvetica, sans-serif'
            
        })   
    # print(run_hour)
    if report_no==1:

        for change in range(0, len(level_meter)):
            if change == 0:
                code = re.sub('<td>{}</td>'.format(f'title1{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;">{}</td>'.format(title1[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title2{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title2[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title3{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title3[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title4{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title4[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title5{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title5[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title6{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title6[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title7{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title7[change]), code)

                code = re.sub('<td>{}</td>'.format(f'level_meter{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(level_meter[change]), code)
                code = re.sub('<td>{}</td>'.format(f'vol_ltr{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(vol_ltr[change]), code)
                code = re.sub('<td>{}</td>'.format(f'off_level_meter{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(off_level_meter[change]), code)
                code = re.sub('<td>{}</td>'.format(f'off_level_ltr{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(off_level_ltr[change]), code)
            code = re.sub('<td>{}</td>'.format(f'level_meter{change}'), '<td style="text-align:right">{}</td>'.format(level_meter[change]), code)
            code = re.sub('<td>{}</td>'.format(f'vol_ltr{change}'), '<td style="text-align:right">{}</td>'.format(vol_ltr[change]), code)
            code = re.sub('<td>{}</td>'.format(f'off_level_meter{change}'), '<td style="text-align:right">{}</td>'.format(off_level_meter[change]), code)
            code = re.sub('<td>{}</td>'.format(f'off_level_ltr{change}'), '<td style="text-align:right">{}</td>'.format(off_level_ltr[change]), code)
            
        code = re.sub('<td>{}</td>'.format(run_hour[0]), '<table><td style="text-align:center; font-size:23px;">{}</td></table>'.format(run_hour[0]), code)


    if report_no==2:
        for change in range(0, len(level_meter2)):
            if change ==0:
                code = re.sub('<td>{}</td>'.format(f'title1{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;">{}</td>'.format(title1[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title2{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title2[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title3{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title3[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title4{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title4[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title5{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title5[change]), code)
                # code = re.sub('<td>{}</td>'.format(f'title6{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title6[change]), code)
                # code = re.sub('<td>{}</td>'.format(f'title7{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(title7[change]), code)
               
                code = re.sub('<td>{}</td>'.format(f'level_meter{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(level_meter2[change]), code)
                # code = re.sub('<td>{}</td>'.format(f'low_level{change}'), '<td style="text-align:center;font-size: 22px;font-weight: bold;padding: 30px;">{}</td>'.format(low_level[change]), code)
        
            code = re.sub('<td>{}</td>'.format(f'level_meter{change}'), '<td style="text-align:right">{}</td>'.format(level_meter2[change]), code)
            # code = re.sub('<td>{}</td>'.format(f'low_level{change}'), '<td style="text-align:right">{}</td>'.format(low_level[change]), code)
        
        code = re.sub('<td>{}</td>'.format(run_hour[0]), '<table><td style="text-align:center; font-size:23px;">{}</td></table>'.format(run_hour[0]), code)
        code = re.sub('<td>{}</td>'.format(run_hour[1]), '<table><td style="text-align:center; font-size:23px;">{}</td></table>'.format(run_hour[1]), code)
        
    
    if report_no==3:
        for change in range(0, len(water_mtr)):
            if change==0:
                code = re.sub('<td>{}</td>'.format(f'title1{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;">{}</td>'.format(title1[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title2{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(title2[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title3{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(title3[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title4{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(title4[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title5{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;">{}</td>'.format(title5[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title6{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;">{}</td>'.format(title6[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title7{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;">{}</td>'.format(title7[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title8{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;">{}</td>'.format(title8[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title9{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;">{}</td>'.format(title9[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title10{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;">{}</td>'.format(title10[change]), code)
                # code = re.sub('<td>{}</td>'.format(f'title11{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 22px;">{}</td>'.format(title11[change]), code)

                # code = re.sub('<td>{}</td>'.format(f'duration{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 20px;">{}</td>'.format(duration[change]), code)
                # code = re.sub('<td>{}</td>'.format(f'battery_voltage_status{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 20px;">{}</td>'.format(bat_vol_sts[change]), code)
                code = re.sub('<td>{}</td>'.format(f'water_level_meter{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 20px;">{}</td>'.format(water_mtr[change]), code)
                code = re.sub('<td>{}</td>'.format(f'battery_voltage{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 20px;">{}</td>'.format(bat_vol[change]), code)
                code = re.sub('<td>{}</td>'.format(f'ac_power{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 20px;">{}</td>'.format(ac_power[change]), code)
        
            # code = re.sub('<td>{}</td>'.format(f'duration{change}'), '<td style="text-align:right">{}</td>'.format(duration[change]), code)
            # code = re.sub('<td>{}</td>'.format(f'battery_voltage_status{change}'), '<td style="text-align:right">{}</td>'.format(bat_vol_sts[change]), code)
            code = re.sub('<td>{}</td>'.format(f'water_level_meter{change}'), '<td style="text-align:right">{}</td>'.format(water_mtr[change]), code)
            code = re.sub('<td>{}</td>'.format(f'battery_voltage{change}'), '<td style="text-align:right">{}</td>'.format(bat_vol[change]), code)
            code = re.sub('<td>{}</td>'.format(f'ac_power{change}'), '<td style="text-align:right">{}</td>'.format(ac_power[change]), code)
        code = re.sub('<td>{}</td>'.format(run_hour[0]), '<table><td style="text-align:center; font-size:23px;">{}</td></table>'.format(run_hour[0]), code)
        code = re.sub('<td>{}</td>'.format(run_hour[1]), '<table><td style="text-align:center; font-size:23px;">{}</td></table>'.format(run_hour[1]), code)
        code = re.sub('<td>{}</td>'.format(run_hour[2]), '<table><td style="text-align:center; font-size:23px;">{}</td></table>'.format(run_hour[2]), code)
        code = re.sub('<td>{}</td>'.format(run_hour[3]), '<table><td style="text-align:center; font-size:23px;">{}</td></table>'.format(run_hour[3]), code)

    if report_no==4:
        for change in range(0, len(on_mtr)):
            if change==0:
                code = re.sub('<td>{}</td>'.format(f'title1{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;">{}</td>'.format(title1[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title2{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(title2[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title3{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(title3[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title4{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(title4[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title5{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;">{}</td>'.format(title5[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title6{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(title6[change]), code)
                
                code = re.sub('<td>{}</td>'.format(f'on_mtr{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(on_mtr[change]), code)
                code = re.sub('<td>{}</td>'.format(f'on_ltr{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(on_ltr[change]), code)
                code = re.sub('<td>{}</td>'.format(f'pump_off_mtr{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(pump_off_mtr[change]), code)
                code = re.sub('<td>{}</td>'.format(f'pump_off_ltr{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(pump_off_ltr[change]), code)
                code = re.sub('<td>{}</td>'.format(f'run_hrs{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(run_hrs[change]), code)
                code = re.sub('<td>{}</td>'.format(f'water_vol{change}'), '<td style="text-align:center;font-size: 20px;font-weight: bold;padding: 30px;">{}</td>'.format(water_vol[change]), code)
            
            code = re.sub('<td>{}</td>'.format(f'on_mtr{change}'), '<td style="text-align:right">{}</td>'.format(on_mtr[change]), code)
            code = re.sub('<td>{}</td>'.format(f'on_ltr{change}'), '<td style="text-align:right">{}</td>'.format(on_ltr[change]), code)
            code = re.sub('<td>{}</td>'.format(f'pump_off_mtr{change}'), '<td style="text-align:right">{}</td>'.format(pump_off_mtr[change]), code)
            code = re.sub('<td>{}</td>'.format(f'pump_off_ltr{change}'), '<td style="text-align:right">{}</td>'.format(pump_off_ltr[change]), code)
            code = re.sub('<td>{}</td>'.format(f'run_hrs{change}'), '<td style="text-align:right">{}</td>'.format(run_hrs[change]), code)
            code = re.sub('<td>{}</td>'.format(f'water_vol{change}'), '<td style="text-align:right">{}</td>'.format(water_vol[change]), code)
        
        code = re.sub('<td>{}</td>'.format(run_hour[0]), '<table><td style="text-align:center; font-size:22px;">{}</td></table>'.format(run_hour[0]), code)

    if report_no==6:
        for change in range(0, len(period)):
            if change==0:
                code = re.sub('<td>{}</td>'.format(f'title1{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;">{}</td>'.format(title1[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title2{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;padding: 18px;">{}</td>'.format(title2[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title3{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;padding: 18px;">{}</td>'.format(title3[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title4{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;padding: 18px;">{}</td>'.format(title4[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title5{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;padding: 18px;">{}</td>'.format(title5[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title6{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;padding: 18px;">{}</td>'.format(title6[change]), code)
                code = re.sub('<td>{}</td>'.format(f'title7{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;padding: 18px;">{}</td>'.format(title7[change]), code)
                # code = re.sub('<td>{}</td>'.format(f'title8{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;padding: 18px;">{}</td>'.format(title8[change]), code)
                # code = re.sub('<td>{}</td>'.format(f'title9{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;padding: 18px;">{}</td>'.format(title9[change]), code)
                
                code = re.sub('<td>{}</td>'.format(f'period{change}'), '<td style="text-align:center;font-size: 18px;font-weight: bold;padding: 18px;">{}</td>'.format(period[change]), code)
                
            code = re.sub('<td>{}</td>'.format(f'period{change}'), '<td style="text-align:right">{}</td>'.format(period[change]), code)
            
    
    option = { 
      'margin-top': '40px',
    'margin-right': '10px',
    'margin-bottom': '45px',
    'margin-left': '10px',
     } 
    html_file = open(f'{path}{FileName}.html', 'w')
    html_file = html_file.write(code)
#    pdfkit.from_file(f'{path}{FileName}.html', f'{path}{FileName}.pdf',options=option)
    with open(f'{path}{FileName}.html') as f:
        pdfkit.from_file(f, f'{path}{FileName}.pdf',options=option)
    return True
