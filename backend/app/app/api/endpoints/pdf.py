# from typing import Any, List,Optional
# from sqlalchemy import Boolean, Column, Integer, String, DateTime,or_,and_, desc, asc, func
# from fastapi import APIRouter, Body, Depends, HTTPException,Form,Request
# from fastapi.encoders import jsonable_encoder
# from sqlalchemy.orm import Session
# from sqlalchemy.sql.expression import table
# from app.utils import create_reset_key,common_date_only,common_time_only,cal_tot_hrs,check_phone,digit_check, common_date,set_time_format,isInteger,check_pump_running
# from app.core.security import get_password_hash
# from app import crud, models, schemas
# from app.models import ReportPumpRunTime,UserLocationMapping,ReportSuppliedWater,Location,ReportFailureAnalysis,UserLocationMapping,ReportPumpFailedToRun
# from app.api import deps
# from app.core.config import settings
# from datetime import time,datetime,date,timedelta
# from fastapi_pagination import Page, add_pagination, paginate
# from app.core.config import settings, Page as Bpage
# import datetime as dt
# from phpserialize import serialize,unserialize
# from .report import report_pump_run_time,water_supplied_based_on_pump_capacity,report_failure_analysis,report_pump_failed_to_run
# import os
# import pathlib
# from pathlib import Path
# from pprint import pprint
# import json
# import sys
# import pandas as pd
# from pdfrw import PdfWriter
# # import win32com.client as win32 
# import xlsxwriter
# import jpype
# import asposecells
# # jpype.startJVM()
# # from asposecells.api import Workbook, FileFormatType, PdfSaveOptions

# router = APIRouter()

# @router.post("/r1_report")
# def pump_run_time_report(*,db:Session=Depends(deps.get_db),current_user: models.User = Depends(deps.get_current_user),location_id:int=Form(...),from_date:datetime=Form(...),to_date:datetime=Form(None),report:int=Form(None)):
#     today = datetime.utcnow().date()
#     var_from_date=today.strftime("%Y-%m-%d 00:00:00")
#     var_to_date=today.strftime("%Y-%m-%d 23:59:59")
#     data=[]
#     timelist=[]
#     # print(from_date,to_date)/
#     if not to_date:
#         to_date=var_to_date
#     else:
#         to_date=datetime.strptime(str(to_date),"%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d 23:59:59")
#     # print(current_user)
#     if current_user.user_type==1 or current_user.user_type==2:
#         if current_user.user_type==1:
#             check_location=db.query(Location).filter(Location.id == location_id ).first()

#         if current_user.user_type==2:
#             check_location=db.query(UserLocationMapping).filter(UserLocationMapping.user_id ==current_user.id,UserLocationMapping.location_id == location_id ).first()
        
#         if check_location:
#             pump_run_report=db.query(ReportPumpRunTime).filter(ReportPumpRunTime.location_id==location_id,from_date<= ReportPumpRunTime.created_at,ReportPumpRunTime.created_at<=to_date).all()
#             # if pump_run_report:

#             for row in pump_run_report:
#                 if row.pump_running_time:
#                     timelist.append(str(row.pump_running_time))
#                 data.append({"pump_run_report_id":row.id if row else "",
#                     "location_id":row.location.location_code if row else "",
#                     "location_code":row.location.location_code if row else "",
#                     "pumping_station":row.location.name if row else "",
#                     "date":row.created_at if row else "",
#                     "pump_on_time":common_time_only(row.pump_on_time) if row else "",
#                     "on_level_in_mtr":row.water_level_in_meter if row else "",
#                     "on_volume_in_ltr":row.water_volume_in_liter if row else "",
#                     "pump_off_time":common_time_only(row.pump_off_time) if row.pump_off_time else "",
#                     "pump_off_water_level_in_mtr":row.pump_off_water_in_level if row else "",
#                     "pump_off_water_volume_in_ltr":row.pump_off_water_volume if row else "",
#                     "pump_running_hrs":row.pump_running_time if row else "",
#                     "running_pump":row.running_pump if row else "","mode":row.mode if row else ""})
#             tot_running_hrs=cal_tot_hrs(timelist)
#             value=({"total_running_hrs":tot_running_hrs})
#             reply=data,value
#             if report==1 or report ==2:
#                 base_dir = settings.BASE_UPLOAD_FOLDER
#                 try:
#                     os.makedirs(base_dir, mode=0o777, exist_ok=True)
#                 except OSError as e:
#                     sys.exit("Can't create {dir}: {err}".format(
#                         dir=base_dir, err=e))

#                 output_dir = base_dir + "/"
#                 try:
#                     os.makedirs(output_dir, mode=0o777, exist_ok=True)
#                 except OSError as e:
#                     sys.exit("Can't create {dir}: {err}".format(
#                         dir=output_dir, err=e))
#                 dt = str(int(datetime.utcnow().timestamp()))

#                 fileName = f"{output_dir}PumpRunTimeReport{dt}.xls"

#                 pdf_values = [['Sl.no.', 'Location Id','Pumping Station','Date','Pump ON Time',
#                                     'ON Level in Mtr','ON  Volume in Ltr','Pump OFF Time',
#                                     'Pump OFF Water Level in Mtr','PUMP OFF - Water Volume in Ltr',
#                                     'Pump Running Time Hrs','Running Pump','Mode']]

#                 sr_no = 0
#                 for row in data:
#                     datas = []
#                     sr_no += 1
#                     datas.append(str(sr_no))
#                     datas.append(str(row['location_id']))
#                     datas.append(str(row['pumping_station']))

#                     t_date=common_date_only(row['date'])
                    
#                     datas.append(str(t_date))
#                     datas.append(str(row['pump_on_time']))
#                     datas.append(str(row['on_level_in_mtr']))
#                     datas.append(str(row['on_volume_in_ltr']))
#                     datas.append(str(row['pump_off_time']))
#                     datas.append(str(row['pump_off_water_level_in_mtr']))
#                     datas.append(str(row['pump_off_water_volume_in_ltr']))
#                     datas.append(str(row['pump_running_hrs']))
#                     if row['running_pump']==1:
#                         pump="Pump1"
#                     if row['running_pump']==2:
#                         pump="Pump2"
#                     datas.append(pump)
                    
#                     if row['mode']==0:
#                         mode="Auto mode"
#                     if row['mode']==1:
#                         mode="Manual mode"
#                     if row['mode']==2:
#                         mode="Server mode"
                    
#                     datas.append(mode)

#                     pdf_values.append(datas)
#                 workbook = xlsxwriter.Workbook(fileName)
#                 worksheet = workbook.add_worksheet()
#                 worksheet.write('A1', 'Pump Run Time Report')

#                 for i, l in enumerate(pdf_values):
#                     i+=2
#                     for j, col in enumerate(l):
#                         j+=1
#                         worksheet.write(i, j, col)
                
#                 worksheet.write(i+2,j-i, 'Total Running Hr of Pump ')
#                 worksheet.write(i+2,j-i+4, tot_running_hrs)
                
        
#                 workbook.close()
                



#                 # workbook = Workbook("example.xlsx")
#                 # saveOptions = PdfSaveOptions()
#                 # saveOptions.setOnePagePerSheet(True)
#                 # workbook.save("example.pdf", saveOptions)
                
                
#                 # #get a full path
#                 # pathname = Path(fileName).resolve()
                
#                 # workbook = Workbook("Book1.xlsx")

#                 # # Convert Excel to PDF
#                 # workbook.save("xlsx-to-pdf.pdf", SaveFormat.PDF)

                
                
#                 # wb = pd.read_excel(pathname)

#                 # wb=PdfWriter()
#                 # PdfName = f"{output_dir}PumpRunTimeReport{dt}.pdf"

#                 # wb.write(PdfName)

                
#                 reply = f'{settings.BASE_DOMAIN}{fileName}'                    

                
#             return reply
         