import random
import binascii

from paho.mqtt import client as mqtt_client
import requests
# from datetime import time
import time
# broker = '3.108.33.243'
broker = '35.167.187.17'

port = 1883
# topic = "perioddatatest"
topic = "xerioddata"

data = b'h#\x05\x00\x00ffh\x05\x18\xc3\xc3\x83gE33333333333He;I7T\x83\x16'
# generate client ID with pub prefix randomly
# client_id = f'python-mqtt-{random.randint(0, 100)}'
client_id = "hari00037"
# username = 'emqx'
# password = 'public'
import json
from fastapi import APIRouter
from fastapi import APIRouter, Body, Depends, HTTPException,Form,Request

router = APIRouter()

def connect_mqtt() -> mqtt_client:
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def subscribe(client: mqtt_client):
    def on_message(client, userdata, msg):
        print(f"Received from devices - `{binascii.hexlify(msg.payload).decode()}`")
        print(msg.payload)
        print(type(msg.payload))
        print(type(binascii.hexlify(msg.payload).decode()))
        data=binascii.hexlify(msg.payload).decode()
        
        
    
    client.subscribe(topic)
    client.on_message = on_message

def publish(client,msg):
    msg_count = 0
    # /while True:
    # time.sleep(1)
    msg = f"messages: {msg}"
    result = client.publish(topic, msg)
    # result: [0, 1]
    print(result)
    status = result[0]
    if status == 0:
        print(f"Sending `{msg}` to topic --- `{topic}`")
        return True

    else:
        print(f"Failed to send message to topic {topic}")
        return True
            # msg_count += 1

def run(msg=None):
    client = connect_mqtt()
    subscribe(client)
    if msg:
        publish(client,msg)

    client.loop_forever()

@router.post("/mqtt_publish_1")
def mqtt_publish_1(msg:str=Form(...)):
    run(msg)


if __name__ == '__main__':
    print("main file")
    run()
