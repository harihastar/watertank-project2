from typing import Any, List,Optional
from fastapi_pagination.api import response
from sqlalchemy import Boolean, Column, Integer, String, DateTime,or_,and_, desc, asc, func
from fastapi import APIRouter, Body, Depends, HTTPException,Form,Request
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from app.utils import create_reset_key,check_phone,digit_check,get_pagination,common_date,set_time_format,isInteger,check_pump_running
from app.core.security import get_password_hash
from app import crud, models, schemas
from app.models import WebhookCallHistory,Location,UserLocationMapping,LocationData,LocationSetting,LocationDataTimeSettingLog,TimeSetting,ReportPumpRunTime
from app.api import deps
from app.core.config import settings
from datetime import time,datetime,date,timedelta
from fastapi_pagination import Page, add_pagination, paginate
from app.core.config import settings, Page as Bpage
import datetime as dt
from phpserialize import serialize,unserialize
from .report import report_pump_run_time,water_supplied_based_on_pump_capacity,report_pump_failed_to_run,report_failure_analysis
router = APIRouter()

# Location Data
# @router.api_route("/location_data",methods=["GET","POST"])
@router.get("/location_data/")
async def location_data(*,db:Session=Depends(deps.get_db),request: Request):
    print(request)
    api= str(request.url)
    ip=request.client.host
    today = datetime.utcnow().date()

    from_date=today.strftime("%Y-%m-%d 00:00:00")
    to_date=today.strftime("%Y-%m-%d 23:59:59")
   
    call_method=request.method

    if call_method=="GET":
        data=request.query_params
    else:
        data=await request.form()
    print(data)
   
    location_code=data.get("f1") if data.get("f1") and data.get("f1") != None else None
    
    power_voltage_l1=data.get("f2") if data.get("f2") and data.get("f2") != None else None

    power_voltage_l2=data.get("f3") if data.get("f3") and data.get("f3") != None else None
    
    power_voltage_l3=data.get("f4") if data.get("f4") and data.get("f4") != None else None
    
    power_status=data.get("f5") if data.get("f5") and data.get("f5") != None else None

    water_level_meter=data.get("f6") if data.get("f6") and data.get("f6") != None else None
   
    pump_status=data.get("f7") if data.get("f7") and data.get("f7") != None else None

    battery_voltage=data.get("f8") if data.get("f8") and data.get("f8") != None else None

    valve_status=data.get("f9") if data.get("f9") and data.get("f9") != None else None
    
    pump_running_hrs=data.get("f10")
    
    pump_running_hrs_day=data.get("f11") if data.get("f11") and data.get("f11") != None else None

    running_pump=data.get("f12") if data.get("f12") and data.get("f12") != None else None
    
    discharged_water_volume=data.get("f13") if data.get("f13") and data.get("f13") != None else None
    
    signal_strength=data.get("f14") if data.get("f14") and data.get("f14") != None else None

    pump1_failure_status=data.get("f15") if data.get("f15") and data.get("f15") != None else None

    pump2_failure_status=data.get("f16") if data.get("f16") and data.get("f16") != None else None

    valve_open_fail=data.get("f17") if data.get("f17") and data.get("f17") != None else None

    valve_close_fail=data.get("f18") if data.get("f18") and data.get("f18") != None else None

    mode=data.get("f19") if data.get("f19") and data.get("f19") != None else None

    battery_status=data.get("f20") if data.get("f20") and data.get("f20") != None else None

    running_beyond_set_time=data.get("f21") if data.get("f21") and data.get("f21") != None else None

    error_codes=data.get("f22") if data.get("f22") and data.get("f22") != None else None

    error=0
    if power_voltage_l1 == None:
        error=1
        msg="power voltage l1 is missing"
    if power_voltage_l2 == None:
        error=1
        msg="power voltage l2 is missing"
    if power_voltage_l3 == None:
        error=1
        msg="power voltage l3 is missing"
    
    if power_status == None:
        error=1
        msg="power status is missing"
    else:
        if not (power_status == '0' or power_status== '1'):
            error=1
            msg="power status is wrong"
    if water_level_meter == None:
        error=1
        msg="Water level in mtrs is missing"
    
    if pump_status == None:
        error=1
        msg="pump status is missing"
    else:
        if not (pump_status == '0' or pump_status== '1'):
            error=1
            msg="pump status is wrong"
    if battery_voltage == None:
        error=1
        msg="Battery Voltage is missing"

    if valve_status == None:
        error=1
        msg="Valve status is missing"
    else:
        if not (valve_status == '0' or valve_status== '1'):
            error=1
            msg="Valve status is wrong"
    if running_pump == None:
        error=1
        msg="Running Pump is missing"
    else:
        if not (running_pump == '0' or running_pump== '1' or running_pump== '2'):
            error=1
            msg="Running Pump is wrong"
    if discharged_water_volume == None:
        error=1
        msg="Water Volme Discharged in KL is missing"
    if signal_strength == None:
        error=1
        msg="signal strength is missing"


    if pump1_failure_status == None:
        error=1
        msg="Pump1 fail is missing"
    else:
        if not (pump1_failure_status == '0' or pump1_failure_status== '1'):
            error=1
            msg="Pump1 fail is wrong"
    if pump2_failure_status == None:
        error=1
        msg="Pump2 fail is missing"
    else:
        if not (pump2_failure_status == '0' or pump2_failure_status== '1'):
            error=1
            msg="Pump2 fail is wrong"
    if valve_open_fail == None:
        error=1
        msg="Valve open fail is missing"
    else:
        if not (valve_open_fail == '0' or valve_open_fail== '1'):
            error=1
            msg="Valve open fail is wrong"
    if valve_close_fail == None:
        error=1
        msg="Valve close fail is missing"
    else:
        if not (valve_close_fail == '0' or valve_close_fail== '1'):
            error=1
            msg="Valve close fail is wrong"

    if mode == None:
        error=1
        msg="mode is missing"
    else:
        if not (mode == '0' or mode== '1' or mode== '2'):
            error=1
            msg="mode is wrong"
        
    if battery_status == None:
        error=1
        msg="Battery Low is missing"
    else:
        if not (battery_status == '0' or battery_status== '1'):
            error=1
            msg="Battery Low is wrong"
    print(running_beyond_set_time)
    if running_beyond_set_time == None:
        error=1
        msg="Pump running beyong set time is missing"
    else:
        print(running_beyond_set_time)
        if not (running_beyond_set_time == '0' or running_beyond_set_time== '1'):
            error=1
            msg="Pump running beyong set time is wrong"
    if error==0:
        response=deps.location_data_readings(db,location_code,power_voltage_l1,power_voltage_l2,power_voltage_l3
                                        ,power_status,water_level_meter,pump_status,battery_voltage,valve_status,
                                        pump_running_hrs,pump_running_hrs_day,running_pump,discharged_water_volume,
                                        signal_strength,pump1_failure_status,pump2_failure_status,valve_open_fail,valve_close_fail,
                                        mode,battery_status,running_beyond_set_time,error_codes)
        
    else:
        response=msg
        
    api_response=serialize(response)
    webhook_call_history=WebhookCallHistory(api=api,call_method=call_method,params=data,ip=ip,status=1,api_response=api_response,datetime=datetime.now(settings.tz_NY))
    db.add(webhook_call_history)
    db.commit()
    
    return response
   # List Location Data,response_model=Bpage[schemas.ListLocationData]
@router.post("/list_location_data/")
def list_location_data(
                *,page:int=1,size:int=10,
                db: Session = Depends(deps.get_db),
                location_code:Optional[str] = Form(None),
                location_name:Optional[str] = Form(None),
                from_date:datetime=Form(None),
                to_date:datetime=Form(None),
                order_by:Optional[str]=Form(None),
                order:Optional[str]=Form(None),
                current_user: models.User = Depends(deps.get_current_user),request: Request):
    
    data=request.query_params
    if data:
        size=int(data['size']) if int(data['size']) >=1 else 10
        page=int(data['page']) if int(data['page']) >=1 else 1
    else:
        size=10
        page=1

    listed_location_data=[]
    today = date.today()
    if to_date == None:
        to_date = today.strftime("%Y-%m-%d 23:59:59")
    else:
        to_date = to_date.strftime("%Y-%m-%d 23:59:59")

    if from_date == None:
        from_date = today - timedelta(days=7)
        from_date = from_date.strftime("%Y-%m-%d 00:00:00")
    else:
        from_date = from_date.strftime("%Y-%m-%d 00:00:00")

    location_data=db.query(LocationData).join(Location)
        
    if current_user.user_type == 1:
        location_data=location_data.filter(LocationData.status == 1,from_date<=LocationData.created_at,LocationData.created_at<=to_date)
    
    else:
        
        location_data=location_data.outerjoin(UserLocationMapping, LocationData.location_id == UserLocationMapping.location_id).filter(LocationData.status == 1,UserLocationMapping.status==1,UserLocationMapping.user_id== current_user.id,from_date<=LocationData.created_at,LocationData.created_at<=to_date)
    
    
    if location_code:
        location_data = location_data.filter(Location.location_code.like("%"+location_code+"%"))
    if location_name:
        location_data = location_data.filter(Location.name.like("%"+location_name+"%"))
    
    # if from_date:
    #     from_date = from_date.strftime("%Y-%m-%d 00:00:00")
    #     location_data =location_data.filter(LocationData.created_at >= from_date)

    # if to_date:
    #     to_date = to_date.strftime("%Y-%m-%d 23:59:59")
    #     location_data =location_data.filter(LocationData.created_at <= to_date)
    
    # if from_date and to_date:
    #     location_data =location_data.filter(LocationData.created_at >= from_date,LocationData.created_at <= to_date)
    
    if order_by:
        field, table = ["location_code",Location ] if order_by == "location_code"  else ['name', Location] if order_by == "location_name"  else [order_by, LocationData]

        if order == "desc":
            location_data = location_data.order_by(desc(getattr(table, field)))
        else:
            location_data = location_data.order_by(asc(getattr(table, field)))

    else:
        location_data = location_data.order_by(LocationData.id.desc())
    location_data_count=location_data.count()
    total_pages, offset, limit = get_pagination(row_count=location_data_count, current_page_no=page, default_page_size=size)
    
    location_data=location_data.limit(limit).offset(offset).all()
    for row in location_data:
        # return ((row.created_at).strftime("%d-%m-%Y")) 
        data_list = {"location_data_id":row.id if row.id else 0,
                "location_id":row.location.id if row.location.id else 0,
                "location_code":str(row.location.location_code) if str(row.location.location_code) else "", 
                "location_name":str(row.location.name) if str(row.location.name) else "", 
                
                "power_voltage_l1":str(row.power_voltage_l1) if str(row.power_voltage_l1) else '', 
                
                "power_voltage_l2":str(row.power_voltage_l2) if str(row.power_voltage_l2) else '', 
                "power_voltage_l3":str(row.power_voltage_l3) if str(row.power_voltage_l3) else '', 
                "power_status":row.power_status if row.power_status else 0, 
                "water_level_meter":str(row.water_level_meter) if str(row.water_level_meter) else '', 
                "water_volume_liter":str(row.water_volume_liter) if str(row.water_volume_liter) else '', 
                "pump_status":row.pump_status if row.pump_status else 0, 
                "battery_voltage":str(row.battery_voltage) if str(row.battery_voltage) else '', 
                "valve_status":row.valve_status if row.valve_status else 0, 
                "pump_running_hrs":datetime.fromtimestamp(row.pump_running_hrs).strftime("%H:%M:%S") if row.pump_running_hrs else datetime.fromtimestamp(0).strftime("%H:%M:%S"), 
                "pump_running_hrs_day":datetime.fromtimestamp(row.pump_running_hrs_day).strftime("%H:%M:%S") if row.pump_running_hrs_day else datetime.fromtimestamp(0).strftime("%H:%M:%S"), 
                "running_pump":row.running_pump if row.running_pump else 0, 
                "discharged_water_volume":str(row.discharged_water_volume) if str(row.discharged_water_volume) else '', 
                "discharged_water_volume_in_day":str(row.discharged_water_volume_in_day) if str(row.discharged_water_volume_in_day) else '', 
                "signal_strength":row.signal_strength if row.signal_strength else 0, 
                "pump1_failure_status":row.pump1_failure_status if row.pump1_failure_status else 0, 
                "pump2_failure_status":row.pump2_failure_status if row.pump2_failure_status else 0, 
                "valve_open_fail":row.valve_open_fail if row.valve_open_fail else 0, 
                "valve_close_fail":row.valve_close_fail if row.valve_close_fail else 0, 
                "mode":row.mode if row.mode else 0, 
                "battery_status":row.battery_status if row.battery_status else 0, 
                "running_beyond_set_time":str(row.running_beyond_set_time) if str(row.running_beyond_set_time) else '', 
                "pump1_waterflow_per_min":str(row.pump1_waterflow_per_min) if str(row.pump1_waterflow_per_min) else '', 
                "pump2_waterflow_per_min":str(row.pump2_waterflow_per_min) if str(row.pump2_waterflow_per_min) else '', 
                "allowed_running_hrs":str(row.allowed_running_hrs) if str(row.allowed_running_hrs) else '', 
                "low_water_level":str(row.low_water_level) if str(row.low_water_level) else '', 
                "high_water_level":str(row.high_water_level) if str(row.high_water_level) else '', 
                "preset_water_level":str(row.preset_water_level) if str(row.preset_water_level) else '', 
                "pump1_failure_alarm":row.pump1_failure_alarm if row.pump1_failure_alarm else 0, 
                "pump2_failure_alarm":row.pump2_failure_alarm if row.pump2_failure_alarm else 0, 
                "valve_open_failure_alarm":row.valve_open_failure_alarm if row.valve_open_failure_alarm else 0, 
                "valve_close_failure_alarm":row.valve_close_failure_alarm if row.valve_close_failure_alarm else 0, 
                "battery_low_alarm":row.battery_low_alarm if row.battery_low_alarm else 0, 
                "pump_run_beyond_running_hours_alarm":row.pump_run_beyond_running_hours_alarm if row.pump_run_beyond_running_hours_alarm else 0,  
                
                "data_date":((row.created_at).strftime("%d-%m-%Y")) if row.created_at else "",
                "data_time":(row.created_at.time()).strftime("%I:%M:%S %p") if row.created_at else "",
                "created_at": common_date(row.created_at) if row.created_at else "",
                "status":row.status if row.status else 0
                }
        listed_location_data.append(data_list)

    return {"items":listed_location_data,"total": location_data_count,"page": page,"size": page}

# ,response_model=schemas.LocationData
# View Location Data
@router.get("/view_location_data/{location_data_id}",response_model=schemas.LocationData)
def view_location_data(
    *,
    db: Session = Depends(deps.get_db),
    location_data_id: int,
    current_user: models.User = Depends(deps.get_current_user)):

    view_location_data={}
    
    location_data=db.query(LocationData).filter(LocationData.status != -1)
        
    if current_user.user_type == 1:
        location_data=location_data.filter(LocationData.id == id).first()
    
    else:
        
        location_data=location_data.outerjoin(UserLocationMapping, LocationData.id == id).filter(UserLocationMapping.user_id== current_user.id).first()
    
    if location_data:
        view_location_data.update({"location_data_id":location_data.id if location_data.id else 0,
                                    "location_id":location_data.location_id if location_data.location_id else 0,
                                    "power_voltage":location_data.power_voltage if location_data.power_voltage else 0,
                                    "power_status":location_data.power_status if location_data.power_status else 0,
                                    "waterlevel_meter":location_data.waterlevel_meter if location_data.waterlevel_meter else 0,
                                    "watervolume_liter":location_data.watervolume_liter if location_data.watervolume_liter else 0,
                                    "pump_status":location_data.pump_status if location_data.pump_status else "",
                                    "valve_status":location_data.valve_status if location_data.valve_status else "",
                                    "battery_voltage":location_data.battery_voltage if location_data.battery_voltage else "",
                                    "battery_status":location_data.battery_status if location_data.battery_status else "",
                                    "battery_alarm":location_data.battery_alarm if location_data.battery_alarm else "",
                                    "pump_running_hrs":datetime.fromtimestamp(location_data.pump_running_hrs).strftime("%H:%M:%S") if location_data.pump_running_hrs else datetime.fromtimestamp(0).strftime("%H:%M:%S"),
                                    "pump_running_hrs_day":datetime.fromtimestamp(location_data.pump_running_hrs_day).strftime("%H:%M:%S") if location_data.pump_running_hrs_day else datetime.fromtimestamp(0).strftime("%H:%M:%S"),
                                    "discharged_water_volume":location_data.discharged_water_volume if location_data.discharged_water_volume else 0,
                                    "signal_strength":location_data.signal_strength if location_data.signal_strength else 0,
                                    "pump1_failure_status":location_data.pump1_failure_status if location_data.pump1_failure_status else "",
                                    "pump2_failure_status":location_data.pump2_failure_status if location_data.pump2_failure_status else "",
                                    "pump1_failure_alarm":location_data.pump1_failure_alarm if location_data.pump1_failure_alarm else "",
                                    "pump2_failure_alarm":location_data.pump2_failure_alarm if location_data.pump2_failure_alarm else "",
                                    "valve_failure_status":location_data.valve_failure_status if location_data.valve_failure_status else "",
                                    "valve_alarm":location_data.valve_alarm if location_data.valve_alarm else "",
                                    "mode":location_data.mode if location_data.mode else "",
                                    "running_beyond_time_alarm":location_data.running_beyond_time_alarm if location_data.running_beyond_time_alarm else "",
                                    "created_at":common_date(location_data.created_at) if location_data.created_at else "",
                                    "status":location_data.status if location_data.status else 0})
       
        return view_location_data
    else:
        raise HTTPException(
        status_code=404,
        detail="Data not found",
        )


add_pagination(router)