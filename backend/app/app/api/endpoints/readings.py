import re
from typing import Any, List,Optional
from fastapi_pagination.api import response
from sqlalchemy import Boolean, Column, Integer, String, DateTime,or_,and_, desc, asc, func
from fastapi import APIRouter, Body, Depends, HTTPException,Form,Request
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import delete
from app.utils import create_reset_key,check_phone,check_metric,hex_to_float,hex_to_date,digit_check, common_date,set_time_format,isInteger,check_pump_running
from app.core.security import get_password_hash
from app import crud, models, schemas
from app.models import WebhookCallHistory,Location,UserLocationMapping,LocationData,LocationSetting,LocationDataTimeSettingLog,TimeSetting,ReportPumpRunTime
from app.api import deps
from app.core.config import settings
from datetime import time,datetime,date,timedelta
from fastapi_pagination import Page, add_pagination, paginate
from app.core.config import settings, Page as Bpage
import datetime as dt
from phpserialize import serialize,unserialize
from .report import report_pump_run_time,water_supplied_based_on_pump_capacity,report_pump_failed_to_run,report_failure_analysis
import json
import binascii
from app.models import MetricTable,PeriodicData,PeriodicHeader,DeviceOutput,HealthStatus,NetworkStatus,DeviceAlarm
# import shlex

router = APIRouter()

# Location Data
# @router.api_route("/location_data",methods=["GET","POST"])
@router.api_route("/readings",methods=["GET","POST"])
async def readings(*,db:Session=Depends(deps.get_db),request: Request):
    print(request)
    api= str(request.url)
    ip=request.client.host
    today = datetime.utcnow().date()
    pre_10_day=((datetime.now(settings.tz_NY))-timedelta(days=5)).replace(microsecond=0,tzinfo=None)
    from_date=today.strftime("%Y-%m-%d 00:00:00")
    to_date=today.strftime("%Y-%m-%d 23:59:59")
    res=None
    call_method=request.method
    print(call_method)

    if call_method=="GET":
        data=request.query_params
    elif call_method =="POST":
        data=await request.form()
    else:
        data = await request.body()
    
    x = data.get("reading")
    if x:
        reading=[i+j for i,j in zip(x[::2], x[1::2])] #zip makes (0,1),(2,3) ...
        success=0
        if reading[0]=="01":
            str_len=("".join(reading[1:3:1]))
            str_len_val = int(str_len, 16)
            tot_len=len(reading[3::1])
            if str_len_val==tot_len:
                lt=[]
                sequence_no=int(("".join(reading[3:7:1])),16)
                
                device_id=int(("".join(reading[7:11:1])),16)
                check_location=db.query(Location).filter(Location.location_code==device_id,Location.status==1).first()
                if check_location:
                    metric_id_date=("".join(reading[11:13:1]))
                    
                    sensor_level=hex_to_float(("".join(reading[13:17:1])))
                    metric_id_level=("".join(reading[17:19:1]))
                    
                    sensor_ac=int(("".join(reading[19:23:1])),16)
                    metric_id_ac=("".join(reading[23:25:1]))
                    
                    sensor_battery=hex_to_float(("".join(reading[25:29:1])))
                    metric_id_battery=("".join(reading[29:31:1]))
                    
                    sensor_volume=hex_to_float(("".join(reading[31:35:1])))
                    metric_id_volume=("".join(reading[35:37:1]))
                    
                    sensor_flow_rate=hex_to_float(("".join(reading[37:41:1])))
                    metric_id_flow_rate=("".join(reading[41:43:1]))

                    crc=("".join(reading[43::1]))

                    insert_header=PeriodicHeader(start=reading[0],bytes_to_follow=str_len_val,sequence_number=sequence_no,
                                    location_id=check_location.id,metric_date=metric_id_date,sensor_level=sensor_level,
                                    metric_level=metric_id_level,sensor_ac_voltage=sensor_ac,metric_ac_voltage=metric_id_ac,
                                    sensor_battery_voltage=sensor_battery,metric_battery_voltage=metric_id_battery,
                                    sensor_volume=sensor_volume,metric_volume=metric_id_volume,sensor_flow_rate=sensor_flow_rate,
                                    metric_flow_rate=metric_id_flow_rate,crc=crc,status=1,created_at=datetime.now(settings.tz_NY))

                    db.add(insert_header)
                    db.commit()
                    
                    delete_data=db.query(PeriodicHeader).filter(PeriodicHeader.location_id==check_location.id,PeriodicHeader.created_at<=pre_10_day).delete()
                    db.commit()
                    
                    res="Success"
                else:
                    res=f"the device id is wrong or location not found!!! device id is {device_id}"
            else:
                res=f"the reading length:{tot_len} is not equal for the given string length{str_len}:{str_len_val}"

        elif reading[0]=="81":
            str_len=("".join(reading[1:3:1]))
            str_len_val = int(str_len, 16)
            tot_len=len(reading[3::1])
            if str_len_val==tot_len:
                lt=[]
                sequence_no=int(("".join(reading[3:7:1])),16)     
                device_id=int(("".join(reading[7:11:1])),16)
                check_location=db.query(Location).filter(Location.location_code==device_id,Location.status==1).first()
                if check_location:
                    date_str,res=hex_to_date(("".join(reading[11:15:1])))
                    
                    
                    level_sensor=hex_to_float(("".join(reading[15:19:1])))
                    ac_voltage=hex_to_float(("".join(reading[19:23:1])))
                    battery_voltage=hex_to_float(("".join(reading[23:27:1])))
                    volume_sensor=hex_to_float(("".join(reading[27:31:1])))
                    flow_rate=hex_to_float(("".join(reading[31:35:1])))
                    tot_flow=hex_to_float(("".join(reading[35:39:1])))
                    
                    crc=("".join(reading[39::1]))

                    insert_data=PeriodicData(start=reading[0],bytes_to_follow=str_len_val,sequence_number=sequence_no,
                                    location_id=check_location.id,date_time=date_str,level=level_sensor,ac_voltage=ac_voltage,battery_voltage=battery_voltage,
                                    volume=volume_sensor,flow_rate=flow_rate,total_flow=tot_flow,crc=crc,status=1,created_at=datetime.now(settings.tz_NY))
                    db.add(insert_data)
                    db.commit()
                    
                    delete_data=db.query(PeriodicData).filter(PeriodicData.location_id==check_location.id,PeriodicData.created_at<=pre_10_day).delete()
                    db.commit()
                    if res == None:
                        res="Success"
                    else:
                        res=res
                    success=1
                else:
                    res=f"the device id is wrong or location not found!!! device id is {device_id}"
            else:
                res=f"the reading length:{tot_len} is not equal for the given string length{str_len}:{str_len_val}"

        elif reading[0]=="10":
            str_len=len("".join(reading[::1]))
            if str_len == 140:
                lt=[]
                device_id=int(("".join(reading[1:5:1])),16)
                check_location=db.query(Location).filter(Location.location_code==device_id,Location.status==1).first()
                if check_location:
                    sequence_no=("".join(reading[5:7:1]))

                    date_str,res=hex_to_date(("".join(reading[7:11:1])))
                    
                    output_no=int(("".join(reading[11])),16)
                    on_off_status=int(("".join(reading[12])),16)
                    output_port=int(("".join(reading[13])),16)
                    get_pre_valve=db.query(DeviceOutput).filter(DeviceOutput.location_id==check_location.id,DeviceOutput.output_port==3).order_by(DeviceOutput.created_at.desc()).first()
                    get_pre_pump=db.query(DeviceOutput).filter(DeviceOutput.location_id==check_location.id,DeviceOutput.output_port!=3).order_by(DeviceOutput.created_at.desc()).first()
                    
                    if on_off_status==1:
                        if output_port==3:
                            valve_status=1
                            running_pump=get_pre_pump.running_pump if get_pre_pump else 0
                            pump_status=get_pre_pump.pump_status if get_pre_pump else 0
                        elif output_port==1:
                            running_pump=1
                            pump_status=1
                            valve_status=get_pre_valve.valve_status if get_pre_valve else 0

                        elif output_port==2:
                            running_pump=2
                            pump_status=1
                            valve_status=get_pre_valve.valve_status if get_pre_valve else 0

                    if on_off_status==0:
                        if output_port==1:
                            valve_status=get_pre_valve.valve_status if get_pre_valve else 0
                            running_pump=1
                            pump_status=0
                        elif output_port==2:
                            running_pump=2
                            pump_status=0
                            valve_status=get_pre_valve.valve_status if get_pre_valve else 0

                        elif output_port==3:
                            running_pump=get_pre_pump.running_pump if get_pre_pump else 0
                            pump_status=get_pre_pump.pump_status if get_pre_pump else 0
                            valve_status=0

                    sensor1=("".join(reading[14:18:1]))
                    metric1=("".join(reading[18:20:1]))
                    val1=("".join(reading[20:24:1]))
                    lt.append(sensor1)
                    lt.append(metric1+'-m')
                    lt.append(val1)
                    
                    sensor2=("".join(reading[24:28:1]))
                    metric2=("".join(reading[28:30:1]))
                    val2=("".join(reading[30:34:1]))
                    lt.append(sensor2)
                    lt.append(metric2+'-m')
                    lt.append(val2)
                    
                    sensor3=("".join(reading[34:38:1]))
                    metric3=("".join(reading[38:40:1]))
                    val3=("".join(reading[40:44:1]))
                    lt.append(sensor3)
                    lt.append(metric3+'-m')
                    lt.append(val3)
                    
                    sensor4=("".join(reading[44:48:1]))
                    metric4=("".join(reading[48:50:1]))
                    val4=("".join(reading[50:54:1]))
                    lt.append(sensor4)
                    lt.append(metric4+'-m')
                    lt.append(val4)
                    
                    sensor5=("".join(reading[54:58:1]))
                    metric5=("".join(reading[58:60:1]))
                    dummy=("".join(reading[60:64:1]))
                    lt.append(sensor5)
                    lt.append(metric5+'-m')
                    lt.append(dummy)
                    data=check_metric(reading[0],lt)
                    pump_run=hex_to_float(("".join(reading[64:68:1])))

                    crc=("".join(reading[68:70:1]))
                    print(lt)
                    # data=
                    insert_device_output=DeviceOutput(start=reading[0],location_id=check_location.id,date_time=date_str,
                                        sequence_number=sequence_no,no_of_alarm=output_no,on_off_status=on_off_status,output_port=output_port,
                                        pump_status=pump_status,running_pump=running_pump,valve_status=valve_status,
                                        sensor_level=data.get("sensor_level"),metric_level=data.get("metric_level"),
                                        level=data.get("level"),
                                        sensor_ac_voltage=data.get("sensor_ac_voltage"),metric_ac_voltage=data.get("metric_ac_voltage"),
                                        ac_voltage=data.get("ac_voltage"),sensor_battery_voltage=data.get("sensor_dc_voltage"),
                                        metric_battery_voltage=data.get("metric_dc_voltage"),battery_voltage=data.get("dc_voltage"),
                                        sensor_volume=data.get("sensor_volume"),metric_volume=data.get("metric_volume"),volume=data.get("volume"),
                                        sensor_flow_rate=data.get("sensor_flow_rate"),metric_flow_rate=data.get("metric_flow_rate"),
                                        pump_run_minutes=pump_run,crc=crc,created_at=datetime.now(settings.tz_NY),status=1)
                    db.add(insert_device_output)
                    db.commit()
                    
                    delete_data=db.query(DeviceOutput).filter(DeviceOutput.location_id==check_location.id,DeviceOutput.created_at<=pre_10_day).delete()
                    db.commit()
                    
                    # print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
                    # print("success",insert_device_output.id)
                    # print("sensor_level",data.get("sensor_level"),"metric_level",data.get("metric_level"),
                    #                     "level",data.get("level"))
                    if res == None:
                        res="Success"
                    else:
                        res=res
                    success=1
                else:
                    res=f"the device id is wrong or location not found!!! device id is {device_id}"
            else:
                res=f"the reading length is not equal for the given string length{str_len}"

                
            
        elif reading[0]=="11":
            str_len=len("".join(reading[::1]))
            if str_len == 78:
                lt=[]
                device_id=int(("".join(reading[1:5:1])),16)
                check_location=db.query(Location).filter(Location.location_code==device_id,Location.status==1).first()
                if check_location:
                    date_str,res=hex_to_date(("".join(reading[5:9:1])))

                    metric_id1=("".join(reading[9:11:1]))
                    lt.append(metric_id1+'-m')
                    val1=("".join(reading[11:13:1]))
                    lt.append(val1)
                    
                    metric_id2=("".join(reading[13:15:1]))
                    lt.append(metric_id2+'-m')
                    val2=("".join(reading[15:17:1]))
                    lt.append(val2)

                    metric_id3=("".join(reading[17:19:1]))
                    lt.append(metric_id3+'-m')
                    val3=("".join(reading[19:21:1]))
                    lt.append(val3)

                    metric_id4=("".join(reading[21:23:1]))
                    lt.append(metric_id4+'-m')
                    val4=("".join(reading[23:25:1]))
                    lt.append(val4)

                    metric_id5=("".join(reading[25:27:1]))
                    lt.append(metric_id5+'-m')
                    val5=("".join(reading[27:31:1]))
                    lt.append(val5)

                    metric_id6=("".join(reading[31:33:1]))
                    lt.append(metric_id6+'-m')
                    val6=("".join(reading[33:37:1]))
                    lt.append(val6)

                    data=check_metric(reading[0],lt)
                    crc=("".join(reading[37::1]))
                    # return data.get("gprs")
                    insert_health_data=HealthStatus(start=reading[0],location_id=check_location.id,date_time=date_str,
                                        metric_csq=data.get("metric_csq"),csq=data.get("csq"),metric_gsm=data.get("metric_gsm"),
                                        gsm=data.get("gsm"),
                                        metric_gprs=data.get("metric_gprs"),gprs=data.get("gprs"),metric_data_transmit_time=data.get("metric_data_transmit"),
                                        data_transmit_time=data.get("data_transmit"),metric_dc_voltage=data.get("metric_dc_voltage"),dc_voltage=data.get("dc_voltage"),
                                        metric_ac_voltage=data.get("metric_ac_voltage"),ac_voltage=data.get("ac_voltage"),crc=crc,status=1,created_at=datetime.now(settings.tz_NY))
                    db.add(insert_health_data)
                    db.commit()
                    
                    delete_data=db.query(HealthStatus).filter(HealthStatus.location_id==check_location.id,HealthStatus.created_at<=pre_10_day).delete()
                    db.commit()
                    
                    if res == None:
                        res="Success"
                    else:
                        res=res
                    success=1
                else:
                    res=f"the device id is wrong or location not found!!! device id is {device_id}"
            else:
                res=f"the reading length is not equal for the given string length {str_len}"


        elif reading[0]=="12":
            str_len=len("".join(reading[::1]))
            if str_len == 142:
                lt=[]
                device_id=int(("".join(reading[1:5:1])),16)

                check_location=db.query(Location).filter(Location.location_code==device_id,Location.status==1).first()
                if check_location:
                    sequence_no=("".join(reading[5:7:1]))

                    date_str,res=hex_to_date(("".join(reading[7:11:1])))

                    no_of_alarm=int(("".join(reading[11])),16)
                    alarm_status=int(("".join(reading[12])),16)

                    code=int(("".join(reading[13:15:1])),16)
                    # get_pre_device_alarm=db.query(DeviceAlarm).filter(DeviceAlarm.location_id==check_location.id).order_by(DeviceAlarm.created_at.desc()).first()
                    # if get_pre_device_alarm:
                    #     alarm=get_pre_device_alarm.alarm
                    #     error=get_pre_device_alarm.error
                    #     battery_status=get_pre_device_alarm.battery_status
                    #     pump1_failure_status=get_pre_device_alarm.pump1_failure_status
                    #     pump2_failure_status=get_pre_device_alarm.pump2_failure_status
                    #     valve_open_fail=get_pre_device_alarm.valve_open_fail
                    #     valve_close_fail=get_pre_device_alarm.valve_close_fail
                    # else:
                    alarm=0
                    error=0
                    battery_status=0
                    pump1_failure_status=0
                    pump2_failure_status=0
                    valve_open_fail=0
                    valve_close_fail=0
                    if code:
                        if code==1:
                            alarm=1
                            description="Level low Alarm"
                        if code==2:
                            alarm=1
                            description="Level High Alarm"
                        if code==3:
                            alarm=1
                            description="Alarm Low Alarm"
                        if code==4:
                            alarm=1
                            description="Alarm High Alarm"
                        if code==5:
                            alarm=1
                            description="Valve Open SP Reached"
                        if code==6:
                            alarm=1
                            description="Pump Restart SP reached"
                        if code==7:
                            alarm=1
                            description="AC Voltage Fail"
                        if code==8:
                            alarm=1
                            description="Pump Run after completing Daily quota"
                        if code==9:
                            alarm=1
                            description="Pump run during outside time window"
                        if code==10:
                            alarm=1
                            description="Battery Voltage Low"
                            # battery_status=1
                        if code==11:
                            alarm=1
                            description="Battery Voltage High"
                            # battery_status=0
                        if code >=12 and code <= 50000:
                            description="Reserved"

                        if code==50001:
                            error=1
                            description="Pump 1 failure Error"
                            pump1_failure_status=alarm_status
                        if code ==50002:
                            error=1
                            description="Pump 2 failure Error"
                            pump2_failure_status=alarm_status
                        if code ==50003:
                            error=1
                            description="Valve Open Fail"
                            valve_open_fail=alarm_status
                        if code ==50004:
                            error=1
                            description="Valve Close Fail"
                            valve_close_fail=alarm_status

                        if code >=50007 and code <= 50024:
                            description="Reserved"

                        if code ==50025:
                            error=1
                            description="CRC Error in Read/Write Command"
                        if code ==50026:
                            error=1
                            description="Invalid On/Off Status"
                        if code ==50027:
                            error=1
                            description="Invalid Parameter"
                        if code ==50028:
                            error=1
                            description="Invalid Relay Operation"
                        if code ==50029:
                            error=1
                            description="Invalid Server Cmd"
                        if code ==50030:
                            error=1
                            description="Cannot execute due to mode"
                        if code ==50031:
                            error=1
                            description="Cannot execute due to Process violation"
                        if code ==50032:
                            error=1
                            description="Internal Failure (RTC/Memory)"

            

                    sensor1=("".join(reading[15:19:1]))
                    metric1=("".join(reading[19:21:1]))
                    val1=("".join(reading[21:25:1]))
                    lt.append(sensor1)
                    lt.append(metric1+'-m')
                    lt.append(val1)

                    sensor2=("".join(reading[25:29:1]))
                    metric2=("".join(reading[29:31:1]))
                    val2=("".join(reading[31:35:1]))
                    lt.append(sensor2)
                    lt.append(metric2+'-m')
                    lt.append(val2)

                    sensor3=("".join(reading[35:39:1]))
                    metric3=("".join(reading[39:41:1]))
                    val3=("".join(reading[41:45:1]))
                    lt.append(sensor3)
                    lt.append(metric3+'-m')
                    lt.append(val3)

                    sensor4=("".join(reading[45:49:1]))
                    metric4=("".join(reading[49:51:1]))
                    val4=("".join(reading[51:55:1]))
                    lt.append(sensor4)
                    lt.append(metric4+'-m')
                    lt.append(val4)

                    sensor5=("".join(reading[55:59:1]))
                    metric5=("".join(reading[59:61:1]))
                    dummy=("".join(reading[61:65:1]))
                    lt.append(sensor5)
                    lt.append(metric5+'-m')
                    lt.append(dummy)
                    
                    data=check_metric(reading[0],lt)
                    
                    pump_run=hex_to_float("".join(reading[65:69:1]))
                    crc=("".join(reading[69::1]))
                    if data.get("dc_voltage"):
                        if data.get("dc_voltage") < float(21.5) :
                            battery_status=1


                    insert_device_alarm=DeviceAlarm(start=reading[0],location_id=check_location.id,date_time=date_str,
                                        sequence_number=sequence_no,no_of_alarm=no_of_alarm,
                                        alarm_status=alarm_status,alarm=alarm,error=error,description=description,
                                        battery_status=battery_status,pump1_failure_status=pump1_failure_status,
                                        pump2_failure_status=pump2_failure_status,valve_open_fail=valve_open_fail,
                                        valve_close_fail=valve_close_fail,
                                        code=code,sensor_level=data.get("sensor_level"),metric_level=data.get("metric_level"),
                                        level=data.get("level"),sensor_ac_voltage=data.get("sensor_ac_voltage"),
                                        metric_ac_voltage=data.get("metric_ac_voltage"),ac_voltage=data.get("ac_voltage"),
                                        sensor_battery_voltage=data.get("sensor_dc_voltage"),metric_battery_voltage=data.get("metric_dc_voltage"),
                                        battery_voltage=data.get("dc_voltage"),sensor_volume=data.get("sensor_volume"),
                                        metric_volume=data.get("metric_volume"),volume=data.get("volume"),
                                        sensor_flow_rate=data.get("sensor_flow_rate"),metric_flow_rate=data.get("metric_flow_rate"),
                                        pump_run_minutes=pump_run,crc=crc,created_at=datetime.now(settings.tz_NY),status=1)
                    db.add(insert_device_alarm)
                    db.commit()

                    delete_data=db.query(DeviceAlarm).filter(DeviceAlarm.location_id==check_location.id,DeviceAlarm.created_at<=pre_10_day).delete()
                    db.commit()

                    if res == None:
                        res="Success"
                    else:
                        res=res
                    success=1
                else:
                    res=f"the device id is wrong or location not found!!! device id is {device_id}"
            else:
                res=f"the reading length is not equal for the given string length {str_len}:"


        elif reading[0]=="13":
            str_len=len("".join(reading[::1]))
            if str_len == 28:
                lt=[]
                device_id=int(("".join(reading[1:5:1])),16)
                check_location=db.query(Location).filter(Location.location_code==device_id,Location.status==1).first()
                if check_location:
                    sequence_no=("".join(reading[5:7:1]))

                    date_str,res=hex_to_date(("".join(reading[7:11:1])))
                    mode=int(("".join(reading[11])),16)    
                    if mode ==0:
                        mode_status =1
                    if mode ==1 :
                        mode_status=0
                    if mode ==2:
                        mode_status=2
                    crc=("".join(reading[12::1]))    
                    insert_network_status=NetworkStatus(start=reading[0],location_id=check_location.id,date_time=date_str,
                                        sequence_number=sequence_no,mode=mode_status,crc=crc,created_at=datetime.now(settings.tz_NY),status=1)

                    db.add(insert_network_status)
                    db.commit()

                    delete_data=db.query(NetworkStatus).filter(NetworkStatus.location_id==check_location.id,NetworkStatus.created_at<=pre_10_day).delete()
                    db.commit()

                    if res == None:
                        res="Success"
                    else:
                        res=res
                    success=1
                else:
                    res=f"the device id is wrong or location not found!!! device id is {device_id}"
            else:
                res=f"the reading length is not equal for the given string length {str_len}:"
        elif reading[0]=="a0":
            str_len=len("".join(reading[::1]))
            if str_len == 28:
                lt=[]
                device_id=int(("".join(reading[1:5:1])),16)
                check_location=db.query(Location).filter(Location.location_code==device_id,Location.status==1).first()
                if check_location:
                    sequence_no=("".join(reading[5:7:1]))
                    param_no=int(("".join(reading[7:9:1])),16)
                    param_type=int(("".join(reading[9:10:1])),16)
                    if param_type==0:
                        param_value=int(("".join(reading[10:13:1])),16)
                        crc=int(("".join(reading[13:15:1])),16)

                    if param_type==1:
                        param_value=int(("".join(reading[10:13:1])),16)
                        crc=int(("".join(reading[13:15:1])),16)

                    if param_type==2:
                        param_value=int(("".join(reading[10:13:1])),16)
                        crc=int(("".join(reading[13:15:1])),16)

                    if param_type==3:
                        param_value=int(("".join(reading[10:13:1])),16)
                        crc=int(("".join(reading[13:15:1])),16)

                    if param_type==4:
                        param_value=int(("".join(reading[10:13:1])),16)
                        crc=int(("".join(reading[13:15:1])),16)

                    if param_type==5:
                        param_value,crc_val=str(("".join(reading[10::1]))).split('0')
                        crc=int(crc_val,16) 
                    parameter_value=deps.convert_param_value(param_no,param_value)

                    
                    


        else:
            res=f"the header is not match the table header: {reading[0]}:"

        
        if success==1:
            get_last_data=db.query(LocationData).filter(LocationData.location_id==check_location.id).order_by(LocationData.created_at.desc()).first()
            if reading[0]=="81":
                get_periodic_data=insert_data
                table_id=2
                table_data_id=get_periodic_data.id
                ac_voltage=get_periodic_data.ac_voltage
                water_level_meter=get_periodic_data.level
                battery_voltage=get_periodic_data.battery_voltage
                
                valve_status=0
                
            else:
                get_periodic_data=db.query(PeriodicData).filter(PeriodicData.location_id==check_location.id).order_by(PeriodicData.created_at.desc()).first()

            if reading[0]=="10":
                get_device_output=insert_device_output
                table_id=3
                table_data_id=get_device_output.id
                ac_voltage=get_device_output.ac_voltage
                water_level_meter=get_device_output.level
                battery_voltage=get_device_output.battery_voltage
                valve_status=get_device_output.valve_status
                
            else:
                get_device_output=db.query(DeviceOutput).filter(DeviceOutput.location_id==check_location.id).order_by(DeviceOutput.created_at.desc()).first()
                
            
            if reading[0]=="11":
                get_health_data=insert_health_data
                table_id=4
                table_data_id=get_health_data.id
                ac_voltage=get_health_data.ac_voltage
                battery_voltage=get_health_data.dc_voltage

                water_level_meter=0
                valve_status=0
            else:
                get_health_data=db.query(HealthStatus).filter(HealthStatus.location_id==check_location.id).order_by(HealthStatus.created_at.desc()).first()
                

            if reading[0]=="12":
                get_device_alarm=insert_device_alarm
                table_id=5
                table_data_id=get_device_alarm.id
                ac_voltage=get_device_alarm.ac_voltage
                water_level_meter=get_device_alarm.level
                battery_voltage=get_device_alarm.battery_voltage

                valve_status=0
            else:
                get_device_alarm=None

            if reading[0]=="13":
                print("aaaaaaaaaaaaaaaaaaaaaa")
                get_network_status=insert_network_status
                table_id=6
                table_data_id=get_network_status.id
                water_level_meter=0
                valve_status=0
                ac_voltage=0
                battery_voltage=0
            else:
                get_network_status=db.query(NetworkStatus).filter(NetworkStatus.location_id==check_location.id).order_by(NetworkStatus.created_at.desc()).first()

            if water_level_meter==0:
                if get_last_data:
                    water_level_meter=get_last_data.water_level_meter
                else:
                    water_level_meter=0
            
            if valve_status==0:
                if get_device_output:
                    valve_status=get_device_output.valve_status
                else:
                    valve_status=0
            
            if ac_voltage==0:
                if get_health_data:
                    ac_voltage=get_health_data.ac_voltage
                else:
                    ac_voltage=0
            
            if battery_voltage==0:
                if get_health_data:
                    battery_voltage=get_health_data.dc_voltage
                else:
                    battery_voltage=0
            
            print("table_id",table_id,"ac_voltage",type(ac_voltage),ac_voltage)
            res=deps.location_data_readings(db=db,location_code=check_location.location_code,
                    table_id=table_id,table_data_id=table_data_id,power_voltage_l1=ac_voltage,power_voltage_l2=ac_voltage,
                    power_voltage_l3=ac_voltage,power_status=0 if (100 >= ac_voltage >= 0) else 1,
                    water_level_meter=water_level_meter,
                    pump_status=get_device_output.pump_status if get_device_output else 0,
                    battery_voltage=battery_voltage,
                    valve_status=valve_status,pump_running_hrs=None,pump_running_hrs_day=None,
                    running_pump=get_device_output.running_pump if get_device_output else 0,
                    discharged_water_volume=(get_device_output.volume) if get_device_output else 0,
                    signal_strength=None,
                    pump1_failure_status=get_device_alarm.pump1_failure_status if get_device_alarm else 0,
                    pump2_failure_status=get_device_alarm.pump2_failure_status if get_device_alarm else 0,
                    valve_open_fail=get_device_alarm.valve_open_fail if get_device_alarm else 0,
                    valve_close_fail=get_device_alarm.valve_close_fail if get_device_alarm else 0,
                    mode=get_network_status.mode if get_network_status else 0,
                    battery_status=get_device_alarm.battery_status if get_device_alarm else 0,
                    running_beyond_set_time=None,
                    error_codes=get_device_alarm.description if get_device_alarm else 0,
                    description=get_device_alarm.description if get_device_alarm else None)
    else:
        res="request data is empty"

            

    api_response=serialize(res)
    # return (reading)
    print(api_response)
    params=str(x)

    webhook_call_history=WebhookCallHistory(api=api,call_method=call_method,params=params,ip=ip,status=1,api_response=api_response,datetime=datetime.now(settings.tz_NY))
    db.add(webhook_call_history)
    db.commit()
    # delete_data=db.query(WebhookCallHistory).filter(WebhookCallHistory.datetime<=pre_10_day).delete()
    # db.commit()
    return res
    
# from app.models import Parameter
# @router.post("/create_table_parameter")
# def create_table_parameter(
#     *,
#     db: Session = Depends(deps.get_db),parameter_name:str=Form(None),parameter_number:int=Form(None)):
#     insert_data=Parameter(parameter_name=parameter_name,
#                             parameter_number=parameter_number,status=1)
#     db.add(insert_data)
#     db.commit()


@router.post("/create_table")
def create_table(
    *,
    db: Session = Depends(deps.get_db),metric_code:str=Form(None),metric:str=Form(None),
    data_types:str=Form(None),units:str=Form(None)):
    insert_data=MetricTable(metric_code=metric_code,metric=metric,data_types=data_types,
                            units=units,status=1)
    db.add(insert_data)
    db.commit()

        
#delete device
@router.delete("/delete_location_data")
def delete_location_data(
        *,
        db: Session = Depends(deps.get_db),
        location_id: List = Form(..., description="integer Eg: 1,2"),
        from_date:datetime=Form(...),to_date:datetime=Form(None),
        current_user: models.User = Depends(deps.get_current_user)):

    from_date=(from_date).strftime("%Y-%m-%d 23:59:59")
    to_date=(to_date).strftime("%Y-%m-%d 23:59:59")
    location_ids = location_id[0].split(',')
    if current_user.user_type == 1: 

        get_location = crud.location.get(db, id=location_id)

        if not get_location:

            raise HTTPException(status_code=404, detail="Location not found")

        delete_location_data=db.query(LocationData).filter(LocationData.id.in_(location_ids),LocationData.created_at >= from_date,LocationData.created_at <= to_date).delete()

        db.commit()
        reply= "Deleted succesfully"
       
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    
    return reply

@router.post("/chart")
def chart(
    *,current_user: models.User = Depends(deps.get_current_user),
    db: Session = Depends(deps.get_db),location_id:int=Form(...),
    period:int=Form(None,description="1->2hrs,2->8hrs,3->12hrs"),
    type:int=Form(...,description="1->level,2->volume")):
    
    if current_user:
        check_location=db.query(Location).filter(Location.id==location_id,Location.status==1).first()
        if check_location:
            to_date = datetime.now(settings.tz_NY)
            
            end_date = to_date.strftime("%Y-%m-%d %H:%M:%S")

            if period == 1:        
                from_date = (to_date-timedelta(hours=2)).strftime("%Y-%m-%d %H:%M:%S")
            elif period ==2 :
                from_date = (to_date-timedelta(hours=8)).strftime("%Y-%m-%d %H:%M:%S")
            elif period == 3:
                from_date = (to_date-timedelta(hours=24)).strftime("%Y-%m-%d %H:%M:%S")
            else:
                from_date = to_date.strftime("%Y-%m-%d 00:00:00")


            location_reading=db.query(LocationData).filter(LocationData.location_id==location_id,LocationData.created_at >= from_date,LocationData.created_at <= end_date)
            
            data=location_reading.order_by(LocationData.created_at.asc()).all()
            
            
            from_date1 = to_date.strftime("%Y-%m-%d 00:00:00")

            last_data=db.query(LocationData).filter(LocationData.location_id==location_id,LocationData.created_at >= from_date1).order_by(LocationData.created_at.desc()).all()
            
            list_level_data=[]
            list_volume_data=[]
            # water_level_list=[]
            # water_volume_list=[]


            water_level=[]
            water_volume=[]

            level_count=0
            volume_count=0
            avg_level=0
            avg_volume=0
            if data:
                for row in data:
                    if type == 1:
                        list_level_data.append({"id":row.id,"location_id":row.location_id,"location_code":row.location.location_code,
                                        "water_level":row.water_level_meter,"date_time":row.created_at})
                    if type == 2:
                        list_volume_data.append({"id":row.id,"location_id":row.location_id,"location_code":row.location.location_code,
                                    "water_volume":row.water_volume_liter,"date_time":row.created_at})
                
            if last_data:
                for row2 in last_data:
                    
                    level_count+=row2.water_level_meter
                    volume_count+=row2.water_volume_liter

                    water_level.append(row2.water_level_meter)
                    water_volume.append(row2.water_volume_liter)
                avg_level=float(level_count/len(water_level))
                avg_volume=float(volume_count/len(water_volume))
            

            if type == 1:
                return ({"data":list_level_data,
                        "avg_level":avg_level,"min_level":min(water_level) if water_level else 0,"max_level":max(water_level) if water_level else 0,"latest_level":water_level[0] if water_level else 0,
                        })
            elif type == 2:
                return ({"data":list_volume_data,
                        "avg_volume":avg_volume,"min_volume":min(water_volume) if water_volume else 0,"max_volume":max(water_volume) if water_volume else 0,"latest_volume":water_volume[0] if water_volume else 0})
            else:
                raise HTTPException(
                status_code=400,
                detail="Invalid request for chart type",
                )
        else:
            raise HTTPException(
                status_code=404,
                detail="Location not found",
            )
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
# from app.api.endpoints import wttest
# @router.post("/mqtt_publish")
# def mqtt_publish(msg:str=Form(...)):
    
#     wttest.run(msg)
    
# 12 00002781 0000 7ede0062 01 01 c351 00018ab50007401b37d800018ab6000f4379b15600018ab7000e41c9098900018ab5001e3d9d49e600018ab800020000000000000000d708




# 10 00002781 0000 7eda8041 01 00 01 00018ab5 0007 401ac747 
# 00018ab6 000f 43707dde 00018ab7 000e 41c92a99 00018ab5  001e 3d99a4c5 
# 00018ab8 0002 00000000 43f00000 76c7




    # return reading
# 01 002a 000130c0 00002778 0001 00018ab5 0007 00018ab6 000f 00018ab7 000e 00018ab5 001e 00018ab8 0002 6d9e
# "01","00","2a","00"3,"01","30","c0"6,"00","00","27","78"10,"00","01"12,"00","01","8a","b5","00","07","00","01",
# "8a","b6","00","0f","00","01","8a","b7","00", "0e","00","01","8a","b5","00","1e","00","01","8a","b8","00",
# "02","6d","9e"
# 81 0026 0001e1d2 00002781 7ee6c000 3e28d965 434bc1f7 41debc6f 3bad31bb 00000000 00000000 5353

# 11 00002778 7ee6cc00 0011 0010 0012 0010 0013 0059 0014 0e09 000e 41d47c11 000f 00000000 f9d9