from typing import Any, List,Optional
from sqlalchemy import or_,and_, desc, asc
from fastapi import APIRouter, Depends, HTTPException
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session, load_only
from app.utils import common_date, get_timestamp
from app.models import User, Location,TimeSettingLog,DeviceSetting,DeviceTimeSetting,LocationSettingLog, LocationSetting, TimeSettingLog, TimeSetting,UserLocationMapping
from app import crud, schemas, models
from app.api import deps
from app.core.config import settings
from datetime import datetime
from fastapi_pagination import Page, add_pagination, paginate 

router = APIRouter()


# create setting  
@router.post("/create_setting/{location_id}")
async def create_setting(
    
    db: Session = Depends(deps.get_db), *,
    location_id :int,
    current_user: User = Depends(deps.get_current_user),
    location_setting: schemas.CreateSetting,

) -> Any:
    if current_user.user_type == 1 or current_user.user_type == 2:
        location_setin = jsonable_encoder(location_setting)    
        time_set = location_setin.pop('time_setting') 
        running_hrs=location_setin["allowed_running_hrs"]
        # return time_set
        total_time = 0
        if not time_set:
            raise HTTPException(
                    status_code=400,
                    detail="Fill the time window",
                )

        else:
            tot_time=datetime.strptime('00:00:00',"%H:%M:%S")
            for i in range(0, len(time_set)):
                tdelta = datetime.strptime(time_set[i]['to_time'], "%H:%M:%S") - datetime.strptime(time_set[i]['from_time'], "%H:%M:%S")
                tot_time+=tdelta

                if not time_set[i]['from_time'] or not time_set[i]['to_time']:
                    raise HTTPException(
                            status_code=400,
                            detail="Time data is missing",
                        )
                
                if get_timestamp(time_set[i]['from_time']) > get_timestamp(time_set[i]['to_time']):
                        raise HTTPException(
                            status_code=400,
                            detail="Your Time window is overlapped",
                        )
                

                for j in range(i+1, len(time_set)):

                    if get_timestamp(time_set[i]['to_time']) > get_timestamp(time_set[j]['from_time']):
                        raise HTTPException(
                            status_code=400,
                            detail="Your Time window is overlapped",
                        )
        if datetime.strptime(str(tot_time), "%Y-%m-%d %H:%M:%S")<=datetime.strptime(str(running_hrs), "%H:%M:%S"):
            # print(datetime.strptime(str(tot_time), "%Y-%m-%d %H:%M:%S"),datetime.strptime(str(running_hrs), "%H:%M:%S"))
            raise HTTPException(
                        status_code=400,
                        detail="The total running hrs of time window should be greater than or equal to allowed running hrs",
                    )

                # if time_set[i]['from_time'] and time_set[i]['to_time']:
                #     total_time += get_timestamp(time_set[i]['to_time']) - get_timestamp(time_set[i]['from_time'])
                    # print(total_time)
        else:
            check_location = db.query(Location).filter(Location.id == location_id, Location.status != -1).scalar()
            if check_location:
                # if check_location.status == 0:
                #     raise HTTPException(
                #     status_code=400,
                #     detail="Location is inactive",
                # )
                if location_setting.high_water_level > check_location.height:
                    raise HTTPException(
                        status_code=400,
                        detail="Water level height is overflow",
                    )

                locatn_setting = db.query(LocationSetting).filter_by(status = 1, location_id = location_id).all()
                # return locatn_setting
                time_setting = db.query(TimeSetting).filter_by(status = 1, location_id = location_id).all()
                if locatn_setting or time_setting:
                    raise HTTPException(
                        status_code=400,
                        detail="Location setting is already set",
                    )
                    
                setting = crud.location_setting.create(db,current_user=current_user.id ,location_id = location_id, location_data = location_setin, time_data = time_set)
                location_setting={}
                time_setting=[]
                
                location_setting.update({"location_setting_id":setting.id,"location_id":setting.location_id,"pump1_waterflow_per_min":setting.pump1_waterflow_per_min,
                                        "pump2_waterflow_per_min":setting.pump2_waterflow_per_min,"allowed_running_hrs":setting.allowed_running_hrs,"low_water_level":setting.low_water_level,
                                        "high_water_level":setting.high_water_level,"preset_water_level":setting.preset_water_level,"remote_contact_no":setting.remote_contact_no,
                                        "contact_email":setting.contact_email})
                
                time_setting=db.query(TimeSetting).filter(TimeSetting.location_id == location_id,TimeSetting.status == 1).all()
                
                location_setting.update({"time_setting":time_setting})

                # mqtt_call=deps.setting_call(db=db,location_id=location_id,operation='getdevicesettings')
                
                return location_setting
            else:
                raise HTTPException(
                        status_code=404,
                        detail="Location not found",
                    )
                

    # return "Location setting created successfully"

# #  Update setting
# @router.post("/update_setting/{location_id}")
# async def update_setting(
    
#     db: Session = Depends(deps.get_db), *,
#     location_id :int,
#     current_user: User = Depends(deps.get_current_user),
#     location_setting: schemas.UpdateSetting,

# ) -> Any:
    
#     if current_user.user_type == 1 or current_user.user_type == 2:
#         location_setin = jsonable_encoder(location_setting)    
#         time_set = location_setin.pop('time_setting') 
#         running_hrs=location_setin.pop("allowed_running_hrs")
        
#         total_time = 0
#         if not time_set:
#             raise HTTPException(
#                     status_code=400,
#                     detail="Fill the time window",
#                 )

#         else:

#             for i in range(0, len(time_set)):
#                 tot_time=datetime.strptime('00:00:00',"%H:%M:%S")
#                 for i in range(0, len(time_set)):
#                     tdelta = datetime.strptime(time_set[i]['to_time'], "%H:%M:%S") - datetime.strptime(time_set[i]['from_time'], "%H:%M:%S")
#                     tot_time+=tdelta

#                 if not time_set[i]['from_time'] or not time_set[i]['to_time']:
#                     raise HTTPException(
#                             status_code=400,
#                             detail="Time data is missing",
#                         )
                
#                 if get_timestamp(time_set[i]['from_time']) > get_timestamp(time_set[i]['to_time']):
#                         raise HTTPException(
#                             status_code=400,
#                             detail="Your Time window is overlapped",
#                         )
                

#                 for j in range(i+1, len(time_set)):

#                     if get_timestamp(time_set[i]['to_time']) > get_timestamp(time_set[j]['from_time']):
#                         raise HTTPException(
#                             status_code=400,
#                             detail="Your Time window is overlapped",
#                         )
                
#                 # if time_set[i]['from_time'] and time_set[i]['to_time']:
#                 #     total_time += get_timestamp(time_set[i]['to_time']) - get_timestamp(time_set[i]['from_time'])
#                     # print(total_time)
#         if datetime.strptime(str(tot_time), "%Y-%m-%d %H:%M:%S")>datetime.strptime(str(running_hrs), "%H:%M:%S"):
#             # print(datetime.strptime(str(tot_time), "%Y-%m-%d %H:%M:%S"),datetime.strptime(str(running_hrs), "%H:%M:%S"))
#             raise HTTPException(
#                         status_code=400,
#                         detail="The total running hrs of time window is greater than allowed running hrs",
#                     )
#         else:
#             check_location = db.query(Location).filter(Location.id == location_id, Location.status != -1).scalar()

#             if check_location:

#                 locatn_setting = db.query(LocationSetting).filter_by(status = 1, location_id = location_id).scalar()
#                 time_setting = db.query(TimeSetting).filter_by(status = 1, location_id = location_id).all()
#                 if locatn_setting or time_setting:

#                     if location_setting.high_water_level > check_location.height:
#                         raise HTTPException(
#                             status_code=400,
#                             detail="Water level height is overflow",
#                         )
                    
#                     location_setin['last_updated_at'] = datetime.now(settings.tz_NY)
#                     location_setin['last_updated_by'] = current_user.id

#                     update_location_setting = crud.location_setting.update(db=db, db_obj=locatn_setting, obj_in=location_setin)
#                     # return update_location_setting
#                     create_setting_log = LocationSettingLog(new_low_water_level=update_location_setting.low_water_level,
#                                                 new_high_water_level=update_location_setting.high_water_level,
#                                                 new_allowed_running_hrs=update_location_setting.allowed_running_hrs,
#                                                 new_preset_water_level=update_location_setting.preset_water_level,
#                                                 new_remote_contact_no=update_location_setting.remote_contact_no,
#                                                 new_contact_email=update_location_setting.contact_email,
#                                                 new_pump1_waterflow_per_min=update_location_setting.pump1_waterflow_per_min,
#                                                 new_pump2_waterflow_per_min=update_location_setting.pump2_waterflow_per_min,
                                                
#                                                 old_low_water_level=locatn_setting.low_water_level,
#                                                 old_high_water_level=locatn_setting.high_water_level,
#                                                 old_allowed_running_hrs=locatn_setting.allowed_running_hrs,
#                                                 old_preset_water_level=locatn_setting.preset_water_level,
#                                                 old_remote_contact_no=locatn_setting.remote_contact_no,
#                                                 old_contact_email=locatn_setting.contact_email,
#                                                 old_pump1_waterflow_per_min=locatn_setting.pump1_waterflow_per_min,
#                                                 old_pump2_waterflow_per_min=locatn_setting.pump2_waterflow_per_min,
                                                
#                                                 location_id = location_id, 
#                                                 location_setting_id = update_location_setting.id, 
#                                                 status = 1, 
#                                                 created_at=datetime.now(settings.tz_NY),
#                                                 created_by=locatn_setting.created_by,
#                                                 updated_by=current_user.id,
#                                                 )
#                     db.add(create_setting_log)
#                     db.commit()

#                     # for set in time_set:
#                     get_time_in = [tim['time_setting_id'] for tim in time_set]
#                     time_set_id = [tim.id for tim in time_setting]
#                     matched_list = set(get_time_in) & set(time_set_id)
#                     diff_list = set(get_time_in) - (set(time_set_id))

#                     for setting in time_set:
                        
#                         if matched_list:  
                            
#                             if setting['time_setting_id']:
#                                 old_time_set=db.query(TimeSetting).filter(TimeSetting.id == setting['time_setting_id'],TimeSetting.status==1).scalar()
#                                 old_from_time=old_time_set.from_time
#                                 old_to_time=old_time_set.to_time

#                                 update_time_set = db.query(TimeSetting).filter(TimeSetting.id == setting['time_setting_id']).update({'from_time': setting['from_time'], 'to_time': setting['to_time'],'last_updated_at':datetime.now(settings.tz_NY),'last_updated_by':current_user.id})
#                                 db.commit()

#                                 update_logs = TimeSettingLog(new_from_time = setting['from_time'],old_from_time = old_from_time,updated_by=current_user.id, new_to_time = setting['to_time'], old_to_time = old_to_time, created_at=datetime.now(settings.tz_NY), status = 1, location_id = location_id, time_setting_id  = setting['time_setting_id'])
#                                 db.add(update_logs)
#                                 db.commit()

#                             else:
#                                 create_new_time = TimeSetting(location_id=location_id, from_time = setting['from_time'], to_time = setting['to_time'], created_by=current_user.id,created_at=datetime.now(settings.tz_NY), status = 1)
#                                 db.add(create_new_time)
                                
#                                 update_setting_logs = TimeSettingLog(new_from_time = setting['from_time'], new_to_time = setting['to_time'], created_by=current_user.id,created_at=datetime.now(settings.tz_NY), status = 1, location_id = location_id, time_setting_id  = create_new_time.id)
#                                 db.add(update_setting_logs)
#                                 db.commit()

#                             updated_location_setting={}
#                             updated_time_setting=[]
#                             updated_location_setting.update({"location_setting_id":update_location_setting.id,"location_id":update_location_setting.location_id,"pump1_waterflow_per_min":update_location_setting.pump1_waterflow_per_min,
#                                                 "pump2_waterflow_per_min":update_location_setting.pump2_waterflow_per_min,"allowed_running_hrs":update_location_setting.allowed_running_hrs,"low_water_level":update_location_setting.low_water_level,
#                                                 "high_water_level":update_location_setting.high_water_level,"preset_water_level":update_location_setting.preset_water_level,"remote_contact_no":update_location_setting.remote_contact_no,
#                                                 "contact_email":update_location_setting.contact_email})
                            
#                             updated_time_setting=db.query(TimeSetting).filter(TimeSetting.location_id == location_id,TimeSetting.status == 1).all()

#                             if updated_time_setting:
#                                 updated_location_setting.update({"time_setting":updated_time_setting})
#                                 reply="success"

#                         else:
                            
#                             create_new_time = TimeSetting(location_id=location_id, from_time = setting['from_time'], to_time = setting['to_time'], created_by=current_user.id,created_at=datetime.now(settings.tz_NY), status = 1)
#                             db.add(create_new_time)
                            
#                             update_setting_logs = TimeSettingLog(new_from_time = setting['from_time'], new_to_time = setting['to_time'], created_by=current_user.id,created_at=datetime.now(settings.tz_NY), status = 1, location_id = location_id, time_setting_id  = create_new_time.id)
#                             db.add(update_setting_logs)
#                             db.commit()

#                             updated_time_setting=db.query(TimeSetting).filter(TimeSetting.location_id == location_id,TimeSetting.status == 1).all()
                            
#                             if updated_time_setting:
#                                 # updated_location_setting.update({"time_setting":updated_time_setting})
#                                 reply="success"
                            
#                             # updated_location_setting.update({"time_setting":update_setting_logs})

#                 else:
#                     raise HTTPException(
#                         status_code=400,
#                         detail="Invalid request",
#                     )
                    
#             else:
#                 raise HTTPException(
#                         status_code=404,
#                         detail="Location not found",
#                     )
                
#     return reply
#     # return "Location setting updated successfully"

#  Update setting
@router.post("/update_setting/{location_id}")
async def update_setting(
    
    db: Session = Depends(deps.get_db), *,
    location_id :int,
    current_user: User = Depends(deps.get_current_user),
    location_setting: schemas.UpdateSetting,

) -> Any:
    
    if current_user.user_type == 1 or current_user.user_type == 2:
        location_setin = jsonable_encoder(location_setting)    
        time_set = location_setin.pop('time_setting') 
        running_hrs=location_setin["allowed_running_hrs"]
        time_setting_id_list=[]

        total_time = 0
        if not time_set:
            raise HTTPException(
                    status_code=400,
                    detail="Fill the time window",
                )

        else:

            for i in range(0, len(time_set)):
                tot_time=datetime.strptime('00:00:00',"%H:%M:%S")
                for i in range(0, len(time_set)):
                    tdelta = datetime.strptime(time_set[i]['to_time'], "%H:%M:%S") - datetime.strptime(time_set[i]['from_time'], "%H:%M:%S")
                    tot_time+=tdelta
                print(tot_time)

                if not time_set[i]['from_time'] or not time_set[i]['to_time']:
                    raise HTTPException(
                            status_code=400,
                            detail="Time data is missing",
                        )
                
                if get_timestamp(time_set[i]['from_time']) > get_timestamp(time_set[i]['to_time']):
                        raise HTTPException(
                            status_code=400,
                            detail="Your Time window is overlapped",
                        )
                

                for j in range(i+1, len(time_set)):

                    if get_timestamp(time_set[i]['to_time']) > get_timestamp(time_set[j]['from_time']):
                        raise HTTPException(
                            status_code=400,
                            detail="Your Time window is overlapped",
                        )
                
                # if time_set[i]['from_time'] and time_set[i]['to_time']:
                #     total_time += get_timestamp(time_set[i]['to_time']) - get_timestamp(time_set[i]['from_time'])
                    # print(total_time)
        if datetime.strptime(str(tot_time), "%Y-%m-%d %H:%M:%S")<=datetime.strptime(str(running_hrs), "%H:%M:%S"):
            # print(datetime.strptime(str(tot_time), "%Y-%m-%d %H:%M:%S"),datetime.strptime(str(running_hrs), "%H:%M:%S"))
            raise HTTPException(
                        status_code=400,
                        detail="The total running hrs of time window should be greater than or equal to allowed running hrs",
                    )

        check_location = db.query(Location).filter(Location.id == location_id, Location.status != -1).scalar()

        if check_location:

            locatn_setting = db.query(LocationSetting).filter_by(status = 1, location_id = location_id).scalar()
            time_setting = db.query(TimeSetting).filter_by(status = 1, location_id = location_id).all()
            if locatn_setting or time_setting:

                if location_setting.high_water_level > check_location.height:
                    raise HTTPException(
                        status_code=400,
                        detail="Water level height is overflow",
                    )
                
                location_setin['last_updated_at'] = datetime.now(settings.tz_NY)
                location_setin['last_updated_by'] = current_user.id

                update_location_setting = crud.location_setting.update(db=db, db_obj=locatn_setting, obj_in=location_setin)
                # return update_location_setting
                create_setting_log = LocationSettingLog(new_low_water_level=update_location_setting.low_water_level,
                                            new_high_water_level=update_location_setting.high_water_level,
                                            new_allowed_running_hrs=update_location_setting.allowed_running_hrs,
                                            new_preset_water_level=update_location_setting.preset_water_level,
                                            new_remote_contact_no=update_location_setting.remote_contact_no,
                                            new_contact_email=update_location_setting.contact_email,
                                            new_pump1_waterflow_per_min=update_location_setting.pump1_waterflow_per_min,
                                            new_pump2_waterflow_per_min=update_location_setting.pump2_waterflow_per_min,
                                            new_lpp=update_location_setting.lpp,
                                            new_amps1=update_location_setting.amps1,
                                            new_amps2=update_location_setting.amps2,

                                            old_low_water_level=locatn_setting.low_water_level,
                                            old_high_water_level=locatn_setting.high_water_level,
                                            old_allowed_running_hrs=locatn_setting.allowed_running_hrs,
                                            old_preset_water_level=locatn_setting.preset_water_level,
                                            old_lpp=locatn_setting.lpp,
                                            old_amps1=locatn_setting.amps1,
                                            old_amps2=locatn_setting.amps2,

                                            old_remote_contact_no=locatn_setting.remote_contact_no,
                                            old_contact_email=locatn_setting.contact_email,
                                            old_pump1_waterflow_per_min=locatn_setting.pump1_waterflow_per_min,
                                            old_pump2_waterflow_per_min=locatn_setting.pump2_waterflow_per_min,
                                            
                                            location_id = location_id, 
                                            location_setting_id = update_location_setting.id, 
                                            status = 1, 
                                            created_at=datetime.now(settings.tz_NY))
                db.add(create_setting_log)
                db.commit()


                # for set in time_set:
                get_time_in = [tim['time_setting_id'] for tim in time_set]
                time_set_id = [tim.id for tim in time_setting]
                matched_list = set(get_time_in) & set(time_set_id)
                diff_list = set(get_time_in) - (set(time_set_id))

                for setting in time_set:
                    
                    if matched_list:  
                        if setting['time_setting_id']:
                            time_setting_id_list.append(setting['time_setting_id'])
                            old_time_set=db.query(TimeSetting).filter(TimeSetting.id == setting['time_setting_id'],TimeSetting.status==1).scalar()
                            old_from_time=old_time_set.from_time
                            old_to_time=old_time_set.to_time

                            update_time_set = db.query(TimeSetting).filter(TimeSetting.id == setting['time_setting_id']).update({'from_time': setting['from_time'], 'to_time': setting['to_time'],'last_updated_at':datetime.now(settings.tz_NY),'last_updated_by':current_user.id})
                            db.commit()

                            update_logs = TimeSettingLog(new_from_time = setting['from_time'],old_from_time = old_from_time,updated_by=current_user.id, new_to_time = setting['to_time'], old_to_time = old_to_time, created_at=datetime.now(settings.tz_NY), status = 1, location_id = location_id, time_setting_id  = setting['time_setting_id'])
                            db.add(update_logs)
                            db.commit()

                        else:
                            create_new_time = TimeSetting(location_id=location_id, from_time = setting['from_time'], to_time = setting['to_time'], created_by=current_user.id,created_at=datetime.now(settings.tz_NY), status = 1)
                            db.add(create_new_time)
                            db.commit()

                            time_setting_id_list.append(create_new_time.id)

                            
                            update_setting_logs = TimeSettingLog(new_from_time = setting['from_time'], new_to_time = setting['to_time'], created_by=current_user.id,created_at=datetime.now(settings.tz_NY), status = 1, location_id = location_id, time_setting_id  = create_new_time.id)
                            db.add(update_setting_logs)
                            db.commit()

                        updated_location_setting={}
                        updated_time_setting=[]
                        updated_location_setting.update({"location_setting_id":update_location_setting.id,"location_id":update_location_setting.location_id,"pump1_waterflow_per_min":update_location_setting.pump1_waterflow_per_min,
                                            "pump2_waterflow_per_min":update_location_setting.pump2_waterflow_per_min,"allowed_running_hrs":update_location_setting.allowed_running_hrs,"low_water_level":update_location_setting.low_water_level,
                                            "high_water_level":update_location_setting.high_water_level,"preset_water_level":update_location_setting.preset_water_level,
                                            "lpp":update_location_setting.lpp,"amps1":update_location_setting.amps1,"amps2":update_location_setting.amps2,"remote_contact_no":update_location_setting.remote_contact_no,
                                            "contact_email":update_location_setting.contact_email})
                        
                        updated_time_setting=db.query(TimeSetting).filter(TimeSetting.location_id == location_id,TimeSetting.status == 1).all()

                        if updated_time_setting:
                            updated_location_setting.update({"time_setting":updated_time_setting})
                            reply=updated_location_setting

                    else:
                        
                        create_new_time = TimeSetting(location_id=location_id, from_time = setting['from_time'], to_time = setting['to_time'], created_by=current_user.id,created_at=datetime.now(settings.tz_NY), status = 1)
                        db.add(create_new_time)
                        db.commit()
                        time_setting_id_list.append(create_new_time.id)

                        
                        update_setting_logs = TimeSettingLog(new_from_time = setting['from_time'], new_to_time = setting['to_time'], created_by=current_user.id,created_at=datetime.now(settings.tz_NY), status = 1, location_id = location_id, time_setting_id  = create_new_time.id)
                        db.add(update_setting_logs)
                        db.commit()

                        updated_location_setting={}
                        updated_location_setting.update({"location_setting_id":update_location_setting.id,"location_id":update_location_setting.location_id,"pump1_waterflow_per_min":update_location_setting.pump1_waterflow_per_min,
                                            "pump2_waterflow_per_min":update_location_setting.pump2_waterflow_per_min,"allowed_running_hrs":update_location_setting.allowed_running_hrs,"low_water_level":update_location_setting.low_water_level,
                                            "high_water_level":update_location_setting.high_water_level,"preset_water_level":update_location_setting.preset_water_level,"remote_contact_no":update_location_setting.remote_contact_no,
                                            "contact_email":update_location_setting.contact_email})

                        updated_time_setting=db.query(TimeSetting).filter(TimeSetting.location_id == location_id,TimeSetting.status == 1).all()
                        
                        if updated_time_setting:
                            updated_location_setting.update({"time_setting":updated_time_setting})
                            reply=updated_location_setting
                        
                        # updated_location_setting.update({"time_setting":update_setting_logs})
                delete_time_setting=db.query(TimeSetting).filter(TimeSetting.location_id == location_id,TimeSetting.status == 1).filter(TimeSetting.id.notin_(time_setting_id_list)).all()
                if delete_time_setting:
                    for row in delete_time_setting:
                        # delete_time_setting_log=db.query(TimeSettingLog).filter(TimeSettingLog.time_setting_id==row.id,TimeSettingLog.status==1).update({'status':-1})
                        row.status=-1
                        db.commit()
                # mqtt_call=deps.setting_call(db=db,location_id=location_id,operation='getdevicesettings')
                
            else:
                raise HTTPException(
                    status_code=400,
                    detail="Invalid request",
                )
                
        else:
            raise HTTPException(
                    status_code=404,
                    detail="Location not found",
                )
    
    return reply
    # return "Location setting updated successfully"

#  List setting
@router.post("/list_setting/{location_id}")
async def list_setting(
    
    db: Session = Depends(deps.get_db), *,
    location_id :int,
    current_user: User = Depends(deps.get_current_user),
) -> Any:
    
    if current_user.user_type == 1 or current_user.user_type == 2:
        setting = []
        locatn_setting = db.query(LocationSetting).filter(LocationSetting.status!=-1,LocationSetting.location_id == location_id).scalar()
        time_setting = db.query(TimeSetting).filter(TimeSetting.status!=-1, TimeSetting.location_id == location_id).all()
        
        location_setting={}
        time_slot=[]
        
        if locatn_setting or time_setting:
    
            for slot in time_setting:
                time_slot.append({'time_setting_id': slot.id, 'from_time': slot.from_time, 'to_time': slot.to_time})
            
            location_setting.update({"location_setting_id":locatn_setting.id,"location_id":locatn_setting.location_id,"location_name":locatn_setting.location.name,"location_code":locatn_setting.location.location_code,"allowed_running_hrs":locatn_setting.allowed_running_hrs,
                                        "pump1_waterflow_per_min":locatn_setting.pump1_waterflow_per_min,"pump2_waterflow_per_min":locatn_setting.pump2_waterflow_per_min,
                                        "high_water_level":locatn_setting.high_water_level,"low_water_level":locatn_setting.low_water_level,"preset_water_level":locatn_setting.preset_water_level,
                                        "lpp":locatn_setting.lpp,"amps1":locatn_setting.amps1,"amps2":locatn_setting.amps2,"contact_email":locatn_setting.contact_email,
                                        "remote_contact_no":locatn_setting.remote_contact_no,"time_frequency":locatn_setting.location.time_frequency,"status":locatn_setting.status,"view_status":1,'time_setting': time_slot})
        else:
            locatn_setting = db.query(Location).filter(Location.status!=-1,Location.id == location_id).scalar()

            location_setting.update({"view_status":0,"location_id":locatn_setting.id,"location_name":locatn_setting.name,"location_code":locatn_setting.location_code})

            
    return location_setting

@router.post("/list_device_setting/{location_id}")
async def list_device_setting(
    
    db: Session = Depends(deps.get_db), *,
    location_id :int,
    current_user: User = Depends(deps.get_current_user),
) -> Any:
    today = datetime.now(settings.tz_NY)

    today_date=today.strftime("%Y-%m-%d %H:%M:%S")

    if current_user.user_type == 1 or current_user.user_type == 2:
        setting = []
        locatn_setting = db.query(DeviceSetting).filter(DeviceSetting.status!=-1,DeviceSetting.location_id == location_id).scalar()
        time_setting = db.query(DeviceTimeSetting).filter(DeviceTimeSetting.status!=-1, DeviceTimeSetting.location_id == location_id).all()
        
        location_setting={}
        time_slot=[]
        
        if locatn_setting or time_setting:
    
            for slot in time_setting:
                time_slot.append({'time_setting_id': slot.id, 'from_time': slot.from_time, 'to_time': slot.to_time})
            
            location_setting.update({"location_setting_id":locatn_setting.id,"location_id":locatn_setting.location_id,"location_name":locatn_setting.location.name,"location_code":locatn_setting.location.location_code,"allowed_running_hrs":locatn_setting.allowed_running_hrs,
                                        "pump1_waterflow_per_min":locatn_setting.pump1_waterflow_per_min,"pump2_waterflow_per_min":locatn_setting.pump2_waterflow_per_min,
                                        "high_water_level":locatn_setting.high_water_level,"low_water_level":locatn_setting.low_water_level,"preset_water_level":locatn_setting.preset_water_level,
                                        "lpp":locatn_setting.lpp,"amps1":locatn_setting.amps1,"amps2":locatn_setting.amps2,"time_frequency":locatn_setting.location.time_frequency,"status":locatn_setting.status,"created_at":common_date(locatn_setting.created_at),"view_status":1,'time_setting': time_slot})
            current_date=datetime.strptime(today_date, '%Y-%m-%d %H:%M:%S')
        
            cal_date=current_date - locatn_setting.created_at
            location_setting.update({"time_diff":str(cal_date.seconds)})
        
        else:
            locatn_setting = db.query(Location).filter(Location.status!=-1,Location.id == location_id).scalar()

            location_setting.update({"view_status":0,"location_id":locatn_setting.id,"location_name":locatn_setting.name,"location_code":locatn_setting.location_code,"time_diff":0})

            
    return location_setting



#  List setting_log
@router.post("/list_setting_log/{location_id}")
async def list_setting_log(
    
    db: Session = Depends(deps.get_db), *,
    location_id :int,
    current_user: User = Depends(deps.get_current_user),
) -> Any:
    
    if current_user.user_type == 1 or current_user.user_type == 2:
        setting = []
        locat_setting_log = db.query(LocationSettingLog).filter_by(status = 1, location_id = location_id).all()
        # time_setting_log = db.query(TimeSettingLog).filter_by(status = 1, location_id = location_id).all()

        time_slot=[]
        
        if locat_setting_log :
    
            location_setting=[]
            # for slot in time_setting_log:
            #     time_slot.append({'time_setting_id': slot.id, 'from_time': slot.from_time, 'to_time': slot.to_time})
            for row in locat_setting_log:
                location_setting.append({"location_setting_id":row.id,"location_id":row.location_id,
                                            "old_allowed_running_hrs":row.old_allowed_running_hrs,
                                            "new_allowed_running_hrs":row.new_allowed_running_hrs,
                                            "old_pump1_waterflow_per_min":row.old_pump1_waterflow_per_min,
                                            "new_pump1_waterflow_per_min":row.new_pump1_waterflow_per_min,
                                            "old_pump2_waterflow_per_min":row.old_pump2_waterflow_per_min,
                                            "new_pump2_waterflow_per_min":row.new_pump2_waterflow_per_min,
                                            "old_high_water_level":row.old_high_water_level,
                                            "new_high_water_level":row.new_high_water_level,

                                            "old_low_water_level":row.old_low_water_level,
                                            "new_low_water_level":row.new_low_water_level,

                                            "old_preset_water_level":row.old_preset_water_level,
                                            "new_preset_water_level":row.new_preset_water_level,

                                            "old_lpp":row.old_lpp,
                                            "new_lpp":row.new_lpp,

                                            "old_amps1":row.old_amps1,
                                            "new_amps1":row.new_amps1,

                                            "old_amps1":row.old_amps1,
                                            "new_amps2":row.new_amps2,
                                            
                                            "old_contact_email":row.old_contact_email,
                                            "new_contact_email":row.new_contact_email,

                                            "old_remote_contact_no":row.old_remote_contact_no,
                                            "new_remote_contact_no":row.new_remote_contact_no,
                                            
                                            "created_by":deps.get_user(db=db,user_id=row.created_by),
                                            "updated_by":deps.get_user(db=db,user_id=row.updated_by),
                                            "updated_at":common_date(row.created_at),
                                            
                                            "status":row.status,'time_setting': time_slot})
        else:
            raise HTTPException(
                status_code=404,
                detail="Not found",
            )

            
    return location_setting

#  Delete setting
@router.post("/delete_setting/{time_setting_id}")
async def delete_setting(
    db: Session = Depends(deps.get_db), *,
    time_setting_id :int,
    current_user: User = Depends(deps.get_current_user),
) -> Any:
    
    if current_user.user_type == 1 or current_user.user_type == 2:
        setting = []
        time_setting = db.query(TimeSetting).filter_by(status = 1, id = time_setting_id).scalar()
        if time_setting:
            time_setting.status = -1
            db.commit()
        else:
            raise HTTPException(
                status_code=404,
                detail="Not found",
            )

    return "time slot removed successfully"


# create customer  
@router.post("/location_dropdown")
async def location_setting(
    
    db: Session = Depends(deps.get_db), *,
    current_user: User = Depends(deps.get_current_user),

) -> Any:

    list_location = []
    location = db.query(Location).filter_by(status = 1).outerjoin(UserLocationMapping)

    if current_user.user_type == 2 or current_user.user_type == 3:
        location = location.filter(UserLocationMapping.user_id == current_user.id)

    location = location.all()
    
    for loc in location:
        list_location.append({'location_id':loc.id if loc.id else 0, 'location_code': loc.location_code if loc.location_code else "", 'location_name': loc.name if loc.name else ""})

    
    
    return list_location