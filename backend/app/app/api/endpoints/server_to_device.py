import re
from typing import Any, List,Optional
from fastapi_pagination.api import response
from sqlalchemy import Boolean, Column, Integer, String, DateTime,or_,and_, desc, asc, func
from fastapi import APIRouter, Body, Depends, HTTPException,Form,Request
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import delete
from app.utils import create_reset_key,check_phone,check_metric,hex_to_float,hex_to_date,digit_check, common_date,set_time_format,isInteger,check_pump_running
from app.core.security import get_password_hash
from app import crud, models, schemas
from app.models import WebhookCallHistory,Location,UserLocationMapping,LocationData,LocationSetting,LocationDataTimeSettingLog,TimeSetting,ReportPumpRunTime
from app.api import deps
from app.core.config import settings
from datetime import time,datetime,date,timedelta
from fastapi_pagination import Page, add_pagination, paginate
from app.core.config import settings, Page as Bpage
import datetime as dt
from phpserialize import serialize,unserialize
from .report import report_pump_run_time,water_supplied_based_on_pump_capacity,report_pump_failed_to_run,report_failure_analysis
import json
from .mqtt_publish import run
import binascii
from app.models import MetricTable,PeriodicData,PeriodicHeader,DeviceOutput,HealthStatus,NetworkStatus,DeviceAlarm
# import shlex
import binascii
import crc16
router = APIRouter()
def hex_to_crc(data):
    # hex_string = 'AA01'
    crc = crc16.crc16xmodem(binascii.unhexlify(data), 0xffff)
    res='{:04X}'.format(crc & 0xffff)
    return res

@router.post("/send_command")
async def send_command(
    db: Session = Depends(deps.get_db), *,
    location_id:int=Form(...),
    current_user: models.User = Depends(deps.get_current_user),
    cmd_type:int=Form(...,description="1->mode,2->pump,3->valve"),mode:int=Form(...,description="0->auto,1->manual,2->server"),
    running_pump:int=Form(...,description="1->pump1,2->pump2"),pump_status:int=Form(...,description="0->off,1->on"),
    valve_status:int=Form(...,description="0->close,1->open")) -> Any:

    check_location=db.query(Location).filter(Location.id==location_id,Location.status==1).first()
    
    if check_location:
        try:
            int(check_location.location_code)
        except:
            raise HTTPException(status_code=400, detail="Invalid Location Id")
        print(check_location.location_code)
        location_code=hex(int(check_location.location_code))
        print(location_code)
        location_id_hex = str(location_code[2:]).zfill(8)
        print(location_id_hex)
        
        if mode ==0:
            mode_status ="0001"
        elif mode ==1 :
            mode_status="0000"
        elif mode ==2:
            mode_status="0002"
        else:
            raise HTTPException(status_code=400, detail="Invalid mode type")

        
        if running_pump ==1 :
            running_pump_status="0001"
        elif running_pump ==2:
            running_pump_status="0002"
        else:
            raise HTTPException(status_code=400, detail="Invalid pump")

        if pump_status ==0 :
            pump_status_hex="0003"
        elif pump_status ==1:
            pump_status_hex="0002"
        else:
            raise HTTPException(status_code=400, detail="Invalid pump status")

        if valve_status ==0 :
            valve_status_hex="0003"
        elif valve_status ==1:
            valve_status_hex="0002"
        else:
            raise HTTPException(status_code=400, detail="Invalid pump status")

        if cmd_type ==1:#mode
            data=f"30{location_id_hex}003000010001{mode_status}"
            # crc=hex(crc16(data.decode("hex")))
            # crc2=(crc16.crc16xmodem(bytes(data, 'utf-8')))
            crc=hex_to_crc(data)
            raw_data=f'{data}{crc}'
        
        elif cmd_type ==2:#pump
            data=f"30{location_id_hex}0030{pump_status_hex}0001{running_pump_status}"
            # crc=hex(crc16(data.decode("hex")))
            # crc2=(crc16.crc16xmodem(bytes(data, 'utf-8')))
            crc=hex_to_crc(data)
            raw_data=f'{data}{crc}'
        
        elif cmd_type ==3:#valve
            data=f"30{location_id_hex}003000030001{valve_status_hex}"
            # crc=hex(crc16(data.decode("hex")))
            # crc2=(crc16.crc16xmodem(bytes(data, 'utf-8')))
            crc=hex_to_crc(data)
            raw_data=f'{data}{crc}'
        else:
            raise HTTPException(status_code=400, detail="Invalid command status")
        publish=run(msg=raw_data)
    else:
        raise HTTPException(status_code=400, detail="Invalid location")
    return raw_data,crc



@router.post("/read_write_command")
async def read_write_command(
    db: Session = Depends(deps.get_db), *,
    location_id:int=Form(...),
    type:int=Form(...,description="1->read,2->write"),
    current_user: models.User = Depends(deps.get_current_user),
    parameter:int=Form(...)) -> Any:
    
    check_location=db.query(Location).filter(Location.id==location_id,Location.status==1).first()
    
    if check_location:
        try:
            int(check_location.location_code)
        except:
            raise HTTPException(status_code=400, detail="Invalid Location Id")
        print(check_location.location_code)
        location_code=hex(int(check_location.location_code))
        print(location_code)
        location_id_hex = str(location_code[2:]).zfill(8)
        print(location_id_hex)
        parameter_val=hex(int(parameter+1))
        parameter_hex=str(parameter_val[2:]).zfill(4)
        if type == 1:
            data=f'20{location_id_hex}0032{parameter_hex}0001'
            crc=hex_to_crc(data)
        # elif type ==2:



        raw_data=f'{data}{crc}'
    else:
        raise HTTPException(status_code=400, detail="Invalid location")

