from fastapi.datastructures import UploadFile
from datetime import timedelta
from re import U
from typing import Any, List,Optional
from fastapi.routing import run_endpoint_function
from pydantic.networks import EmailStr
from sqlalchemy import or_
from fastapi import APIRouter, Body, Depends, HTTPException, Form,File
from fastapi.responses import JSONResponse
from fastapi.security import OAuth2PasswordRequestForm
from sqlalchemy.orm import Session
from fastapi.encoders import jsonable_encoder
from app import models, schemas, crud
import json
from app.utils import send_mail, common_date
from app.models import User,WaterTankLogo
from app.api import deps
from app.core import security
from app.core.config import settings
from app.core.security import get_password_hash
from app.schemas import Login, OtpMsg, VerifyOtp
import asyncio
from datetime import datetime
import random
import os,sys
import shutil

router = APIRouter()


@router.post("/login/access-token", response_model=schemas.Login)
def login_access_token(
        db: Session = Depends(deps.get_db), *, username: str = Form(...), password: str = Form(...), device_type: int = Form(None), push_id: str = Form(None)):
    
    
    if len(username) != 10:  
        raise HTTPException(
            status_code=400, detail="Mobile number must be 10 digits")

    user = deps.authenticate(
        db, mobile=username, password=password)
    
    if not user:
        raise HTTPException(
            status_code=400, detail="Invalid username or password")

    elif not deps.is_active(user):
        raise HTTPException(status_code=400, detail="Inactive user")

    # elif (device_type == 1 or device_type == 2) and user.user_type == 1:
    #     raise HTTPException(status_code=400, detail="User not found")

    # elif device_type == 3 and user.user_type == 2:
    #     raise HTTPException(status_code=400, detail="User not found")


    access_token_expires = timedelta(
        minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)

   

    # if device_type == 1 or device_type == 2:
    #     update_device_type = db.query(models.User).filter(models.User.id == user.id).update(
    #         {"device_type": device_type, "push_id": push_id})
    #     db.commit()


    return {
        "access_token": security.create_access_token(
            user.id, expires_delta=access_token_expires
        ),
        "token_type": "bearer",
        "username": user.username if user.username else "",
        "user_type": user.user_type if user.user_type else 0,
        "user_id": user.id if user.id else 0
    }


#Change Password
@router.post("/change_password/")
async def change_password(db:Session=Depends(deps.get_db),
                           current_user: User = Depends(deps.get_current_user),
                            *, user_in:schemas.ChangePassword):
                            
    user = crud.user.get(db, id=current_user.id)
    change_pswd = jsonable_encoder(user_in)
    if user:
        if security.verify_password(change_pswd['current_password'], user.password):
                user.password = get_password_hash(change_pswd['new_password'])
                db.commit()
        else:
            raise HTTPException(
            status_code=400,
            detail="Wrong current password",
        )
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )


    return JSONResponse(content="Password changed succesfully")


@router.post("/check_user")
async def check_user(*, db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user)):
    if current_user:
        get_user_status=db.query(User).filter(User.id==current_user.id,User.status == 1).scalar()
        if get_user_status:                
            reply_msg = 1
        else:
            reply_msg= 0
    else:
        reply_msg= 1

    return ({"msg":reply_msg})


# forgot Password
@router.post("/forgot_password")
async def forgot_password(db: Session = Depends(deps.get_db),
                                   mobile: int = Form(...)):
    user = db.query(User).filter(User.mobile_no == mobile, User.status == 1)
    check_user = user.first()
    if check_user:
        if check_user.mobile_no:

            # if check_user.otp_verified_status == 0:
            #     raise HTTPException(
            #         status_code=201,
            #         detail="Your account verification is pending. So, Verify your Account.",
            #     )

            otp, reset, created_at, expire_time, expire_at, otp_valid_upto = deps.get_otp()
            message = f"""Hi, \n\n{otp} is the OTP to reset your password for Water level Montitoring account. \n\n Note : This OTP is valid upto {otp_valid_upto} \n\n Regards, \n Administration team, \n Water level Montitoring."""
            reset_key = f'{reset}{check_user.id}H3dfAd4jR87uI@'
            # otp2 = random.randint(1111,9999)
            otp2=1234
            user = user.update({"otp": otp2, "otp_created_at": created_at,
                               "otp_expired_at": expire_at, "reset_key": reset_key})
            db.commit()

            mbl_no = f"+91{mobile}"

            # response = deps.send_msg(message, mobile)
            # await asyncio.get_event_loop().run_in_executor(
            #     None, deps.send_sms, mbl_no, message)
            # if response and response['return']:
            return ({"reset_key": reset_key, "msg": f"An OTP message has been sent to {mobile}"})

            # else:
            #     raise HTTPException(
            #         status_code=400,
            #         detail=response['message'],
            #     )

        else:
            raise HTTPException(
                status_code=400,
                detail="Sorry. This account does not have mobile number. Please contact administrator",
            )

    else:
        raise HTTPException(
            status_code=400,
            detail="Sorry. The requested account not found",
        )


@router.post("/reset_forgot_password")
async def reset_forgot_password(db: Session = Depends(deps.get_db), *, 
                                reset_password : schemas.ResetPassword
                                ):
    user = db.query(User).filter(User.reset_key == reset_password.reset_key, User.status == 1)
    check_user = user.first()
    
    if check_user:
        now = datetime.datetime.now()
        # return check_user.otp_expired_at,now
        if now <= check_user.otp_expired_at:
            if reset_password.otp == check_user.otp:
                user = user.update({"otp": None, "otp_created_at": None, "otp_expired_at": None,
                                    "reset_key": None, "password": get_password_hash(reset_password.password)})

                db.commit()
                return "Your password has been updated successfully!"

            else:
                raise HTTPException(
                    status_code=400,
                    detail="Sorry! Invalid OTP!",
                )
        else:
            raise HTTPException(
                status_code=400,
                detail="Sorry! The OTP has been expired! Please try again",
            )

    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request!",
        )


@router.post("/reset_password")
async def reset_password(current_user: User = Depends(deps.get_current_user),db: Session = Depends(deps.get_db), *,user_id:int=Form(...),password:str=Form(...)):
    
    if current_user.user_type==1:
        user=db.query(User).filter(User.id==user_id,User.status!=-1).first()
        if user:
            user.password=(get_password_hash(password))
            db.commit()
            return "Your password has been updated successfully!"
        else:
            raise HTTPException(
                status_code=404,
                detail="User not found!",
            )
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request!",
        )


@router.post("/resend_otp", response_model=OtpMsg)
async def resend_otp(db: Session = Depends(deps.get_db),
                                   mobile: int = Form(...)):
    user = db.query(User).filter(User.mobile_no == mobile, User.status == 1)
    check_user = user.first()
    if check_user:
        if check_user.mobile_no:


            otp, reset, created_at, expire_time, expire_at, otp_valid_upto = deps.get_otp()
            message = f"""Hi, \n\n{otp} is the OTP to reset your password for Water level Montitoring account. \n\n Note : This OTP is valid upto {otp_valid_upto} \n\n Regards, \n Administration team, \n Water level Montitoring."""
            reset_key = f'{reset}{check_user.id}H3dfAd4jR87uI@'
            otp2 = 1234
            # otp2=random.randint(1111,9999)
            user = user.update({"otp": otp2, "otp_created_at": created_at,
                               "otp_expired_at": expire_at, "reset_key": reset_key})
            db.commit()

            mbl_no = f"+91{mobile}"

            return ({"reset_key": reset_key, "msg": f"An OTP message has been sent to {mobile}"})


        else:
            raise HTTPException(
                status_code=400,
                detail="Sorry. This account does not have mobile number. Please contact administrator",
            )

    else:
        raise HTTPException(
            status_code=400,
            detail="Sorry. The requested account not found",
        )

@router.post("/add_logo")
async def add_logo(*, db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user),
                    image:UploadFile=File(...),notes:str=Form(None)):
    if current_user:

        base_dir = f"{settings.BASE_UPLOAD_FOLDER}/logo"
        base_dir_2 = f"{settings.BASE_UPLOAD_FOLDER_LIVE}/logo"

        try:
            os.makedirs(base_dir, mode=0o777, exist_ok=True)
        except OSError as e:
            sys.exit("Can't create {dir}: {err}".format(
                dir=base_dir, err=e))

        output_dir = base_dir + "/"
        output_dir_2 = base_dir_2 + "/"

        
        filename=image.filename
        
        save_full_path=f'{output_dir}{filename}'
        save_full_path_2=f'{output_dir_2}{filename}'

        
        with open(save_full_path, "wb") as buffer:
            shutil.copyfileobj(image.file, buffer)

        # Insert Logo
        insert_logo=WaterTankLogo(image=save_full_path_2,notes=notes,created_at=datetime.now(settings.tz_NY),status=1)
        db.add(insert_logo)
        db.commit()
    return "success"



@router.post("/edit_logo")
async def edit_logo(*, db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user),
                    logo_id:int=Form(...),image:UploadFile=File(None),notes:str=Form(None)):
    
    if current_user.user_type == 1:
        check_logo =db.query(WaterTankLogo).filter(WaterTankLogo.id == logo_id,WaterTankLogo.status == 1).scalar()
        if check_logo:
            if image:
                base_dir = f"{settings.BASE_UPLOAD_FOLDER}/logo"
                base_dir_2 = f"{settings.BASE_UPLOAD_FOLDER_LIVE}/logo"

                try:
                    os.makedirs(base_dir, mode=0o777, exist_ok=True)
                except OSError as e:
                    sys.exit("Can't create {dir}: {err}".format(
                        dir=base_dir, err=e))

                output_dir = base_dir + "/"
                output_dir_2 = base_dir_2 + "/"

                
                filename=image.filename

                
                save_full_path=f'{output_dir}{filename}'
                save_full_path_2=f'{output_dir_2}{filename}'

                
                with open(save_full_path, "wb") as buffer:
                    shutil.copyfileobj(image.file, buffer)
            else:
                save_full_path=check_logo.image

            check_logo.image=save_full_path_2
            check_logo.notes=notes
            db.commit()

        else:
            raise HTTPException(
            status_code=400,
            detail="Invalid request",
            )
    else:
        raise HTTPException(
        status_code=400,
        detail="Invalid request",
        )

    return "success"


@router.post("/view_logo")
async def view_logo(*, db: Session = Depends(deps.get_db)):
    
    view_logo=db.query(WaterTankLogo).filter(WaterTankLogo.status == 1).order_by(WaterTankLogo.created_at.desc()).first()
    logo_view={}
    # if view_logo:
    logo_view.update({"id":view_logo.id if view_logo else None,"image":f"{settings.BASE_DOMAIN}{view_logo.image}" if view_logo else None,"notes":view_logo.notes if view_logo else None,"status":view_logo.status if view_logo else None})
    return logo_view