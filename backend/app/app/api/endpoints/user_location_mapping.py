from typing import Any, List, Mapping,Optional
from sqlalchemy import Boolean, Column, Integer, String, DateTime,text,or_,and_, desc, asc
from fastapi import APIRouter, Body, Depends, HTTPException,Form,File,UploadFile,Response
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session,load_only
from starlette.types import Message
from app.models import User,Location,UserLocationMapping,UserLocationMappingLog
from app.schemas import ListMapping
from app.schemas import ListUser,UserCreate,ViewByUser,ListUserDrop
from app.core.config import settings
from app.core.config import settings, Page as Bpage

from app import crud, models, schemas
from app.api import deps
from datetime import datetime
from fastapi_pagination import Page, add_pagination, paginate


router = APIRouter()

# create mapping
@router.post("/create_mapping")
async def create_mapping(*,db: Session = Depends(deps.get_db),user_in: schemas.CreateMapping = None,
                current_user: models.User = Depends(deps.get_current_user))-> Any:
    mapping = jsonable_encoder(user_in)
    location_id=mapping["location_id"]
    if current_user.user_type == 1:
        if location_id:
            location=db.query(Location).filter(Location.id.in_(location_id),Location.status==1).all()
            if not location:
                    raise HTTPException(
                    status_code=404,
                    detail="Location not found",
                )
            else:   
                check_mapping=db.query(UserLocationMapping).filter(UserLocationMapping.location_id.in_(location_id),UserLocationMapping.user_id==mapping['user_id'],UserLocationMapping.status==1).first()
                if check_mapping:
                    raise HTTPException(
                    status_code=400,
                    detail=f"This User already assigned with {check_mapping.location.name}",
                )
                else:
                    maping = crud.user_location_mapping.create(db, obj_in=mapping,current_user=current_user.id)
                    if mapping:
                        reply="Succesfully assigned"
        else:
            raise HTTPException(
            status_code=400,
            detail="Invalid request",
            )

    elif current_user.user_type==2:
        check_mapping=db.query(UserLocationMapping).filter(UserLocationMapping.user_id==current_user.id,UserLocationMapping.status==1).first()

        if check_mapping.access_type==2:
            raise HTTPException(
            status_code=400,
            detail="Invalid request",
            )
        else:
            location=db.query(Location).filter(Location.id.in_(location_id),Location.status==1).all()
            if not location:
                    raise HTTPException(
                    status_code=404,
                    detail="Location not found",
                )
            else:
                maping = crud.user_location_mapping.create(db, obj_in=mapping,current_user=current_user.id)
                if mapping:
                    reply="Succesfully assigned"
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    return reply


# list mapping
@router.post("/list_mapping", response_model = Bpage[ListMapping]) 
async def list_mapping(db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user),location_code:Optional[str] = Form(None),
                username:Optional[str] = Form(None), location_name:Optional[str] = Form(None),
                mobile_no:Optional[str] = Form(None),
                access_type:Optional[str] = Form(None),
    order_by:Optional[str]=Form(None), order:Optional[str]=Form(None)):

    if current_user:
        if current_user.user_type==1:
            mapping=db.query(UserLocationMapping).filter(UserLocationMapping.status != -1)
        if current_user.user_type==2:
            mapping=db.query(UserLocationMapping.location_id).filter(UserLocationMapping.status == 1,UserLocationMapping.user_id==current_user.id)
            r=[]
            for row in mapping:
                r.append(row[0])
            # return r
            mapping=db.query(UserLocationMapping).filter(UserLocationMapping.location_id.in_(r),UserLocationMapping.status != -1).filter(UserLocationMapping.user_id!=current_user.id)
            
        data=[]
        if location_code:
            mapping = mapping.outerjoin(Location).filter(Location.location_code.like("%"+location_code+"%"))
        if location_name:
            mapping = mapping.outerjoin(Location).filter(Location.name.like("%"+location_name+"%"))
        
        if username:
            mapping=mapping.outerjoin(User).filter(User.username.like("%"+username+"%"))
        if mobile_no:
            mapping=mapping.outerjoin(User).filter(User.mobile_no.like("%"+mobile_no+"%"))
        if access_type:
            mapping=mapping.filter(UserLocationMapping.access_type.like("%"+access_type+"%"))
        if order_by:

            field, table = ["username", User] if order_by == "username" else ["mobile_no", User] if order_by == "mobile_no" else ["location_code", Location] if order_by == "location_code" else ["name", Location] if order_by == "location_name" else [order_by, UserLocationMapping]

            if table != UserLocationMapping:
                mapping = mapping.outerjoin(table)
            
            
            if order == "desc":
                mapping = mapping.order_by(desc(getattr(table, field)))
            else:
                mapping = mapping.order_by(asc(getattr(table, field)))


        else:
            mapping = mapping.order_by(UserLocationMapping.id.desc())
        
        
        mapping=mapping.all()

        for row in mapping:
            values = {"mapping_id": row.id,"user_id": row.user_id if row.user_id else 0,"mobile_no": row.user.mobile_no if row.user.mobile_no else 0, "location_id": row.location_id if row.location_id else None,"location_code":row.location.location_code,"location_name":row.location.name,"access_type": row.access_type if row.access_type else 0,"username":row.user.username, "status": row.status if row.status else 0 }
            
            data.append(values)
        
        reply=paginate(data)

    else:
        raise HTTPException(
                status_code=400,
                detail="inactive user",
            )

    return reply


# update mapping, response_model = ListMapping
@router.put("/update_mapping/{mapping_id}", response_model = ListMapping)
async def update_mapping(*,db: Session = Depends(deps.get_db),user_in: schemas.UpdateMapping = None,mapping_id: int,
                current_user: models.User = Depends(deps.get_current_user))-> Any:
    if current_user:
        mapping = crud.user_location_mapping.get(db, id=mapping_id)
        old_user_id=mapping.user_id
        old_location_id=mapping.location_id
        old_access_type=mapping.access_type

        # return mapping
        if not mapping:  
            raise HTTPException(
                status_code=404,
                detail="mapping not found",
            )
        else:       
            mapping_data = jsonable_encoder(user_in)
            location_id=mapping_data["location_id"]
            if not location_id:
                raise HTTPException(
                status_code=400,
                detail="Invalid request",
                )    
            else:
                location=db.query(Location).filter(Location.id.in_(location_id),Location.status==1).all()
                if not location:
                        raise HTTPException(
                        status_code=404,
                        detail="Location not found",
                    )   
                else:
                    if current_user.user_type==2:
                        check_mapping=db.query(UserLocationMapping).filter(UserLocationMapping.user_id==current_user.id,UserLocationMapping.status==1).first()
                        if current_user.id==mapping.user_id:
                            raise HTTPException(
                            status_code=400,
                            detail="Invalid request",
                            )
                        else:
                            if check_mapping.access_type==2:
                                raise HTTPException(
                                status_code=400,
                                detail="Invalid request",
                                )
                            else:
                                location=db.query(Location).filter(Location.id.in_(location_id),Location.status==1).all()
                                if not location:
                                        raise HTTPException(
                                        status_code=404,
                                        detail="Location not found",
                                    )
                    if not (mapping.location_id==mapping_data['location_id'] and mapping.user_id==mapping_data['user_id'] and mapping.access_type == mapping_data['access_type']):
                        mapping.location_id=mapping_data['location_id']
                        mapping.user_id=mapping_data['user_id']
                        mapping.access_type=mapping_data['access_type']
                        mapping.last_updated_by=current_user.id
                        mapping.last_updated_at=datetime.now(settings.tz_NY)
                        mapping.created_by=mapping.created_by
                        db.commit()
                        db.refresh(mapping)
                        # return mapping
                        msg1=''
                        msg2=''
                        msg3=''
                        if old_user_id!=mapping.user_id:
                            msg1="user id changed"

                        if old_location_id!=mapping.location_id:
                            msg2="location id changed"

                        if old_access_type!=mapping.access_type:
                            msg3="access type changed"

                        mapping_log = UserLocationMappingLog(
                        user_location_mapping_id=mapping_id,
                        old_user_id=old_user_id,
                        new_user_id=mapping.user_id,

                        old_location_id=old_location_id,
                        new_location_id=mapping.location_id,

                        old_access_type=old_access_type,
                        new_access_type=mapping.access_type,
                        
                        created_at=datetime.now(settings.tz_NY),
                        updated_by=current_user.id,
                        created_by=mapping.created_by,
                        message=f'{msg1} {msg2} {msg3}',
                        status=1
                        )
                        db.add(mapping_log)
                        db.commit()
                    
                    values = {"mapping_id": mapping.id,"user_id": mapping.user_id if mapping.user_id else 0, "username":mapping.user.username if mapping.user.username else "", "mobile_no":mapping.user.mobile_no if mapping.user.mobile_no else "","location_id": mapping.location_id if mapping.location_id else None,"location_code": mapping.location.location_code if mapping.location.location_code else "","location_name": mapping.location.name if mapping.location.name else "","access_type": mapping.access_type if mapping.access_type else None, "status": mapping.status if mapping.status else 0 }

    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    return values


# delete mapping
@router.delete("/delete_mapping/{mapping_id}")
async def delete_mapping(*,db: Session = Depends(deps.get_db),mapping_id: int,
                current_user: models.User = Depends(deps.get_current_user))-> Any:
    if current_user.user_type==1:
        mapping = crud.user_location_mapping.get(db, id=mapping_id)
        
        if mapping:
            mapping.status=-1
            mapping.last_updated_at=datetime.now(settings.tz_NY)
            mapping.last_updated_by=current_user.id
            mapping_log = UserLocationMappingLog(
                                user_location_mapping_id=mapping_id,
                                created_at=datetime.now(settings.tz_NY),
                                updated_by=current_user.id,
                                created_by=mapping.created_by,
                                message="deleted",
                                status=1
                                )
            db.add(mapping_log)
            db.commit()
            reply = "Deleted successfully"
        else:
            raise HTTPException(
                status_code=404,
                detail="mapping not found",
            )
    elif current_user.user_type==2:
        mapping = crud.user_location_mapping.get(db, id=mapping_id)
        check_mapping=db.query(UserLocationMapping).filter(UserLocationMapping.user_id==current_user.id,UserLocationMapping.status==1).first()

        if mapping.access_type==2:                
            if current_user.id==mapping.user_id:
                raise HTTPException(
                status_code=400,
                detail="Invalid request",
                )
            else:
                if check_mapping.access_type==2:
                    raise HTTPException(
                    status_code=400,
                    detail="Invalid request",
                    )
                else:
                    mapping.status=-1
                    mapping.last_updated_at=datetime.now(settings.tz_NY)
                    mapping.last_updated_by=current_user.id
                    mapping_log = UserLocationMappingLog(
                                        user_location_mapping_id=mapping_id,
                                        created_at=datetime.now(settings.tz_NY),
                                        updated_by=current_user.id,
                                        created_by=mapping.created_by,
                                        message="deleted",
                                        status=1
                                        )
                    db.add(mapping_log)
                    db.commit()
                    reply = "Deleted successfully"
        else:
            raise HTTPException(
                status_code=400,
                detail="Invalid request",
            )
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    return reply


# Block user
@router.post("/block_user/{mapping_id}")
async def block_user(*,db: Session = Depends(deps.get_db),mapping_id: int,
                current_user: models.User = Depends(deps.get_current_user))-> Any:
    if current_user.user_type==1:
        mapping = crud.user_location_mapping.get(db, id=mapping_id)
        
        if mapping:
            status = 0 if mapping.status==1 else 1

            if status ==1:
                reply = "Unblocked successfully"
            else:
                reply = "Blocked successfully"

            mapping.status=status
            mapping.last_updated_at=datetime.now(settings.tz_NY)
            mapping.last_updated_by=current_user.id
            mapping_log = UserLocationMappingLog(
                                user_location_mapping_id=mapping_id,
                                created_at=datetime.now(settings.tz_NY),
                                updated_by=current_user.id,
                                created_by=mapping.created_by,
                                message=reply,
                                status=1
                                )
            db.add(mapping_log)
            db.commit()

        else:
            raise HTTPException(
                status_code=404,
                detail="mapping not found",
            )
    elif current_user.user_type==2:
        mapping = crud.user_location_mapping.get(db, id=mapping_id)
        check_mapping=db.query(UserLocationMapping).filter(UserLocationMapping.user_id==current_user.id,UserLocationMapping.status==1).first()

        if mapping.access_type==2:                
            if current_user.id==mapping.user_id:
                raise HTTPException(
                status_code=400,
                detail="Invalid request",
                )
            else:
                if check_mapping.access_type==2:
                    raise HTTPException(
                    status_code=400,
                    detail="Invalid request",
                    )
                else:
                    status = 0 if mapping.status==1 else 1
                    if status ==1:
                        reply = "Unblocked successfully"
                    else:
                        reply = "Blocked successfully"
                        
                    mapping.status=status
                    mapping.last_updated_at=datetime.now(settings.tz_NY)
                    mapping.last_updated_by=current_user.id
                    mapping_log = UserLocationMappingLog(
                                        user_location_mapping_id=mapping_id,
                                        created_at=datetime.now(settings.tz_NY),
                                        updated_by=current_user.id,
                                        created_by=mapping.created_by,
                                        message=reply,
                                        status=1
                                        )
                    db.add(mapping_log)
                    db.commit()
    
        else:
            raise HTTPException(
                status_code=400,
                detail="Invalid request",
            )
                        
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    return reply


@router.post("/user_dropdown", response_model = List[ListUserDrop]) 
async def user_dropdown(db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user),location_id:int=Form(None)):

    if current_user:
        if current_user.user_type==1:
            user=db.query(User).outerjoin(UserLocationMapping).filter(User.status != -1, User.user_type != 1)
        if current_user.user_type==2:
            mapping=db.query(UserLocationMapping.location_id).filter(UserLocationMapping.status == 1,UserLocationMapping.user_id==current_user.id)
            list_location=[]
            for row in mapping:
                list_location.append(row[0])
            # return r
            user=db.query(User).outerjoin(UserLocationMapping).filter(UserLocationMapping.location_id.in_(list_location),UserLocationMapping.status != -1)
        if location_id:
            mapping=db.query(UserLocationMapping.user_id).filter(UserLocationMapping.status == 1,UserLocationMapping.location_id==location_id)
            list_user=[]
            for row in mapping:
                list_user.append(row[0])
            user=user.filter(User.id.notin_(list_user))
      
        data=[]
        user = user.order_by(User.id.desc())
    
        
        
        user=user.all()

        for row in user:
            values = {"user_id": row.id,"username": row.username if row.username else ""}
            
            data.append(values)
        
        reply=data

    else:
        raise HTTPException(
                status_code=400,
                detail="Invalid request",
            )

    return reply


@router.post("/location_dropdown/", response_model=List[schemas.ListLocationDrop])
def location_dropdown(
                *,
                db: Session = Depends(deps.get_db),
                user_id:int=Form(None),
                current_user: models.User = Depends(deps.get_current_user)):
    
    listed_location=[]
    
    location=db.query(Location)
    
    # return location
    if current_user.user_type == 1:
        location=location.filter(Location.status != -1)
        
    else:
        location=location.filter(UserLocationMapping.user_id == current_user.id,UserLocationMapping.status == 1,Location.status != -1).outerjoin(UserLocationMapping)
    
    if user_id:
        mapping=db.query(UserLocationMapping.location_id).filter(UserLocationMapping.status == 1,UserLocationMapping.user_id==user_id)
        list_location=[]
        for row in mapping:
            list_location.append(row[0])
        location=location.filter(Location.id.notin_(list_location))
    
    
    location = location.order_by(Location.id.desc())
    
    location=location.distinct(Location.id).all()
    
    if location:
        for row in location:
            
            device_list = {"location_id":row.id if row.id else 0,
                    "location_code":row.location_code if row.location_code else "", 
                    "name":row.name if row.name else "",
                    }
            listed_location.append(device_list)
    else:
        raise HTTPException(
        status_code=404,
        detail="Data not found",
        )

    return listed_location



add_pagination(router)