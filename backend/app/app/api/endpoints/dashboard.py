from fastapi import APIRouter, Body, Depends, HTTPException,Form,File,UploadFile,Response
from typing import Any, List, Mapping,Optional
from fastapi import APIRouter, Body, Depends, HTTPException,Form,File,UploadFile,Response
from sqlalchemy.orm import Session
from app.models import User,Location,UserLocationMapping,UserLocationMappingLog,LocationData
from app.api import deps
from datetime import datetime
from fastapi_pagination import Page, add_pagination, paginate
from datetime import datetime, date, timedelta
from app.core.config import settings

import time
router = APIRouter()

# dashboard for admin , response_model=Dashboard
@router.post("/dashboard") 
async def dashboard(db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user)):
    if current_user.user_type == 1:
        today1 = datetime(datetime.today().year, datetime.today().month, datetime.today().day)
        today2 = datetime(datetime.today().year, datetime.today().month, datetime.today().day, 23, 59, 59)
        yesterday=today1 - timedelta(hours=24)
        # return today1,today2
        tot_pump_on=0
        tot_ear_qty=0
        tot_qty_supplied=0

        seven_days = datetime.now(settings.tz_NY) - timedelta(days=7)
# ,Location.ear_marked_qty
        total_locations = db.query(Location.id).filter(Location.status != -1).all()        

        total_user = db.query(User).filter(User.status != -1,User.user_type!=1).count()

        for row in total_locations:
            # return row.id
            # if row.ear_marked_qty:
            #     tot_ear_qty+=row.ear_marked_qty
            check_location=db.query(LocationData).filter(LocationData.location_id==row.id)
            check_run_location=check_location.filter(LocationData.pump_status==1,LocationData.created_at>=today1).order_by(LocationData.created_at.desc()).first()
            check_tot_qty_sup=check_location.filter(LocationData.created_at<=today1,LocationData.created_at>=yesterday).order_by(LocationData.created_at.desc()).first()
            
            if check_run_location:
                tot_pump_on+=1
            if check_tot_qty_sup:
                tot_qty_supplied+=check_tot_qty_sup.discharged_water_volume_in_day
            

        # return tot_pump_on

        user_registered_today = db.query(User).filter(User.user_type == 2, User.status != -1, User.created_at.between(today1, today2)).count()
        
        
        data = {'total_locations': len(total_locations), 'total_user':total_user, "user_registered_today": user_registered_today}

    
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    
    return data 


# @router.post("/dashboard") 
# async def dashboard(db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user)):
#     if current_user.user_type == 1:
#         today1 = datetime(datetime.today().year, datetime.today().month, datetime.today().day)
#         today2 = datetime(datetime.today().year, datetime.today().month, datetime.today().day, 23, 59, 59)
#         yesterday=today1 - timedelta(hours=24)
# # dashboard for customer
# @router.post("/list_device_customer", response_model=List[ListCustomerDevice]) 
# async def list_device_customer(db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user)):
#     if current_user.user_type == 2:
        
#         list_devices =  db.query(CustomerDeviceMapping).filter(CustomerDeviceMapping.status ==1 , CustomerDeviceMapping.admin_approved == 1).filter(or_(CustomerDeviceMapping.customer_id == current_user.customer_id, UserAccessMonitor.customer_id == current_user.customer_id)).outerjoin(UserAccessMonitor, and_(UserAccessMonitor.mapping_id == CustomerDeviceMapping.id, UserAccessMonitor.status == 1)).all()
#         device = []
        
#         if list_devices:
#             for dev in list_devices:
#                 root = 0
#                 last_reading = db.query(Reading).filter(Reading.device_id == dev.device_id, Reading.customer_id == dev.customer_id).order_by(Reading.reading_datetime.desc()).limit(1).first()
                
#                 if dev.customer_id == current_user.customer_id:
#                     root = 1

#                 device.append({'device_id':dev.device_id if dev.device_id else 0, 'device_name':dev.device.device_code if dev.device_id else "", 'customer_id':dev.customer_id if dev.customer_id else o, 'customer_name':dev.customer.customer_name if dev.customer_id else "", 'name':dev.name if dev.name else "",'location':dev.location if dev.location else "", 'root_customer':root, 'mapping_id':dev.id if dev.id else 0, 'current_capacity' :f"{last_reading.water_level_capacity}%" if last_reading and last_reading.water_level_capacity else "", "last_reading_datetime": common_date(last_reading.reading_datetime) if last_reading and last_reading.reading_datetime else "", "last_reading": f'{float(last_reading.level_in_meter)}m' if last_reading and last_reading.level_in_meter else "", "installation_date": common_date(dev.created_at, without_time =1) if dev and dev.created_at else "" })
    
#     else:
#         raise HTTPException(
#             status_code=400,
#             detail="Invalid request",
#         )
    
#     return device