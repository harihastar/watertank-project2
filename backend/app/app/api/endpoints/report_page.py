from typing import Any, List,Optional
from sqlalchemy import Boolean, Column, Integer, String, DateTime,or_,and_, desc, asc, func
from fastapi import APIRouter, Body, Depends, HTTPException,Form,Request
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.sql.expression import table
from app.utils import create_reset_key,common_date_only,common_time_only,cal_tot_hrs,check_phone,digit_check, common_date,set_time_format,isInteger,check_pump_running
from app.core.security import get_password_hash
from app import crud, models, schemas
from app.models import ReportPumpRunTime,UserLocationMapping,ReportSuppliedWater,Location,ReportFailureAnalysis,UserLocationMapping,ReportPumpFailedToRun
from app.api import deps
from app.core.config import settings
from datetime import time,datetime,date,timedelta
from fastapi_pagination import Page, add_pagination, paginate
from app.core.config import settings, Page as Bpage
import datetime as dt
from phpserialize import serialize,unserialize
from .report import report_pump_run_time,water_supplied_based_on_pump_capacity,report_failure_analysis,report_pump_failed_to_run
import os
import pathlib
from pathlib import Path
from pprint import pprint
import json
import sys
import pandas as pd
# from pdfrw import PdfWriter
# import win32com.client as win32 
import xlsxwriter

router = APIRouter()

@router.post("/r1_report")
def pump_run_time_report(*,db:Session=Depends(deps.get_db),current_user: models.User = Depends(deps.get_current_user),location_id:int=Form(...),from_date:datetime=Form(...),to_date:datetime=Form(None)
                            ,report:int=Form(None),report_date1:str=Form(None),report_date2:str=Form(None),period:str=Form(None)):
    today = datetime.utcnow().date()
    var_from_date=today.strftime("%Y-%m-%d 00:00:00")
    var_to_date=today.strftime("%Y-%m-%d 23:59:59")
    data=[]
    # print(from_date,to_date)
    if not to_date:
        to_date=var_to_date
    else:
        to_date=datetime.strptime(str(to_date),"%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d 23:59:59")
    # print(current_user)/
    if current_user.user_type==1 or current_user.user_type==2:
        if current_user.user_type==1:
            check_location=db.query(Location).filter(Location.id == location_id ).first()

        if current_user.user_type==2:
            check_location=db.query(UserLocationMapping).filter(UserLocationMapping.user_id ==current_user.id,UserLocationMapping.location_id == location_id ).first()
        
        if check_location:
            pump_run_report=db.query(ReportPumpRunTime).filter(ReportPumpRunTime.location_id==location_id,from_date<= ReportPumpRunTime.created_at,ReportPumpRunTime.created_at<=to_date,ReportPumpRunTime.status==1).all()
            # if pump_run_report:
            timelist=0

            for row in pump_run_report:
                if row.pump_running_time:

                    timelist+=(float(row.pump_running_time))
                    print("t=",timelist)
                data.append({"pump_run_report_id":row.id if row else "",
                    "location_id":row.location.location_code if row else "",
                    "location_code":row.location.location_code if row else "",
                    "pumping_station":row.location.name if row else "",
                    "date":''+'/'.join((str(common_date_only(row.created_at)).split("-"))) if row else "",
                    "pump_on_time":common_time_only(row.pump_on_time) if row else "",
                    "on_level_in_mtr":str(row.water_level_in_meter) if row else "",
                    "on_volume_in_ltr":str(row.water_volume_in_liter) if row else "",
                    "pump_off_time":common_time_only(row.pump_off_time) if row.pump_off_time else "",
                    "pump_off_water_level_in_mtr":str(row.pump_off_water_in_level) if row else "",
                    "pump_off_water_volume_in_ltr":str(row.pump_off_water_volume) if row else "",
                    "pump_running_hrs":datetime.fromtimestamp(row.pump_running_time).strftime("%H:%M:%S") if row.pump_running_time else datetime.fromtimestamp(0).strftime("%H:%M:%S"),
                    "running_pump":row.running_pump if row else "","mode":row.mode if row else ""})
            tot_running_hrs=timelist
            value=({"total_running_hrs":datetime.fromtimestamp(tot_running_hrs).strftime("%H:%M:%S") if tot_running_hrs else datetime.fromtimestamp(0).strftime("%H:%M:%S")})
            reply=data,value
            if report==6:
                return reply
            if report==1 or report ==2:
                base_dir = settings.BASE_UPLOAD_FOLDER
                try:
                    os.makedirs(base_dir, mode=0o777, exist_ok=True)
                except OSError as e:
                    sys.exit("Can't create {dir}: {err}".format(
                        dir=base_dir, err=e))

                output_dir = base_dir + "/"
                output_dir_2 = settings.BASE_UPLOAD_FOLDER_LIVE + "/"
                try:
                    os.makedirs(output_dir, mode=0o777, exist_ok=True)
                except OSError as e:
                    sys.exit("Can't create {dir}: {err}".format(
                        dir=output_dir, err=e))
                dt = str(int(datetime.utcnow().timestamp()))

                fileName = f"{output_dir}PumpRunTimeReport{dt}.xls"
                fileName_2 = f"{output_dir_2}PumpRunTimeReport{dt}.xls"


                pdf_values = [['Sl.no.','Date','Pump ON Time',
                                    'Pump ON Water Level in Mtr','PUMP ON Water Volume in Mtr3','Pump OFF Time',
                                    'Pump OFF Water Level in Mtr','PUMP OFF  Water Volume in Mtr3',
                                    'Pump Running Time Hrs','Running Pump','Mode']]

                sr_no = 0
                # return data
                if data!=[]:
                    for row in data:
                        datas = []
                        sr_no += 1
                        datas.append(str(sr_no))
                        # datas.append(str(row['location_id']))
                        # datas.append(str(row['pumping_station']))

                        t_date=row['date']
                        # if t_date:
                        #     t_date=''+'/'.join((str(t_date).split("-")))
                        
                        datas.append(str(t_date))
                        datas.append(str(row['pump_on_time']))
                        datas.append(str(row['on_level_in_mtr']))
                        datas.append(str(row['on_volume_in_ltr']))
                        datas.append(str(row['pump_off_time']))
                        datas.append(str(row['pump_off_water_level_in_mtr']))
                        datas.append(str(row['pump_off_water_volume_in_ltr']))
                        datas.append(str(row['pump_running_hrs']))
                        if row['running_pump']==1:
                            pump="Pump1"
                        if row['running_pump']==2:
                            pump="Pump2"
                        datas.append(pump)
                        
                        if row['mode']==0:
                            mode="Auto"
                        if row['mode']==1:
                            mode="Manual"
                        if row['mode']==2:
                            mode="Server"
                        
                        datas.append(mode)

                        pdf_values.append(datas)
                    workbook = xlsxwriter.Workbook(fileName)
                    worksheet = workbook.add_worksheet()
                    merge_format = workbook.add_format({
                        'bold': 1,
                        'border': 1,
                        'align': 'left',
                        'valign': 'vcenter'})
                    merge_format_row = workbook.add_format({
                        'align': 'center',
                        'valign': 'vcenter'})
                    merge_format_row2 = workbook.add_format({
                        'align': 'right',
                        'valign': 'vcenter'})
                    merge_format_head = workbook.add_format({
                        'bold': 1,
                        'border': 1,
                        'align': 'center',
                        'valign': 'vcenter',
                        'fg_color': 'yellow'})
                    worksheet.merge_range('B2:L2', f'Pump Run Time Report for the {period} of {report_date1} to {report_date2} : {str(row["location_id"]) if row else ""}-{str(row["pumping_station"]) if row else ""}',merge_format)

                    for i, l in enumerate(pdf_values):
                        i+=2
                        for j, col in enumerate(l):
                            j+=1
                            if i==2:
                                worksheet.write(i, j, col,merge_format_head)
                            else:
                                worksheet.write(i, j, col,merge_format_row)
                            if i>2:
                                if j==4 or j==5 or j==7 or j==8:
                                    worksheet.write(i, j, col,merge_format_row2)

                    # print(i,j)
                    my_format = workbook.add_format()
                    my_format.set_align('vcenter')
                    worksheet.set_column('B:XFC', 15, my_format)
                    worksheet.merge_range(i+2,1,i+2,11, f'Total Running Hours of Pump={datetime.fromtimestamp(tot_running_hrs).strftime("%H:%M:%S") if tot_running_hrs else datetime.fromtimestamp(0).strftime("%H:%M:%S")}',merge_format_row)
                    # worksheet.write(i+2,7, tot_running_hrs)
                
                    workbook.close()

                    #pdf file
                    if report==2:
                        
                        title=f'Pump Run Time Report for the {period} of {report_date1} to {report_date2} : {str(row["location_id"]) if row else ""}-{str(row["pumping_station"]) if row else ""}'
                        pathname = Path(fileName).resolve()
                        org_path=''+'/'.join((str(pathname).split("/"))[:-1])
                        FileName = f'PumpRunTimeReport{dt}'
                        excel_to_pdf=deps.xls_to_pdf(path=f'{org_path}/',FileName=FileName,title=title,report_no=1)
                        pdfname=f"{output_dir_2}{FileName}.pdf"
                        os.remove(f"{output_dir}{FileName}.csv")
                        os.remove(f"{output_dir}{FileName}.html")
                        reply = f'{settings.BASE_DOMAIN}{pdfname}'
                    else:
                        reply = f'{settings.BASE_DOMAIN}{fileName_2}'

                else:
                    raise HTTPException(
                        status_code=404,
                        detail="data not found")

        else:
            raise HTTPException(
                status_code=404,
                detail="Location not found")

    else:
        raise HTTPException(
                status_code=400,
                detail="Invalid request")
    return reply


@router.post("/r2_report")
def pump_failed_to_run_report(*,db:Session=Depends(deps.get_db),current_user: models.User = Depends(deps.get_current_user),location_id:int=Form(...),from_date:datetime=Form(...),to_date:datetime=Form(None)
            ,report:int=Form(None),report_date1:str=Form(None),report_date2:str=Form(None),period:str=Form(None)):
    today = datetime.utcnow().date()
    var_from_date=today.strftime("%Y-%m-%d 00:00:00")
    var_to_date=today.strftime("%Y-%m-%d 23:59:59")
    data=[]
    


    if not to_date:
        to_date=var_to_date
    else:
        to_date=datetime.strptime(str(to_date),"%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d 23:59:59")
    if current_user.user_type==1 or current_user.user_type==2:
        if current_user.user_type==1:
            check_location=db.query(Location).filter(Location.id == location_id ).first()

        if current_user.user_type==2:
            check_location=db.query(UserLocationMapping).filter(UserLocationMapping.user_id ==current_user.id,UserLocationMapping.location_id == location_id ).first()
        
        if check_location:
            pump_fail_run_report=db.query(ReportPumpFailedToRun).filter(ReportPumpFailedToRun.location_id==location_id,from_date<= ReportPumpFailedToRun.created_at,ReportPumpFailedToRun.created_at<=to_date).all()
            # if pump_fail_run_report:
            timelist1=0
            timelist2=0
            for row in pump_fail_run_report:
                
                if row.failed_pump==1:
                    timelist1+=1
                if row.failed_pump==2:
                    timelist2+=1

                data.append({"pump_fail_run_report_id":row.id if row else "",
                "location_id":row.location.location_code if row else "",
                "location_name":row.location.name if row else "",
                "date":''+'/'.join((str(common_date_only(row.created_at)).split("-"))) if row else "",
                "pump_fail_from":common_time_only(row.pump_fail_from_time) if row else 0,
                "pump_fail_to":"",
                "duration_of_pump_fail":0,
                "failed_pump":row.failed_pump if row else "",
                "water_level_in_mtr":str(row.water_level_in_meter) if row else "",
                "low_level":str(row.low_level) if row else "","mode":row.mode if row else ""})
            # return timelist1,timelist2
        
            duration_of_pump1=timelist1
            duration_of_pump2=timelist2

            value=({"duration_of_pump1_failure":duration_of_pump1 if duration_of_pump1 else 0,
            "duration_of_pump2_failure":duration_of_pump1 if duration_of_pump2 else 0})
            reply=data,value
            if report==6:
                return reply
            if data!=[]:
                if report==1 or report ==2:
                    base_dir = settings.BASE_UPLOAD_FOLDER
                    try:
                        os.makedirs(base_dir, mode=0o777, exist_ok=True)
                    except OSError as e:
                        sys.exit("Can't create {dir}: {err}".format(
                            dir=base_dir, err=e))

                    output_dir = base_dir + "/"
                    output_dir_2 = settings.BASE_UPLOAD_FOLDER_LIVE + "/"
                    try:
                        os.makedirs(output_dir, mode=0o777, exist_ok=True)
                    except OSError as e:
                        sys.exit("Can't create {dir}: {err}".format(
                            dir=output_dir, err=e))
                    dt = str(int(datetime.utcnow().timestamp()))

                    fileName = f"{output_dir}PumpFailedtoRunReport{dt}.xls"
                    fileName_2 = f"{output_dir_2}PumpFailedtoRunReport{dt}.xls"


                    pdf_values = [['Sl.no.','Date','Pump Fail Time',
                                    'Failed Pump', 'Water Level(in Mtr)','Mode']]

                    sr_no = 0
                    for row in data:
                        datas = []
                        sr_no += 1
                        datas.append(str(sr_no))
                        # datas.append(str(row['location_id']))
                        # datas.append(str(row['location_name']))

                        t_date=row['date']
                        # if t_date:
                        #     t_date=''+'/'.join((str(t_date).split("-")))
                        
                        datas.append(str(t_date))
                        datas.append(str(row['pump_fail_from']))
                        # datas.append(str(row['pump_fail_to']))
                        # datas.append(str(row['duration_of_pump_fail']))
                        if row['failed_pump']==1:
                            pump="Pump1"
                        if row['failed_pump']==2:
                            pump="Pump2"
                        datas.append(pump)
                        datas.append(str(row['water_level_in_mtr']))
                        # datas.append(str(row['low_level']))
                        
                        if row['mode']==0:
                            mode="Auto"
                        if row['mode']==1:
                            mode="Manual"
                        if row['mode']==2:
                            mode="Server"
                        
                        datas.append(mode)

                        pdf_values.append(datas)
                    workbook = xlsxwriter.Workbook(fileName)
                    worksheet = workbook.add_worksheet()
                    merge_format = workbook.add_format({
                        'bold': 1,
                        # 'font_size': 15,
                        'border': 1,
                        'align': 'left',
                        'valign': 'vcenter'})
                    merge_format_head = workbook.add_format({
                        'bold': 1,
                        'border': 1,
                        'align': 'center',
                        'valign': 'vcenter',
                        'fg_color': 'yellow'})
                    merge_format_row2 = workbook.add_format({
                            'align': 'right',
                            'valign': 'vcenter'})
                    merge_format_row = workbook.add_format({
                            'align': 'center',
                            'valign': 'vcenter'})
                    worksheet.merge_range('B2:H2', f'Pump Failed to Run for Programmed Hours for the {period} of {report_date1} to {report_date2}:{str(row["location_id"])}-{str(row["location_name"])}',merge_format)

                    for i, l in enumerate(pdf_values):
                        i+=2
                        for j, col in enumerate(l):
                            j+=1
                            if i==2:
                                worksheet.write(i, j, col,merge_format_head)
                            else:
                                worksheet.write(i, j, col,merge_format_row)
                            if i>2:
                                if j==7 or j==8:
                                    worksheet.write(i, j, col,merge_format_row2)
                    # print(i,j,col)/
                    my_format = workbook.add_format()
                    my_format.set_align('vcenter')
                    worksheet.set_column('B:XFC', 15, my_format)
                    worksheet.merge_range(i+2,1,i+2,6, f'Number of Pump Failure - Pump1={duration_of_pump1 if duration_of_pump1 else 0}',merge_format_row)
                    # worksheet.write(i+2,7, duration_of_pump1)
                    worksheet.merge_range(i+3,1,i+3,6, f'Number of Pump Failure - Pump2={duration_of_pump2 if duration_of_pump2 else 0}',merge_format_row)
                    # worksheet.write(i+3,7, duration_of_pump2)                    
            
                    workbook.close()
                    if report==2:
                        
                        title=f'Pump Failed to Run for Programmed Hours for the {period} of {report_date1} to {report_date2}:{str(row["location_id"])}-{str(row["location_name"])}'
                        pathname = Path(fileName).resolve()
                        org_path=''+'/'.join((str(pathname).split("/"))[:-1])
                        FileName = f'PumpFailedtoRunReport{dt}'
                        excel_to_pdf=deps.xls_to_pdf(path=f'{org_path}/',FileName=FileName,title=title,report_no=2)
                        pdfname=f"{output_dir_2}{FileName}.pdf"
                        os.remove(f"{output_dir}{FileName}.csv")
                        os.remove(f"{output_dir}{FileName}.html")
                        reply = f'{settings.BASE_DOMAIN}{pdfname}'  
                    else:
                        reply = f'{settings.BASE_DOMAIN}{fileName_2}'

            else:
                raise HTTPException(
                    status_code=404,
                    detail="data not found")
        else:
            raise HTTPException(
                status_code=404,
                detail="Location not found")

    else:
        raise HTTPException(
                status_code=400,
                detail="Invalid request")
    return reply
    

@router.post("/r3_report")
def failure_analysis_report(*,db:Session=Depends(deps.get_db),current_user: models.User = Depends(deps.get_current_user),location_id:int=Form(...),from_date:datetime=Form(...),to_date:datetime=Form(None)
        ,report:int=Form(None),report_date1:str=Form(None),report_date2:str=Form(None),period:str=Form(None)):
    today = datetime.utcnow().date()
    var_from_date=today.strftime("%Y-%m-%d 00:00:00")
    var_to_date=today.strftime("%Y-%m-%d 23:59:59")
    data=[]
    timelist1=[]
    timelist2=[]
    timelist3=[]
    timelist4=[]
    pump1_count=0
    pump2_count=0
    valve_open_count=0
    valve_close_count=0



    if not to_date:
        to_date=var_to_date
    else:
        to_date=datetime.strptime(str(to_date),"%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d 23:59:59")

    if current_user.user_type==1 or current_user.user_type==2:
        if current_user.user_type==1:
            check_location=db.query(Location).filter(Location.id == location_id ).first()

        if current_user.user_type==2:
            check_location=db.query(UserLocationMapping).filter(UserLocationMapping.user_id ==current_user.id,UserLocationMapping.location_id == location_id ).first()
        
        if check_location:
            pump_fail_analysis_report=db.query(ReportFailureAnalysis).filter(ReportFailureAnalysis.location_id==location_id,from_date<= ReportFailureAnalysis.created_at,ReportFailureAnalysis.created_at<=to_date).all()
            # if pump_fail_analysis_report:
            for row in pump_fail_analysis_report:
                if row.pump1_fail==1:
                    pump1_count+=1
                    if row.duration_of_fail:
                        timelist1.append(str(row.duration_of_fail))
                if row.pump2_fail==1:
                    pump2_count+=1
                    if row.duration_of_fail:
                        timelist2.append(str(row.duration_of_fail))
                if row.valve_open_fail==1:
                    valve_open_count+=1
                    if row.duration_of_fail:
                        timelist3.append(str(row.duration_of_fail))
                if row.valve_close_fail==1:
                    valve_close_count+=1
                    if row.duration_of_fail:
                        timelist4.append(str(row.duration_of_fail))

                data.append({"pump_fail_analysis_report_id":row.id if row else "",
                        "location_id":row.location.location_code if row else "",
                        "location_name":row.location.name if row else "",
                        "date":''+'/'.join((str(common_date_only(row.created_at)).split("-"))) if row else "",
                        "pump_fail_from":common_time_only(row.fail_from_time) if row else 0,
                        "pump_fail_to":"",
                        "duration_of_pump_fail":datetime.fromtimestamp(0).strftime("%H:%M:%S"),
                        "pump1_fail":row.pump1_fail if row else "",
                        "pump2_fail":row.pump2_fail if row else "",
                        "valve_open_fail":row.valve_open_fail if row else "",
                        "valve_close_fail":row.valve_close_fail if row else "",
                        "battery_voltage_level":row.battery_voltage_level if row else "",
                        "pump_status":row.pump_status if row else "",
                        "power_status":row.power_status if row else "",
                        "water_level_in_mtr":str(row.water_level_in_meter) if row else "",
                        "battery_voltage":str(row.battery_voltage) if row else "",
                        "ac_power_voltage":str(row.ac_power_voltage) if row else "",
                        "mode":row.mode if row else ""})
            
            # duration_of_pump1_fail=cal_tot_hrs(timelist1)
            # duration_of_pump2_fail=cal_tot_hrs(timelist2)
            # duration_of_open_fail=cal_tot_hrs(timelist3)
            # duration_of_close_fail=cal_tot_hrs(timelist4)

            value=({
            # "duration_of_pump1_fail":duration_of_pump1_fail,
            #         "duration_of_pump2_fail":duration_of_pump2_fail,
            #         "duration_of_open_fail":duration_of_open_fail,
            #         "duration_of_close_fail":duration_of_close_fail,
                        "number_of_fails_to_run_pump1":pump1_count,
                        "number_of_fails_to_run_pump2":pump2_count,
                        "number_of_fails_to_valve_open":valve_open_count,
                        "number_of_fails_to_valve_close":valve_close_count})
            reply = data,value
            if report==6:
                return reply
            if data!=[]:
                if report==1 or report == 2:
                    base_dir = settings.BASE_UPLOAD_FOLDER
                    try:
                        os.makedirs(base_dir, mode=0o777, exist_ok=True)
                    except OSError as e:
                        sys.exit("Can't create {dir}: {err}".format(
                            dir=base_dir, err=e))

                    output_dir = base_dir + "/"
                    output_dir_2 = settings.BASE_UPLOAD_FOLDER_LIVE + "/"

                    try:
                        os.makedirs(output_dir, mode=0o777, exist_ok=True)
                    except OSError as e:
                        sys.exit("Can't create {dir}: {err}".format(
                            dir=output_dir, err=e))
                    dt = str(int(datetime.utcnow().timestamp()))

                    fileName = f"{output_dir}FaliureAnalysisReport{dt}.xls"
                    fileName_2 = f"{output_dir_2}FaliureAnalysisReport{dt}.xls"


                    pdf_values = [['Sl.no.','Date','Fail Time','Pump1 Fail',
                                    'Pump2 Fail','Valve  Open Fail','Valve  Close Fail',
                                    'Pump Status',
                                    'Power status','Water Level in Mtr','Battery Voltage',
                                    'AC Power Voltage','Mode']]

                    sr_no = 0
                    for row in data:
                        datas = []
                        sr_no += 1
                        datas.append(str(sr_no))
                        # datas.append(str(row['location_id']))
                        # datas.append(str(row['location_name']))

                        t_date=row['date']
                        # if t_date:
                        #     t_date=''+'/'.join((str(t_date).split("-")))
                        
                        datas.append(str(t_date))
                        datas.append(str(row['pump_fail_from']))
                        # datas.append(str(row['pump_fail_to']))
                        # datas.append(str(row['duration_of_pump_fail']))
                        
                        if row['pump1_fail']==0:
                            pump1_fail="No"
                        if row['pump1_fail']==1:
                            pump1_fail="Yes"
                        datas.append(str(pump1_fail))
                        
                        if row['pump2_fail']==0:
                            pump2_fail="No"
                        if row['pump2_fail']==1:
                            pump2_fail="Yes"
                        datas.append(str(pump2_fail))
                        
                        if row['valve_open_fail']==0:
                            valve_open_fail="No"
                        if row['valve_open_fail']==1:
                            valve_open_fail="Yes"
                        datas.append(str(valve_open_fail))
                        
                        if row['valve_close_fail']==0:
                            valve_close_fail="No"
                        if row['valve_close_fail']==1:
                            valve_close_fail="Yes"
                        datas.append(str(valve_close_fail))

                        # if row['battery_voltage_level']==0:
                        #     battry_level="Normal"
                        # if row['battery_voltage_level']==1:
                        #     battry_level="Low"
                        # datas.append(battry_level)


                        if row['pump_status']==0:
                            pump_status="Off"
                        if row['pump_status']==1:
                            pump_status="On"
                        datas.append(pump_status)

                        if row['power_status']==0:
                            power_status="Off"
                        if row['power_status']==1:
                            power_status="On"
                        datas.append(power_status)
                        datas.append(str(row['water_level_in_mtr']))
                        datas.append(str(row['battery_voltage']))
                        datas.append(str(row['ac_power_voltage']))

                        
                        if row['mode']==0:
                            mode="Auto"
                        if row['mode']==1:
                            mode="Manual"
                        if row['mode']==2:
                            mode="Server"
                        
                        datas.append(mode)

                        pdf_values.append(datas)
                    workbook = xlsxwriter.Workbook(fileName)
                    worksheet = workbook.add_worksheet()
                    merge_format = workbook.add_format({
                        'bold': 1,
                        'border': 1,
                        'align': 'left',
                        'valign': 'vcenter'})
                    merge_format_head = workbook.add_format({
                        'bold': 1,
                        'border': 1,
                        'align': 'center',
                        'valign': 'vcenter',
                        'fg_color': 'yellow'})
                    merge_format_row2 = workbook.add_format({
                            'align': 'right',
                            'valign': 'vcenter'})
                    merge_format_row = workbook.add_format({
                            'align': 'center',
                            'valign': 'vcenter'})
                    worksheet.merge_range('B2:Q2', f'Faliure Analysis Report for the {period} of {report_date1} to {report_date2}:{str(row["location_id"])}-{str(row["location_name"])}',merge_format)

                    for i, l in enumerate(pdf_values):
                        i+=2
                        for j, col in enumerate(l):
                            j+=1
                            if i==2:
                                worksheet.write(i, j, col,merge_format_head)
                            else:
                                worksheet.write(i, j, col,merge_format_row)
                            if i>2:
                                if j==5 or j==10 or j==13 or j==14 or j==15:
                                    worksheet.write(i, j, col,merge_format_row2)

                    # print(i,j,col)
                    my_format = workbook.add_format()
                    my_format.set_align('vcenter')
                    worksheet.set_column('B:XFC', 15, my_format)
                    worksheet.merge_range(i+2,1,i+2,16, f'Number of fails to run Pump1={pump1_count}',merge_format_row)
                    # worksheet.write(i+2,7, pump1_count)
                    worksheet.merge_range(i+3,1,i+3,16, f'Number of fails to run Pump2={pump2_count}',merge_format_row)
                    # worksheet.write(i+3,7, pump2_count)
                    worksheet.merge_range(i+4,1,i+4,16, f'Number of fails to Valve Close={valve_close_count}',merge_format_row)
                    # worksheet.write(i+4,7,i+4,7, valve_open_count)
                    worksheet.merge_range(i+5,1,i+5,16, f'Number of fails to Valve Open={valve_open_count}',merge_format_row)
                    # worksheet.write(i+5,7, valve_close_count)z`
                    
            
                    workbook.close()
                    if report==2:
                        title=f'Faliure Analysis Report for the {period} of {report_date1} to {report_date2}:{str(row["location_id"])}-{str(row["location_name"])}'
                        pathname = Path(fileName).resolve()
                        org_path=''+'/'.join((str(pathname).split("/"))[:-1])
                        FileName = f'FaliureAnalysisReport{dt}'
                        excel_to_pdf=deps.xls_to_pdf(path=f'{org_path}/',FileName=FileName,title=title,report_no=3)
                        pdfname=f"{output_dir_2}{FileName}.pdf"
                        os.remove(f"{output_dir}{FileName}.csv")
                        os.remove(f"{output_dir}{FileName}.html")
                        reply = f'{settings.BASE_DOMAIN}{pdfname}' 
                    else:
                        reply = f'{settings.BASE_DOMAIN}{fileName_2}'

            else:
                raise HTTPException(
                    status_code=404,
                    detail="data not found")
    
        else:
            raise HTTPException(
                status_code=404,
                detail="Location not found")

    else:
        raise HTTPException(
                status_code=400,
                detail="Invalid request")
    return reply


@router.post("/r4_report")
def supplied_water_report(*,db:Session=Depends(deps.get_db),current_user: models.User = Depends(deps.get_current_user),location_id:int=Form(...),from_date:datetime=Form(...),to_date:datetime=Form(None),pump:int=Form(None),
                                            report:int=Form(None),report_date1:str=Form(None),report_date2:str=Form(None),period:str=Form(None)):
    today = datetime.utcnow().date()
    var_from_date=today.strftime("%Y-%m-%d 00:00:00")
    var_to_date=today.strftime("%Y-%m-%d 23:59:59")
    data=[]
    water_volume=0

    if not to_date:
        to_date=var_to_date
    else:
        to_date=datetime.strptime(str(to_date),"%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d 23:59:59")
    # print(location_id,from_date,to_date)
    if current_user.user_type==1 or current_user.user_type==2:
        if current_user.user_type==1:
            check_location=db.query(Location).filter(Location.id == location_id ).first()

        if current_user.user_type==2:
            check_location=db.query(UserLocationMapping).filter(UserLocationMapping.user_id ==current_user.id,UserLocationMapping.location_id == location_id ).first()
        if check_location:

            supplied_water_report=db.query(ReportSuppliedWater).filter(ReportSuppliedWater.location_id==location_id,from_date<= ReportSuppliedWater.created_at,ReportSuppliedWater.created_at<=to_date)
            if pump:
                supplied_water_report=supplied_water_report.filter(ReportSuppliedWater.running_pump==pump)
            
            supplied_water_report=supplied_water_report.all()
            # if supplied_water_report:
            for row in supplied_water_report:
                if row.water_volume_supplied_based_on_capacity:
                    water_volume+=float(row.water_volume_supplied_based_on_capacity)
        

                data.append({"distributed_water_report_id":row.id if row else "",
                "location_id":row.location.location_code if row else "",
                "pumping_station":row.location.name if row else "",
                "date":''+'/'.join((str(common_date_only(row.created_at)).split("-"))) if row else "",
                "pump_on_time":common_time_only(row.pump_on_time) if row else "",
                "on_level_in_mtr":str(row.water_level_in_meter) if row else "",
                "on_volume_in_ltr":str(row.water_volume_in_liter) if row else "",
                "pump1_of_time":common_time_only(row.pump_off_time) if row.pump_off_time else '',
                "pump_off_water_level_in_mtr":str(row.pump_off_water_level_in_meter) if row else "",
                "pump_off_water_level_in_ltr":str(row.pump_off_water_volume_in_liter) if row else "",
                "pump_running_time":datetime.fromtimestamp(row.pump_running_hours).strftime("%H:%M:%S") if row.pump_running_hours else 0,
                "running_pump":row.running_pump if row else "",
                "water_volume_supplied_based_on_capacity":str(row.water_volume_supplied_based_on_capacity) if row else "",
                "mode":row.mode if row else ""})

            value=({"total_volume_of_water_supplied_for_the_period":water_volume})
            reply= data,value
            if report==6:
                return reply
            if data!=[]:
                if report==1 or report ==2:
                    base_dir = settings.BASE_UPLOAD_FOLDER
                    try:
                        os.makedirs(base_dir, mode=0o777, exist_ok=True)
                    except OSError as e:
                        sys.exit("Can't create {dir}: {err}".format(
                            dir=base_dir, err=e))

                    output_dir = base_dir + "/"
                    output_dir_2 = settings.BASE_UPLOAD_FOLDER_LIVE + "/"

                    try:
                        os.makedirs(output_dir, mode=0o777, exist_ok=True)
                    except OSError as e:
                        sys.exit("Can't create {dir}: {err}".format(
                            dir=output_dir, err=e))
                    dt = str(int(datetime.utcnow().timestamp()))
                    if pump:
                        fileName = f"{output_dir}WaterDistributionReport{dt}.xls"
                        fileName_2 = f"{output_dir_2}WaterDistributionReport{dt}.xls"

                        pdf_title="WaterDistributionReport"
                    else:
                        fileName = f"{output_dir}WaterSuppliedReport{dt}.xls"
                        fileName_2 = f"{output_dir_2}WaterSuppliedReport{dt}.xls"

                        pdf_title="WaterSuppliedReport"

                    
                    if pump:
                        pdf_values = [['Sl.no.','Date','Pump ON Time',
                                        'Pump ON Water Level in Mtr','Pump ON Water Volume in Mtr3','Pump OFF Time',
                                        'Pump OFF Water Level in Mtr','Pump OFF  Water Volume in Mtr3',
                                        'Pump Running Time Hrs','Running Pump',
                                        'Water Volume Discharged Based on Capcity','Mode']]
                    else:
                        pdf_values = [['Sl.no.','Date','Pump ON Time',
                                        'Pump ON Water Level in Mtr','Pump ON Water Volume in Mtr3','Pump OFF Time',
                                        'Pump OFF Water Level in Mtr','Pump OFF  Water Volume in Mtr3',
                                        'Pump Running Time Hrs','Running Pump',
                                        'Water Volume Supplied Based on Capcity','Mode']]

                    sr_no = 0
                    for row in data:
                        datas = []
                        sr_no += 1
                        datas.append(str(sr_no))
                        # datas.append(str(row['location_id']))
                        # datas.append(str(row['pumping_station']))

                        t_date=row['date']
                        # if t_date:
                        #     t_date=''+'/'.join((str(t_date).split("-")))
                        
                        datas.append(str(t_date))
                        datas.append(str(row['pump_on_time']))
                        datas.append(str(row['on_level_in_mtr']))
                        datas.append(str(row['on_volume_in_ltr']))
                        datas.append(str(row['pump1_of_time']))
                        datas.append(str(row['pump_off_water_level_in_mtr']))
                        datas.append(str(row['pump_off_water_level_in_ltr']))
                        datas.append(str(row['pump_running_time']))
                        
                        if row['running_pump']==1:
                            running_pump="Pump1"
                        if row['running_pump']==2:
                            running_pump="Pump2"
                        datas.append(str(running_pump))
                        datas.append(str(row['water_volume_supplied_based_on_capacity']))
                                                
                        if row['mode']==0:
                            mode="Auto"
                        if row['mode']==1:
                            mode="Manual"
                        if row['mode']==2:
                            mode="Server"
                        
                        datas.append(mode)

                        pdf_values.append(datas)
                    workbook = xlsxwriter.Workbook(fileName)
                    worksheet = workbook.add_worksheet()
                    merge_format = workbook.add_format({
                        'bold': 1,
                        'border': 1,
                        'align': 'left',
                        'valign': 'vcenter'})
                    merge_format_head = workbook.add_format({
                        'bold': 1,
                        'border': 1,
                        'align': 'center',
                        'valign': 'vcenter',
                        'fg_color': 'yellow'})
                    merge_format_row2 = workbook.add_format({
                            'align': 'right',
                            'valign': 'vcenter'})
                    merge_format_row = workbook.add_format({
                            'align': 'center',
                            'valign': 'vcenter'})
                    if not pump:
                        worksheet.merge_range('B2:O2', f'Estimation of Water Supplied based on PUMP Capacity for the {period} of {report_date1} to {report_date2}:{str(row["location_id"])}-{str(row["pumping_station"])}',merge_format)
                        pdf_head=f'Estimation of Water Supplied based on PUMP Capacity for the {period} of {report_date1} to {report_date2}:{str(row["location_id"])}-{str(row["pumping_station"])}'
                    else:
                        worksheet.merge_range('B2:O2', f'Estimation of Water Distributed based on Geography for the {period} of {report_date1} to {report_date2}:{str(row["location_id"])}-{str(row["pumping_station"])}',merge_format)
                        pdf_head=f'Estimation of Water Distributed based on Geography for the {period} of {report_date1} to {report_date2}:{str(row["location_id"])}-{str(row["pumping_station"])}'

                    for i, l in enumerate(pdf_values):
                        i+=2
                        for j, col in enumerate(l):
                            j+=1
                            if i==2:
                                worksheet.write(i, j, col,merge_format_head)
                            else:
                                worksheet.write(i, j, col,merge_format_row)
                            if i>2:
                                if j==4 or j==5 or j==7 or j==8 or j==9 or j==11:
                                    worksheet.write(i, j, col,merge_format_row2)

                    # print(i,j,col)
                    my_format = workbook.add_format()
                    my_format.set_align('vcenter')
                    worksheet.set_column('B:XFC', 15, my_format)
                    worksheet.merge_range(i+2,1,i+2,11, f'Total Volume of Water Supplied for the {period} in Mtr3={water_volume}',merge_format_row)
                    # worksheet.write(i+2,7, water_volume)                 
            
                    workbook.close()
                    if report==2:
                        
                        title=pdf_head
                        pathname = Path(fileName).resolve()
                        org_path=''+'/'.join((str(pathname).split("/"))[:-1])
                        FileName = f'{pdf_title}{dt}'
                        excel_to_pdf=deps.xls_to_pdf(path=f'{org_path}/',FileName=FileName,title=title,report_no=4)
                        pdfname=f"{output_dir_2}{FileName}.pdf"
                        os.remove(f"{output_dir}{FileName}.csv")
                        os.remove(f"{output_dir}{FileName}.html")
                        reply = f'{settings.BASE_DOMAIN}{pdfname}'    
                    else:
                        reply = f'{settings.BASE_DOMAIN}{fileName_2}'


            else:
                raise HTTPException(
                    status_code=404,
                    detail="Datas not found")
        else:
            raise HTTPException(
                status_code=404,
                detail="Location not found")

    else:
        raise HTTPException(
                status_code=400,
                detail="Invalid request")
    return reply


@router.post("/r6_report")
async def comparision_report(*,db:Session=Depends(deps.get_db),current_user: models.User = Depends(deps.get_current_user),location_id:int=Form(...),from_date1:datetime=Form(...),to_date1:datetime=Form(None),from_date2:datetime=Form(...),to_date2:datetime=Form(None),report:int=Form(None),
                            report_date1:str=Form(None),report_date2:str=Form(None),period:str=Form(None)):
    today = datetime.utcnow().date()
    # var_from_date=today.strftime("%Y-%m-%d 00:00:00")
    # var_to_date=today.strftime("%Y-%m-%d 23:59:59")
    data=[]
    value=[]
    # if not to_date:
    #     to_date=var_to_date
    # else:
    #     to_date=datetime.strptime(str(to_date),"%Y-%m-%d %H:%M:%S").strftime("%Y-%m-%d 23:59:59")
    if current_user.user_type==1 or current_user.user_type==2:
        if current_user.user_type==1:
            check_location=db.query(Location).filter(Location.id == location_id ).first()

        if current_user.user_type==2:
            check_location=db.query(UserLocationMapping).filter(UserLocationMapping.user_id ==current_user.id,UserLocationMapping.location_id == location_id ).first()
        
        if check_location:
            for row in [0,1]:
                if row==0:
                    from_date=from_date1
                    to_date=to_date1
                    report_date=report_date1
                if row==1:
                    from_date=from_date2
                    to_date=to_date2
                    report_date=report_date2

                data1,value1=pump_run_time_report(db=db,current_user=current_user,location_id=location_id,from_date=from_date,to_date=to_date,report=6)
                
                data2,value2=pump_failed_to_run_report(db=db,current_user=current_user,location_id=location_id,from_date=from_date,to_date=to_date,report=6)
                data3,value3=failure_analysis_report(db=db,current_user=current_user,location_id=location_id,from_date=from_date,to_date=to_date,report=6)
                data4,value4=supplied_water_report(db=db,current_user=current_user,location_id=location_id,from_date=from_date,to_date=to_date,pump=None,report=6)
                # return data1,data2,data3,data4,value1,value2,value3,value4   
                value.append({"date":report_date,
                    "total_running_hrs":value1['total_running_hrs'] if value1['total_running_hrs'] else datetime.fromtimestamp(0).strftime("%H:%M:%S"),
                    # "duration_of_pump1_failure":value2['duration_of_pump1_failure'] if value2['duration_of_pump1_failure'] else datetime.fromtimestamp(0).strftime("%H:%M:%S"),
                    # "duration_of_pump2_failure":value2['duration_of_pump2_failure'] if value2['duration_of_pump2_failure'] else datetime.fromtimestamp(0).strftime("%H:%M:%S"),
                    "number_of_fails_to_run_pump1":value3['number_of_fails_to_run_pump1'] if value3['number_of_fails_to_run_pump1'] else '0',
                    "number_of_fails_to_run_pump2":value3['number_of_fails_to_run_pump2'] if value3['number_of_fails_to_run_pump2'] else '0',
                    "number_of_fails_to_valve_open":value3['number_of_fails_to_valve_open'] if value3['number_of_fails_to_valve_open'] else '0',
                    "number_of_fails_to_valve_close":value3['number_of_fails_to_valve_close'] if value3['number_of_fails_to_valve_close'] else '0',
                    "total_volume_of_water_supplied_for_the_period":value4['total_volume_of_water_supplied_for_the_period'] if value4['total_volume_of_water_supplied_for_the_period'] else '0'})
            reply=value
            # return reply
            if report==1 or report ==2:
                base_dir = settings.BASE_UPLOAD_FOLDER
                try:
                    os.makedirs(base_dir, mode=0o777, exist_ok=True)
                except OSError as e:
                    sys.exit("Can't create {dir}: {err}".format(
                        dir=base_dir, err=e))

                output_dir = base_dir + "/"
                output_dir_2 = settings.BASE_UPLOAD_FOLDER_LIVE + "/"

                try:
                    os.makedirs(output_dir, mode=0o777, exist_ok=True)
                except OSError as e:
                    sys.exit("Can't create {dir}: {err}".format(
                        dir=output_dir, err=e))
                dt = str(int(datetime.utcnow().timestamp()))              
                fileName = f"{output_dir}ComparisionReport{dt}.xls"
                fileName_2 = f"{output_dir_2}ComparisionReport{dt}.xls"

                
                
                pdf_values = [['Sl.no.', 'Period','Pump Running Time Hrs','Water Volume Supplied  Based on Capacity',
                                # 'Pump1 Failed to Run on Programmed Hrs','Pump2 Failed to Run on Programmed Hrs',
                                'Number of fails to run Pump1','Number of fails to run Pump2',
                                'Number of fails to Valve Close','Number of fails to Valve Open']]

                sr_no = 0
                # datas = []
                l=[]
                for row in value:
                    datas=[]

                    sr_no += 1
                    l.append(sr_no)
                    if sr_no==1:
                        t_date=report_date1
                    if sr_no==2:
                        t_date=report_date2

                    datas.append(str(sr_no))
                    # print(data)
                    # t_date=f'{common_date_only(datetime.strptime(str(row["date"]) ,"%d-%m-%Y"))}'
                    datas.append(str(t_date))
                    datas.append(str(row['total_running_hrs']))
                    datas.append(str(row['total_volume_of_water_supplied_for_the_period']))
                    
                    # datas.append(str(row['duration_of_pump1_failure']))
                    # datas.append(str(row['duration_of_pump2_failure']))
                    datas.append(str(row['number_of_fails_to_run_pump1']))
                    datas.append(str(row['number_of_fails_to_run_pump2']))
                    datas.append(str(row['number_of_fails_to_valve_open']))
                    datas.append(str(row['number_of_fails_to_valve_close']))
                    
                    pdf_values.append(datas)
                    
                workbook = xlsxwriter.Workbook(fileName)
                worksheet = workbook.add_worksheet()
                merge_format = workbook.add_format({
                    'bold': 1,
                    'border': 1,
                    'align': 'left',
                    'valign': 'vcenter'})
                merge_format_head = workbook.add_format({
                    'bold': 1,
                    'border': 1,
                    'align': 'center',
                    'valign': 'vcenter',
                    'fg_color': 'yellow'})
                merge_format_row = workbook.add_format({
                    'align': 'center',
                    'valign': 'vcenter',
                   })
                merge_format_row2 = workbook.add_format({
                    'align': 'right',
                    'valign': 'vcenter',
                   })
                worksheet.merge_range('B2:K2', f'Comparision Report for {period} for {report_date1} & {report_date2}',merge_format)

                for i, l in enumerate(pdf_values):
                    i+=2
                    # print(enumerate(l))
                    for j, col in enumerate(l):
                        # print(j)
                        j+=1
                        if i==2:
                            worksheet.write(i, j, col,merge_format_head)
                        else:
                            worksheet.write(i, j, col,merge_format_row2) 
                        if i>2:
                            if j==1 or j==2:
                                worksheet.write(i, j, col,merge_format_row)
                                       
                my_format = workbook.add_format()
                my_format.set_align('vcenter')
                worksheet.set_column('B:XFC', 15, my_format)
                worksheet.write(i+2,5,"")
                workbook.close()
                if report==2:
                    
                    title=f'Comparision Report for {period} for {report_date1} & {report_date2}'
                    pathname = Path(fileName).resolve()
                    org_path=''+'/'.join((str(pathname).split("/"))[:-1])
                    FileName = f'ComparisionReport{dt}'
                    excel_to_pdf=deps.xls_to_pdf(path=f'{org_path}/',FileName=FileName,title=title,report_no=6)
                    pdfname=f"{output_dir_2}{FileName}.pdf"
                    os.remove(f"{output_dir}{FileName}.csv")
                    os.remove(f"{output_dir}{FileName}.html")
                    reply = f'{settings.BASE_DOMAIN}{pdfname}'  
                else:
                    reply = f'{settings.BASE_DOMAIN}{fileName_2}'

            return reply
        else:
            raise HTTPException(
                status_code=404,
                detail="Location not found")
    else:
        raise HTTPException(
        status_code=400,
            detail="Invalid request")





    
#delete device
@router.delete("/delete_report")
def delete_report(
        *,
        db: Session = Depends(deps.get_db),):

    from_date="2021-12-09 00:00:00"
    to_date="2021-12-11 12:00:00"

    delete_location_data=db.query(ReportPumpRunTime).filter(ReportPumpRunTime.created_at >= from_date,ReportPumpRunTime.created_at <= to_date,ReportPumpRunTime.pump_off_time==None).delete()
    delete_location_data=db.query(ReportSuppliedWater).filter(ReportSuppliedWater.created_at >= from_date,ReportSuppliedWater.created_at <= to_date,ReportSuppliedWater.pump_off_time==None).delete()
        
    db.commit()
    reply= "Deleted succesfully"
       
    return reply



