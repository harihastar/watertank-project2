from typing import Any, List,Optional
from sqlalchemy import Boolean, Column, Integer, String, DateTime,or_,and_, desc, asc, func
from fastapi import APIRouter, Body, Depends, HTTPException,Form
from sqlalchemy.orm import Session
from app.utils import check_phone,digit_check, common_date
from app.models import Location,ReportDistributedWater,LocationSetting,ReportPumpRunTime,ReportSuppliedWater,ReportFailureAnalysis,ReportPumpFailedToRun
from fastapi_pagination import Page, add_pagination, paginate
from app.core.config import settings, Page as Bpage
import datetime as dt
from app.api import deps
from datetime import datetime, time
from datetime import timedelta

# R1 (Pump Run Time Report)

def report_pump_run_time(db:Session,data,from_date,to_date):

    if data.pump_status == 1:
        pump_on_time = data.created_at
        pump_off_time =None
    else:
        pump_on_time = None
        pump_off_time = data.created_at

    
    if data.pump1_failure_status==0 and data.pump2_failure_status==0:
        get_last_running_report=db.query(ReportPumpRunTime).filter(ReportPumpRunTime.location_id == data.location_id,
                ReportPumpRunTime.status == 1,ReportPumpRunTime.running_pump ==data.running_pump,ReportPumpRunTime.created_at>=from_date,
                ).order_by(ReportPumpRunTime.created_at.desc()).first()
        if get_last_running_report:
            if get_last_running_report.pump_off_time==None:
                if pump_on_time:
                    # get_last_running_report.pump_off_water_in_level=data.water_level_meter
                    # get_last_running_report.pump_off_water_volume=data.water_volume_liter
                    
                    # db.commit()
                    pass
                if pump_off_time:
                    t1 = (get_last_running_report.pump_on_time).timestamp()
                    t2 = pump_off_time.timestamp()
                    running_duration=t2 - t1
                    get_last_running_report.pump_running_time=running_duration
                    get_last_running_report.pump_off_time=pump_off_time
                    get_last_running_report.pump_off_water_in_level=data.water_level_meter
                    get_last_running_report.pump_off_water_volume=data.water_volume_liter
                    
                    db.commit()
            elif get_last_running_report.pump_off_time:
                if pump_on_time:    
                    pump_report=ReportPumpRunTime(location_data_id=data.id,location_id=data.location_id,
                                                pump_on_time=pump_on_time,
                                                water_level_in_meter=data.water_level_meter,
                                                water_volume_in_liter=data.water_volume_liter,
                                                pump_off_time=pump_off_time,
                                                pump_off_water_in_level=data.water_level_meter if data.pump_status == 0 else None,
                                                pump_off_water_volume=data.water_volume_liter if data.pump_status ==0 else None,
                                                running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                    db.add(pump_report)
                    db.commit()
                if pump_off_time:
                    pass
        else:
            get_pre_running_report=db.query(ReportPumpRunTime).filter(ReportPumpRunTime.location_id == data.location_id,
                    ReportPumpRunTime.status == 1,ReportPumpRunTime.running_pump ==data.running_pump,
                    ReportPumpRunTime.created_at<from_date,
                    ).order_by(ReportPumpRunTime.created_at.desc()).first()
            if get_pre_running_report:
                if get_pre_running_report.pump_off_time==None:
                    if pump_on_time:
                        off_time = datetime.strptime(get_pre_running_report.pump_on_time.strftime("%Y-%m-%d 23:59:59"),"%Y-%m-%d %H:%M:%S")#this changes(all report) for the client told that the whatsapp grp chat(sept 2 2021) 
                    
                        t1 = (get_pre_running_report.pump_on_time).timestamp()
                        t2 = off_time.timestamp()                
                        running_duration=t2 - t1
                        get_pre_running_report.pump_running_time=running_duration
                        get_pre_running_report.pump_off_time=off_time
                        get_pre_running_report.pump_off_water_in_level=None
                        get_pre_running_report.pump_off_water_volume=None
                        
                        db.commit()

                        pump_report=ReportPumpRunTime(location_data_id=data.id,location_id=data.location_id,
                                                pump_on_time=pump_on_time,
                                                water_level_in_meter=data.water_level_meter,
                                                water_volume_in_liter=data.water_volume_liter,
                                                pump_off_time=None,
                                                pump_off_water_in_level=None,
                                                pump_off_water_volume=None,
                                                running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                        db.add(pump_report)
                        db.commit()
                    if pump_off_time:
                        off_time = datetime.strptime(get_pre_running_report.pump_on_time.strftime("%Y-%m-%d 23:59:59"),"%Y-%m-%d %H:%M:%S")#this changes(all report) for the client told that the whatsapp grp chat(sept 2 2021) 
                    
                        t1 = (get_pre_running_report.pump_on_time).timestamp()
                        t2 = off_time.timestamp()                
                        running_duration=t2 - t1
                        get_pre_running_report.pump_running_time=running_duration
                        get_pre_running_report.pump_off_time=off_time
                        get_pre_running_report.pump_off_water_in_level=None
                        get_pre_running_report.pump_off_water_volume=None
                        
                        db.commit()

                        on_time = datetime.strptime(pump_off_time.strftime("%Y-%m-%d 00:00:00"),"%Y-%m-%d %H:%M:%S")
                        t1 =on_time.timestamp()
                        t2 =pump_off_time.timestamp()
                        running_duration=t2 - t1
                        pump_report=ReportPumpRunTime(location_data_id=data.id,location_id=data.location_id,pump_on_time=on_time,
                                                        water_level_in_meter=None,water_volume_in_liter=None,
                                                        pump_off_time=pump_off_time,
                                                        pump_off_water_in_level=data.water_level_meter if data.pump_status == 0 else None,
                                                        pump_off_water_volume=data.water_volume_liter if data.pump_status ==0 else None,
                                                        pump_running_time=running_duration,
                                                        running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                        db.add(pump_report)
                        db.commit()
                elif get_pre_running_report.pump_off_time:
                    if pump_on_time:    
                        pump_report=ReportPumpRunTime(location_data_id=data.id,location_id=data.location_id,
                                                    pump_on_time=pump_on_time,
                                                    water_level_in_meter=data.water_level_meter,
                                                    water_volume_in_liter=data.water_volume_liter,
                                                    pump_off_time=pump_off_time,
                                                    pump_off_water_in_level=data.water_level_meter if data.pump_status == 0 else None,
                                                    pump_off_water_volume=data.water_volume_liter if data.pump_status ==0 else None,
                                                    running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                        db.add(pump_report)
                        db.commit()
                    if pump_off_time:
                        pass
            else:
                if pump_on_time:
                    pump_report=ReportPumpRunTime(location_data_id=data.id,location_id=data.location_id,
                                                pump_on_time=pump_on_time,
                                                water_level_in_meter=data.water_level_meter,
                                                water_volume_in_liter=data.water_volume_liter,
                                                pump_off_time=pump_off_time,
                                                pump_off_water_in_level=None,
                                                pump_off_water_volume=None,
                                                running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                    db.add(pump_report)
                    db.commit()
                if pump_off_time:
                    pass

def report_pump_failed_to_run(db:Session,data,from_date,to_date):
    if data.mode==0 or data.mode==2:
        if data.pump1_failure_status==1 or data.pump2_failure_status==1:
            pump=[]
            pump_fail=0
            pump_fail=0
            if data.pump1_failure_status==1:
                pump_fail=1
            if data.pump2_failure_status==1:
                pump_fail=2
            
            pump_fail_report=ReportPumpFailedToRun(
                                                    location_data_id=data.id,
                                                    location_id=data.location_id,
                                                    # pump_fail_date=data.created_at,
                                                    pump_fail_from_time=data.created_at,
                                                    # pump_fail_to_time=data.created_at,
                                                    # duration_of_pump_fail=data,
                                                    failed_pump=pump_fail,
                                                    water_level_in_meter=data.water_level_meter,
                                                    low_level=data.low_water_level,
                                                    mode=data.mode,
                                                    created_at=datetime.now(settings.tz_NY),status=1)
            db.add(pump_fail_report)
            db.commit()       

    return data


# R3 failure analysis report
        
def report_failure_analysis(db:Session,data,from_date,to_date):
    
    if data:
        fail_item=[]
        non_fail_item=[]
        pump1_fail=0
        pump2_fail=0
        valve_open_fail=0
        valve_close_fail=0
        fail=0

        if data.pump1_failure_status==1:
            pump1_fail=1
            fail=1

        if data.pump2_failure_status==1:
            pump2_fail=1
            fail=1

        if data.valve_open_fail==1:
            valve_open_fail=1
            fail=1

        if data.valve_close_fail==1:
            valve_close_fail=1
            fail=1

        if fail == 1:
            pump_fail_report=ReportFailureAnalysis(
                                                    location_data_id=data.id,
                                                    location_id=data.location_id,
                                                    fail_from_time=data.created_at,
                                                    pump1_fail=pump1_fail,
                                                    pump2_fail=pump2_fail,
                                                    valve_open_fail=valve_open_fail,
                                                    valve_close_fail=valve_close_fail,
                                                    battery_voltage_level=data.battery_status,
                                                    pump_status=data.pump_status,
                                                    power_status=data.power_status,
                                                    water_level_in_meter=data.water_level_meter,
                                                    battery_voltage=data.battery_voltage,
                                                    ac_power_voltage=data.power_voltage_l1,
                                                    mode=data.mode,
                                                    created_at=datetime.now(settings.tz_NY),status=1)
            db.add(pump_fail_report)
            db.commit()

    return data
        


# R4 Estimation of water supplied based on Pump Capacity

def water_supplied_based_on_pump_capacity(db:Session,data,from_date,to_date):
    if data.pump_status == 1:
        pump_on_time = data.created_at
        pump_off_time =None
    else:
        pump_on_time = None
        pump_off_time = data.created_at

    print("start")
    if data.pump1_failure_status==0 and data.pump2_failure_status==0:
        print("start    1")


        get_last_report_supplied_water=db.query(ReportSuppliedWater).filter(ReportSuppliedWater.location_id == data.location_id,
                ReportSuppliedWater.status == 1,ReportSuppliedWater.running_pump ==data.running_pump,ReportSuppliedWater.created_at>=from_date,
                ).order_by(ReportSuppliedWater.created_at.desc()).first()
        
        # get_last_report_supplied_water=db.query(ReportSuppliedWater).filter(ReportSuppliedWater.location_id == data.location_id,
        #             ReportSuppliedWater.status == 1,ReportSuppliedWater.pump_on_time < data.created_at,ReportSuppliedWater.running_pump ==data.running_pump,ReportSuppliedWater.pump_off_time == None,from_date <= ReportSuppliedWater.created_at ,ReportSuppliedWater.created_at<= to_date).order_by(ReportSuppliedWater.created_at.desc()).first()
        
        
        if get_last_report_supplied_water:
            if get_last_report_supplied_water.pump_off_time==None:
                if pump_on_time:
                    get_last_report_supplied_water.pump_off_water_in_level=data.water_level_meter
                    get_last_report_supplied_water.pump_off_water_volume=data.water_volume_liter
                    
                    db.commit()
                if pump_off_time:
                    t1 = (get_last_report_supplied_water.pump_on_time).timestamp()
                    t2 = pump_off_time.timestamp()
                    running_duration=t2 - t1
                    
                    setting_data=db.query(LocationSetting).filter(LocationSetting.location_id==data.location_id,LocationSetting.status==1).first()
                    if data.running_pump==1:
                        pump_waterflow_per_min=setting_data.pump1_waterflow_per_min
                    if data.running_pump==2:
                        pump_waterflow_per_min=setting_data.pump2_waterflow_per_min
                    # h=str(running_duration)
                    # delta = timedelta(hours=int(h.split(':')[0]), minutes=int(h.split(':')[1]), seconds=int(h.split(':')[2]))
                    # minutes = delta.total_seconds()/60
                    
                    minutes= float(running_duration/60)
                    # print(pump_waterflow_per_min,minutes)
                    
                    capacity=float(format(float(pump_waterflow_per_min)*float(minutes), ".3f"))

                    
                    get_last_report_supplied_water.water_volume_supplied_based_on_capacity=capacity
                    get_last_report_supplied_water.mode=data.mode

                    get_last_report_supplied_water.pump_running_hours=running_duration
                    get_last_report_supplied_water.pump_off_time=pump_off_time
                    get_last_report_supplied_water.pump_off_water_level_in_meter=data.water_level_meter
                    get_last_report_supplied_water.pump_off_water_volume_in_liter=data.water_volume_liter
                    
                    db.commit()
            elif get_last_report_supplied_water.pump_off_time:
                if pump_on_time:    
                    pump_report=ReportSuppliedWater(location_data_id=data.id,location_id=data.location_id,
                                                pump_on_time=pump_on_time,
                                                water_level_in_meter=data.water_level_meter,
                                                water_volume_in_liter=data.water_volume_liter,
                                                pump_off_time=pump_off_time,
                                                pump_off_water_level_in_meter=data.water_level_meter if data.pump_status == 0 else None,
                                                pump_off_water_volume_in_liter=data.water_volume_liter if data.pump_status ==0 else None,
                                                running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                    db.add(pump_report)
                    db.commit()
                if pump_off_time:
                    pass
        else:
            print("start 2")

            get_pre_report_supplied_water=db.query(ReportSuppliedWater).filter(ReportSuppliedWater.location_id == data.location_id,
                    ReportSuppliedWater.status == 1,ReportSuppliedWater.running_pump ==data.running_pump,
                    ReportSuppliedWater.created_at<from_date,
                    ).order_by(ReportSuppliedWater.created_at.desc()).first()
            if get_pre_report_supplied_water:
                print("start 22")
                if get_pre_report_supplied_water.pump_off_time==None:
                    print("start 3")

                    if pump_on_time:
                        print("start 33")

                        off_time = datetime.strptime(get_pre_report_supplied_water.pump_on_time.strftime("%Y-%m-%d 23:59:59"),"%Y-%m-%d %H:%M:%S")#this changes(all report) for the client told that the whatsapp grp chat(sept 2 2021) 
                    
                        t1 = (get_pre_report_supplied_water.pump_on_time).timestamp()
                        t2 = off_time.timestamp()                
                        running_duration=t2 - t1

                        setting_data=db.query(LocationSetting).filter(LocationSetting.location_id==data.location_id,LocationSetting.status==1).first()
                        if data.running_pump==1:
                            pump_waterflow_per_min=setting_data.pump1_waterflow_per_min
                        if data.running_pump==2:
                            pump_waterflow_per_min=setting_data.pump2_waterflow_per_min
                        # h=str(running_duration)
                        # delta = timedelta(hours=int(h.split(':')[0]), minutes=int(h.split(':')[1]), seconds=int(h.split(':')[2]))
                        # minutes = delta.total_seconds()/60
                        
                        minutes= float(running_duration/60)
                        # print(pump_waterflow_per_min,minutes)
                        
                        capacity=float(format(float(pump_waterflow_per_min)*float(minutes), ".3f"))

                        
                        get_pre_report_supplied_water.water_volume_supplied_based_on_capacity=capacity
                        get_pre_report_supplied_water.mode=data.mode
                        get_pre_report_supplied_water.pump_running_hours=running_duration
                        get_pre_report_supplied_water.pump_off_time=off_time
                        get_pre_report_supplied_water.pump_off_water_level_in_meter=None
                        get_pre_report_supplied_water.pump_off_water_volume_in_liter=None
                        
                        db.commit()

                        pump_report=ReportSuppliedWater(location_data_id=data.id,location_id=data.location_id,
                                                pump_on_time=pump_on_time,
                                                water_level_in_meter=data.water_level_meter,
                                                water_volume_in_liter=data.water_volume_liter,
                                                pump_off_time=None,
                                                pump_off_water_level_in_meter=None,
                                                pump_off_water_volume_in_liter=None,
                                                running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                        db.add(pump_report)
                        db.commit()
                    if pump_off_time:
                        off_time = datetime.strptime(get_pre_report_supplied_water.pump_on_time.strftime("%Y-%m-%d 23:59:59"),"%Y-%m-%d %H:%M:%S")#this changes(all report) for the client told that the whatsapp grp chat(sept 2 2021) 
                    
                        t1 = (get_pre_report_supplied_water.pump_on_time).timestamp()
                        t2 = off_time.timestamp()                
                        running_duration=t2 - t1

                        setting_data=db.query(LocationSetting).filter(LocationSetting.location_id==data.location_id,LocationSetting.status==1).first()
                        if data.running_pump==1:
                            pump_waterflow_per_min=setting_data.pump1_waterflow_per_min
                        if data.running_pump==2:
                            pump_waterflow_per_min=setting_data.pump2_waterflow_per_min
                        # h=str(running_duration)
                        # delta = timedelta(hours=int(h.split(':')[0]), minutes=int(h.split(':')[1]), seconds=int(h.split(':')[2]))
                        # minutes = delta.total_seconds()/60
                        
                        minutes= float(running_duration/60)
                        # print(pump_waterflow_per_min,minutes)
                        
                        capacity=float(format(float(pump_waterflow_per_min)*float(minutes), ".3f"))

                        
                        get_pre_report_supplied_water.water_volume_supplied_based_on_capacity=capacity
                        get_pre_report_supplied_water.mode=data.mode
                        get_pre_report_supplied_water.pump_running_time=running_duration
                        get_pre_report_supplied_water.pump_off_time=off_time
                        get_pre_report_supplied_water.pump_off_water_level_in_meter=None
                        get_pre_report_supplied_water.pump_off_water_volume_in_liter=None
                        
                        db.commit()

                        on_time = datetime.strptime(pump_off_time.strftime("%Y-%m-%d 00:00:00"),"%Y-%m-%d %H:%M:%S")
                        t1 =on_time.timestamp()
                        t2 =pump_off_time.timestamp()
                        running_duration=t2 - t1
                        pump_report=ReportSuppliedWater(location_data_id=data.id,location_id=data.location_id,pump_on_time=on_time,
                                                        water_level_in_meter=None,water_volume_in_liter=None,
                                                        pump_off_time=pump_off_time,
                                                        pump_off_water_level_in_meter=data.water_level_meter if data.pump_status == 0 else None,
                                                        pump_off_water_volume_in_liter=data.water_volume_liter if data.pump_status ==0 else None,
                                                        pump_running_hours=running_duration,
                                                        running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                        db.add(pump_report)
                        db.commit()
                elif get_pre_report_supplied_water.pump_off_time:
                    if pump_on_time:    
                        pump_report=ReportSuppliedWater(location_data_id=data.id,location_id=data.location_id,
                                                    pump_on_time=pump_on_time,
                                                    water_level_in_meter=data.water_level_meter,
                                                    water_volume_in_liter=data.water_volume_liter,
                                                    pump_off_time=pump_off_time,
                                                    pump_off_water_level_in_meter=data.water_level_meter if data.pump_status == 0 else None,
                                                    pump_off_water_volume_in_liter=data.water_volume_liter if data.pump_status ==0 else None,
                                                    running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                        db.add(pump_report)
                        db.commit()
                    if pump_off_time:
                        pass
            else:
                if pump_on_time:
                    print("start 222")
                    pump_report=ReportSuppliedWater(location_data_id=data.id,location_id=data.location_id,
                                                pump_on_time=pump_on_time,
                                                water_level_in_meter=data.water_level_meter,
                                                water_volume_in_liter=data.water_volume_liter,
                                                pump_off_time=pump_off_time,
                                                pump_off_water_level_in_meter=None,
                                                pump_off_water_volume_in_liter=None,
                                                running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
                    db.add(pump_report)
                    db.commit()
                if pump_off_time:
                    pass

    return data
  
    # if data.pump1_failure_status==0 and data.pump2_failure_status==0:
    
    #     if data.pump_status == 1:
    #         pump_on_time = data.created_at
    #         pump_off_time =None
    #     else:
    #         pump_on_time = None
    #         pump_off_time = data.created_at

    #     get_last_report_supplied_water=db.query(ReportSuppliedWater).filter(ReportSuppliedWater.location_id == data.location_id,ReportSuppliedWater.status == 1,ReportSuppliedWater.pump_on_time < data.created_at,ReportSuppliedWater.running_pump ==data.running_pump,ReportSuppliedWater.pump_off_time == None,from_date <= ReportSuppliedWater.created_at ,ReportSuppliedWater.created_at<= to_date).order_by(ReportSuppliedWater.created_at.desc()).first()
        
    #     if get_last_report_supplied_water:

    #         # If Pump is On (Running duration)
    #         # t1 = dt.datetime.strptime(dt.datetime.strftime(get_last_report_supplied_water.pump_on_time, "%H:%M:%S"),"%H:%M:%S")
    #         # t2 = dt.datetime.strptime(dt.datetime.strftime(data.created_at, "%H:%M:%S"),"%H:%M:%S")
    #         # running_duration=t2 - t1
            
    #         # get_last_report_supplied_water.pump_running_hours=running_duration
            
    #         # db.commit()
            
    #         if pump_off_time:
    #             # Calculate Pump running duration
    #             t1 =(get_last_report_supplied_water.pump_on_time).timestamp()
    #             t2 =pump_off_time.timestamp()
    #             running_duration=t2 - t1
                
    #             get_last_report_supplied_water.pump_running_hours=running_duration
    #             get_last_report_supplied_water.pump_off_time=pump_off_time
    #             get_last_report_supplied_water.pump_off_water_level_in_meter=data.water_level_meter
    #             get_last_report_supplied_water.pump_off_water_volume_in_liter=data.water_volume_liter
                
    #             setting_data=db.query(LocationSetting).filter(LocationSetting.location_id==data.location_id,LocationSetting.status==1).first()
    #             if data.running_pump==1:
    #                 pump_waterflow_per_min=setting_data.pump1_waterflow_per_min
    #             if data.running_pump==2:
    #                 pump_waterflow_per_min=setting_data.pump2_waterflow_per_min
    #             # h=str(running_duration)
    #             # delta = timedelta(hours=int(h.split(':')[0]), minutes=int(h.split(':')[1]), seconds=int(h.split(':')[2]))
    #             # minutes = delta.total_seconds()/60
                
    #             minutes= float(running_duration/60)
    #             # print(pump_waterflow_per_min,minutes)
                
    #             capacity=float(format(float(pump_waterflow_per_min)*float(minutes), ".3f"))

                
    #             get_last_report_supplied_water.water_volume_supplied_based_on_capacity=capacity
    #             get_last_report_supplied_water.mode=data.mode
                

                
    #             db.commit()
            
    #     else:
    #         get_last_report_supplied_water=db.query(ReportSuppliedWater).filter(ReportSuppliedWater.location_id == data.location_id,ReportSuppliedWater.status == 1,ReportSuppliedWater.pump_on_time < data.created_at,ReportSuppliedWater.running_pump ==data.running_pump,from_date <= ReportSuppliedWater.created_at ,ReportSuppliedWater.created_at<= to_date).order_by(ReportSuppliedWater.created_at.desc()).first()
    #         if not get_last_report_supplied_water:
    #             get_last_report_supplied_water2=db.query(ReportSuppliedWater).filter(ReportSuppliedWater.location_id == data.location_id,ReportSuppliedWater.status == 1,ReportSuppliedWater.pump_on_time < data.created_at,ReportSuppliedWater.running_pump ==data.running_pump,ReportSuppliedWater.pump_off_time == None).order_by(ReportSuppliedWater.created_at.desc()).first()
    #             if get_last_report_supplied_water2:
    #                 off_time = datetime.strptime(get_last_report_supplied_water2.pump_on_time.strftime("%Y-%m-%d 23:59:59"),"%Y-%m-%d %H:%M:%S")
    #                 t1 =(get_last_report_supplied_water2.pump_on_time).timestamp()
    #                 t2 =off_time.timestamp()            
    #                 running_duration=t2 - t1
    #                 # Calculate Pump running duration
    #                 # running_duration=t2 - t1
                    
    #                 get_last_report_supplied_water2.pump_running_hours=running_duration
    #                 get_last_report_supplied_water2.pump_off_time=off_time
    #                 get_last_report_supplied_water2.pump_off_water_level_in_meter=None
    #                 get_last_report_supplied_water2.pump_off_water_volume_in_liter=None
                    
    #                 setting_data=db.query(LocationSetting).filter(LocationSetting.location_id==data.location_id,LocationSetting.status==1).first()
    #                 if data.running_pump==1:
    #                     pump_waterflow_per_min=setting_data.pump1_waterflow_per_min
    #                 if data.running_pump==2:
    #                     pump_waterflow_per_min=setting_data.pump2_waterflow_per_min
    #                 # h=str(running_duration)
    #                 # delta = timedelta(hours=int(h.split(':')[0]), minutes=int(h.split(':')[1]), seconds=int(h.split(':')[2]))
    #                 # minutes = delta.total_seconds()/60
    #                 minutes= float(running_duration/60)
                    
    #                 # print(pump_waterflow_per_min,minutes)
                    
    #                 capacity=float(format(float(pump_waterflow_per_min)*float(minutes), ".3f"))

                    
    #                 get_last_report_supplied_water2.water_volume_supplied_based_on_capacity=capacity
    #                 get_last_report_supplied_water2.mode=data.mode
                    

                    
    #                 db.commit()
                    
    #                 if pump_off_time:
    #                     on_time = datetime.strptime(pump_off_time.strftime("%Y-%m-%d 00:00:00"),"%Y-%m-%d %H:%M:%S")
    #                     t1 =on_time.timestamp()
    #                     t2 =pump_off_time.timestamp()
    #                     running_duration=t2 - t1
    #                     setting_data=db.query(LocationSetting).filter(LocationSetting.location_id==data.location_id,LocationSetting.status==1).first()
    #                     if data.running_pump==1:
    #                         pump_waterflow_per_min=setting_data.pump1_waterflow_per_min
    #                     if data.running_pump==2:
    #                         pump_waterflow_per_min=setting_data.pump2_waterflow_per_min
    #                     # h=str(running_duration)
    #                     # delta = timedelta(hours=int(h.split(':')[0]), minutes=int(h.split(':')[1]), seconds=int(h.split(':')[2]))
    #                     # minutes = delta.total_seconds()/60
    #                     minutes= float(running_duration/60)
                    
    #                     # print(pump_waterflow_per_min,minutes)
    #                     capacity=float(format(float(pump_waterflow_per_min)*float(minutes), ".3f"))
                        
    #                     pump_report=ReportSuppliedWater(location_data_id=data.id,location_id=data.location_id,
    #                                                 pump_on_time=on_time,
    #                                                 water_level_in_meter=data.water_level_meter,
    #                                                 water_volume_in_liter=data.water_volume_liter,
    #                                                 pump_off_time=pump_off_time,
    #                                                 pump_running_hours=running_duration,
    #                                                 water_volume_supplied_based_on_capacity=capacity,
    #                                                 pump_off_water_level_in_meter=data.water_level_meter if data.pump_status == 0 else None,
    #                                                 pump_off_water_volume_in_liter=data.water_volume_liter if data.pump_status ==0 else None,
    #                                                 running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
    #                     db.add(pump_report)
    #                     db.commit()
    #         if pump_on_time:
    #             get_last_report_supplied_water=db.query(ReportSuppliedWater).filter(ReportSuppliedWater.location_id == data.location_id,ReportSuppliedWater.status == 1,ReportSuppliedWater.pump_on_time < data.created_at,ReportSuppliedWater.running_pump ==data.running_pump,ReportSuppliedWater.pump_off_time == None).order_by(ReportSuppliedWater.created_at.desc()).first()
    #             if get_last_report_supplied_water:
    #                 off_time = datetime.strptime(get_last_report_supplied_water.pump_on_time.strftime("%Y-%m-%d 23:59:59"),"%Y-%m-%d %H:%M:%S")
    #                 print(off_time)
    #                 t1 =(get_last_report_supplied_water.pump_on_time).timestamp()
    #                 t2 =off_time.timestamp()      
    #                 running_duration=t2 - t1
    #                 # Calculate Pump running duration
    #                 # running_duration=t2 - t1
                    
    #                 get_last_report_supplied_water.pump_running_hours=running_duration
    #                 get_last_report_supplied_water.pump_off_time=off_time
    #                 get_last_report_supplied_water.pump_off_water_level_in_meter=None
    #                 get_last_report_supplied_water.pump_off_water_volume_in_liter=None
                    
    #                 setting_data=db.query(LocationSetting).filter(LocationSetting.location_id==data.location_id,LocationSetting.status==1).first()
    #                 if data.running_pump==1:
    #                     pump_waterflow_per_min=setting_data.pump1_waterflow_per_min
    #                 if data.running_pump==2:
    #                     pump_waterflow_per_min=setting_data.pump2_waterflow_per_min
    #                 # h=str(running_duration)
    #                 # delta = timedelta(hours=int(h.split(':')[0]), minutes=int(h.split(':')[1]), seconds=int(h.split(':')[2]))
    #                 # minutes = delta.total_seconds()/60
    #                 minutes= float(running_duration/60)
                    
    #                 # print(pump_waterflow_per_min,minutes)
                    
    #                 capacity=float(format(float(pump_waterflow_per_min)*float(minutes), ".3f"))

                    
    #                 get_last_report_supplied_water.water_volume_supplied_based_on_capacity=capacity
    #                 get_last_report_supplied_water.mode=data.mode
                    

                    
    #                 db.commit()
    #             pump_report=ReportSuppliedWater(location_data_id=data.id,location_id=data.location_id,
    #                                             pump_on_time=pump_on_time,
    #                                             water_level_in_meter=data.water_level_meter,
    #                                             water_volume_in_liter=data.water_volume_liter,
    #                                             pump_off_time=pump_off_time,
    #                                             water_volume_supplied_based_on_capacity=0,
    #                                             pump_off_water_level_in_meter=data.water_level_meter if data.pump_status == 0 else None,
    #                                             pump_off_water_volume_in_liter=data.water_volume_liter if data.pump_status ==0 else None,
    #                                             running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
    #             db.add(pump_report)
    #             db.commit()


# R5 Estimation of Water Distributed based on Geography
# def water_distributed_based_on_geography(db:Session,data,from_date,to_date):
#     if data.pump_status == 1:
#         pump_on_time = data.created_at
#         pump_off_time =None
#     else:
#         pump_on_time = None
#         pump_off_time = data.created_at

#     get_last_water_distribute_report=db.query(ReportDistributedWater).filter(ReportDistributedWater.location_id == data.location_id,ReportDistributedWater.status == 1,ReportDistributedWater.pump_on_time < data.created_at,ReportDistributedWater.running_pump ==data.running_pump,ReportDistributedWater.pump_off_time == None,from_date <= ReportDistributedWater.created_at ,ReportDistributedWater.created_at<= to_date).order_by(ReportDistributedWater.created_at.desc()).first()
    
#     if get_last_water_distribute_report:
#         water_supplied_data=get_last_water_distribute_report.water_volume_discharged_based_on_capacity

#         # Meter
#         old_water_level=get_last_water_distribute_report.water_level_in_meter
        
#         add_old_and_new_water_level=old_water_level + data.water_level_meter

#         get_last_water_distribute_report.water_level_in_meter=add_old_and_new_water_level
        
#         # Liter
#         old_water_liter=get_last_water_distribute_report.water_volume_in_liter

#         add_old_water_liter=old_water_liter+data.water_volume_liter
        
#         get_last_water_distribute_report.water_volume_in_liter=add_old_water_liter

#         subtract_old_new =add_old_water_liter - old_water_liter
        
        
#         if not get_last_water_distribute_report.water_volume_discharged_based_on_capacity:
            
#             get_last_water_distribute_report.water_volume_discharged_based_on_capacity=get_last_water_distribute_report.water_volume_discharged_based_on_capacity + subtract_old_new       
#         else:
#             get_last_water_distribute_report.water_volume_discharged_based_on_capacity=subtract_old_new + water_supplied_data
        
#         # If Pump is On (Running duration)
#         t1 = dt.datetime.strptime(dt.datetime.strftime(get_last_water_distribute_report.pump_on_time, "%H:%M:%S"),"%H:%M:%S")
#         t2 = dt.datetime.strptime(dt.datetime.strftime(data.created_at, "%H:%M:%S"),"%H:%M:%S")
#         running_duration=t2 - t1
        
#         get_last_water_distribute_report.pump_running_hours=running_duration
        
#         db.commit()
        
#         if pump_off_time:
#             # Calculate Pump running duration
#             t1 = dt.datetime.strptime(dt.datetime.strftime(get_last_water_distribute_report.pump_on_time, "%H:%M:%S"),"%H:%M:%S")
#             t2 = dt.datetime.strptime(dt.datetime.strftime(pump_off_time, "%H:%M:%S"),"%H:%M:%S")
#             running_duration=t2 - t1
            
#             get_last_water_distribute_report.pump_running_hours=running_duration
#             get_last_water_distribute_report.pump_off_time=pump_off_time
#             get_last_water_distribute_report.pump_off_water_level_in_meter=get_last_water_distribute_report.water_level_in_meter
#             get_last_water_distribute_report.pump_off_water_volume_in_liter=get_last_water_distribute_report.water_volume_in_liter

#             db.commit()
        
#     else:
#         if pump_on_time:
#             pump_report=ReportDistributedWater(location_data_id=data.id,location_id=data.location_id,
#                                             pump_on_time=pump_on_time,
#                                             water_level_in_meter=data.water_level_meter,
#                                             water_volume_in_liter=data.water_volume_liter,
#                                             pump_off_time=pump_off_time,
#                                             water_volume_discharged_based_on_capacity=0,
#                                             pump_off_water_level_in_meter=data.water_level_meter if data.pump_status == 0 else None,
#                                             pump_off_water_volume_in_liter=data.water_volume_liter if data.pump_status ==0 else None,
#                                             running_pump=data.running_pump,mode=data.mode,created_at=datetime.now(settings.tz_NY))
#             db.add(pump_report)
#             db.commit()
    
