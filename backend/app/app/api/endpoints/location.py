from typing import Any, List,Optional
from sqlalchemy import Boolean, Column, Integer, String, DateTime,or_,and_, desc, asc, func
from fastapi import APIRouter, Body, Depends, HTTPException,Form
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.sql.functions import ReturnTypeFromArgs
from app.utils import create_reset_key,check_phone,digit_check, common_date
from app.core.security import get_password_hash
from app import crud, models, schemas
from app.models import Location,UserLocationMapping,LocationData,ReportPumpRunTime,LocationSetting
from app.api import deps
from app.core.config import settings
from datetime import datetime, time
from fastapi_pagination import Page, add_pagination, paginate
from app.core.config import settings, Page as Bpage

router = APIRouter()
# , response_model=Bpage[schemas.ListLocation]
# List location
@router.post("/list_location/", response_model=Bpage[schemas.ListLocation])
def list_location(
                *,
                db: Session = Depends(deps.get_db),
                location_code:Optional[str] = Form(None),
                name:Optional[str] = Form(None),
                order_by:Optional[str]=Form(None),
                order:Optional[str]=Form(None),
                current_user: models.User = Depends(deps.get_current_user)):
    
    listed_location=[]
    
    if current_user.user_type == 1:
        location=db.query(Location).filter(Location.status != -1)
        # location=db.query(Location.id,Location.location_code,Location.name,Location.time_frequency,Location.tank_type,Location.created_at,Location.status,UserLocationMapping.access_type).outerjoin(UserLocationMapping)

    
    else:
        location=db.query(Location.id,Location.total_capacity,Location.location_code,Location.name,Location.time_frequency,Location.tank_type,Location.radius,Location.width,Location.height,Location.length,Location.created_at,Location.status,UserLocationMapping.access_type).filter(UserLocationMapping.user_id ==current_user.id,UserLocationMapping.status==1 ).outerjoin(UserLocationMapping)
    
    if location_code:
        location = location.filter(Location.location_code.like("%"+location_code+"%"))
    
    if name:
        location=location.filter(Location.name.like("%"+name+"%"))
    
    if order_by:
        field, table = ["location_code", Location] if order_by == "location_code"  else [order_by, Location]

        if order == "desc":
            location = location.order_by(desc(getattr(table, order_by)))
        else:
            location = location.order_by(asc(getattr(table, order_by)))

    else:
        location = location.order_by(Location.id.desc())
    
    location=location.distinct(Location.id).all()
    
    if location:
        for row in location:
            last_data=db.query(LocationData).filter(LocationData.location_id==row.id,LocationData.status==1).order_by(LocationData.created_at.desc()).first()
            capacity=0
            if last_data:
                capacity=(float(last_data.water_volume_liter)/float(row.total_capacity))*100
                if capacity > 100:
                    capacity=100

            if current_user.user_type ==2:
               access_type= row.access_type if row.access_type else ""
            else:
               access_type= 0

            device_list = {"location_id":row.id if row.id else 0,
                    "location_code":row.location_code if row.location_code else "", 
                    "name":row.name if row.name else "", 
                    "time_frequency":str(row.time_frequency) if str(row.time_frequency)  else "",
                    "tank_type":row.tank_type if row.tank_type else 0,
                    "access_type":access_type,
                    "radius":row.radius if row.radius else 0,
                    "height":row.height if row.height else 0,
                    "length":row.length if row.length else 0,
                    "width":row.width if row.width else 0,
                    "last_data":common_date(last_data.created_at) if last_data else '',
                    "capacity":capacity,
                    "scheme_name":row.scheme_name if row.scheme_name else "", 
                    "district":row.district if row.district else "", 
                    "block":row.block if row.block else "", 
                    "panchayat":row.panchayat if row.panchayat else "", 
                    "ear_marked_qty":row.ear_marked_qty if row.ear_marked_qty else 0, 
                    "field_1":row.field_1 if row.field_1 else "", 
                    "field_2":row.field_2 if row.field_2 else "", 
                    "field_3":row.field_3 if row.field_3 else "", 
                    "created_at": common_date(row.created_at) if row.created_at else "",
                    "status":row.status if row.status else 0
                    }
            listed_location.append(device_list)

        
    # return listed_location
    return paginate(listed_location)

@router.post("/list_location_dropdown/", response_model=List[schemas.ListLocationDrop])
def list_location_dropdown(
                *,
                db: Session = Depends(deps.get_db),
                current_user: models.User = Depends(deps.get_current_user)):
    
    listed_location=[]
    
    location=db.query(Location)
    
    # return location
    if current_user.user_type == 1:
        location=location.filter(Location.status != -1)
        
    else:
        location=location.filter(UserLocationMapping.user_id == current_user.id,UserLocationMapping.status == 1,Location.status != -1).outerjoin(UserLocationMapping)
    location = location.order_by(Location.id.desc())
    
    location=location.distinct(Location.id).all()
    
    if location:
        for row in location:
            
            device_list = {"location_id":row.id if row.id else 0,
                    "location_code":row.location_code if row.location_code else "", 
                    "name":row.name if row.name else "",
                    }
            listed_location.append(device_list)
    else:
        raise HTTPException(
        status_code=404,
        detail="Data not found",
        )

    return listed_location

# Create Location,response_model=schemas.ListLocation
@router.post("/create_location",response_model=schemas.ListLocation)
def create_location(
    *,
    db: Session = Depends(deps.get_db),
    location_in: schemas.CreateLocation,
    user_in:schemas.CreateMappingLocation,
    current_user: models.User = Depends(deps.get_current_user)):
        
    if current_user.user_type == 1:
       
        check_location=crud.location.check_location(db,code=location_in.location_code)
        if check_location:
            raise HTTPException(
                status_code=400,
                detail="Location code already exists",
            )
        
        # create_location = crud.location.create(db, obj_in=location_in)
        obj_in = jsonable_encoder(location_in)
        # return obj_in
        radius = 0.000 if (obj_in['radius']=='' or obj_in['radius']==None) else obj_in['radius']
        height = 0.000 if (obj_in['height']=='' or obj_in['height']==None) else obj_in['height']
        length = 0.000 if (obj_in['length']=='' or obj_in['length']==None) else obj_in['length']
        width = 0.000 if (obj_in['width']=='' or obj_in['width']==None) else obj_in['width']

        create_location=Location(location_code=obj_in['location_code'],name=obj_in['name'],time_frequency=obj_in['time_frequency'],tank_type=obj_in['tank_type'],
                                    radius=radius,height=height,length=length,width=width,scheme_name=obj_in['scheme_name'],district=obj_in['district'],
                                    block=obj_in['block'],panchayat=obj_in['panchayat'],ear_marked_qty=obj_in['ear_marked_qty'],field_1=obj_in['field_1'],
                                    field_2=obj_in['field_2'],field_3=obj_in['field_3'],created_at=datetime.now(settings.tz_NY),status=1)
        db.add(create_location)
        db.commit()
        
        if create_location:
            if create_location.tank_type==1:
                pi=3.14
                total_capacity=(float(pi)*float(create_location.radius)*float(create_location.radius)*float(create_location.height))
                get_location=db.query(Location).filter(Location.id==create_location.id).update({"total_capacity":total_capacity})
                db.commit()

            if create_location.tank_type==2:
                total_capacity=(float(create_location.length)*float(create_location.width)*float(create_location.height))
                get_location=db.query(Location).filter(Location.id==create_location.id).update({"total_capacity":total_capacity})
                db.commit()

        # return user_in
        if user_in.user_id:
            
            user_id=user_in.user_id
            access_type=user_in.access_type
            location_id=[create_location.id]
            
            mapping = jsonable_encoder({"user_id":user_id,"location_id":location_id,"access_type":access_type})
            create_maping = crud.user_location_mapping.create(db, obj_in=mapping,current_user=current_user.id)

        
        view_create_location={}
        
        view_create_location.update({"location_id":create_location.id if create_location.id else "",
                    "location_code":create_location.location_code if create_location.location_code else "",
                    "name":create_location.name if create_location.name else "",
                    "time_frequency":str(create_location.time_frequency) if str(create_location.time_frequency) else "",
                    "tank_type":create_location.tank_type if create_location.tank_type else 0,
                    "created_at": common_date(create_location.created_at) if create_location.created_at else "",
                    "status":create_location.status if create_location.status else 0
                    # "radius":create_location.radius if create_location.radius else 0,
                    # "height":create_location.height if create_location.height else 0,
                    # "length":create_location.length if create_location.length else 0,
                    # "width":create_location.width if create_location.width else 0,
                    # "updated_at": common_date(create_location.updated_at) if create_location.updated_at else "",
                    # "updated_by": create_location.updated_by if create_location.updated_by else "",
                    })
       # mqtt_call=deps.setting_call(db=db,location_id=create_location.id,operation='getdevicesettings')
                
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )

    return view_create_location



#  Update Location
@router.put("/update_location/{location_id}",response_model=schemas.ListLocation)
def update_location(
    *,
    db: Session = Depends(deps.get_db),
    location_id: int,
    location_in: schemas.UpdateLocation,
    current_user: models.User = Depends(deps.get_current_user)):
    
    if current_user.user_type == 1:
        check_location = crud.location.get(db, id=location_id)
        
        if not check_location:
            raise HTTPException(
                status_code=404,
                detail="Location not found",
            )
        
        check_location_code=db.query(Location).filter(Location.id != location_id,Location.location_code ==location_in.location_code,Location.status != -1).first()
       
        if check_location_code:
            raise HTTPException(
                status_code=400,
                detail="Location code already taken",
            )
        
        updated_location = crud.location.update(db, db_obj=check_location, obj_in=location_in,current_user=current_user.id)
        if updated_location.tank_type==1:
            pi=3.14
            total_capacity=(float(pi)*float(updated_location.radius)*float(updated_location.radius)*float(updated_location.height))
            get_location=db.query(Location).filter(Location.id==updated_location.id).update({"total_capacity":total_capacity})
            db.commit()

        if updated_location.tank_type==2:
            total_capacity=(float(updated_location.length)*float(updated_location.width)*float(updated_location.height))
            get_location=db.query(Location).filter(Location.id==updated_location.id).update({"total_capacity":total_capacity})
            db.commit()
        
        change_in_location={}
        change_in_location.update({"location_id":updated_location.id if updated_location.id else "",
                    "location_code":updated_location.location_code if updated_location.location_code else "",
                    "name":updated_location.name if updated_location.name else "",
                    "time_frequency":str(updated_location.time_frequency) if str(updated_location.time_frequency) else "",
                    "tank_type":updated_location.tank_type if updated_location.tank_type else 0,
                    "created_at": common_date(updated_location.created_at) if updated_location.created_at else "",
                    "status":updated_location.status if updated_location.status else 0
                    # "radius":updated_location.radius if updated_location.radius else 0,
                    # "height":updated_location.height if updated_location.height else 0,
                    # "length":updated_location.length if updated_location.length else 0,
                    # "width":updated_location.width if updated_location.width else 0,
                    # "updated_at": common_date(updated_location.updated_at) if updated_location.updated_at else "",
                    # "updated_by": updated_location.updated_by if updated_location.updated_by else "",
                    })
       # mqtt_call=deps.setting_call(db=db,location_id=updated_location.id,operation='getdevicesettings')
        

        return change_in_location
 
    else:
        raise HTTPException(
        status_code=400,
        detail="Invalid request",
        )


# View Location
@router.get("/view_location/{location_id}",response_model=schemas.ViewLocation)
def view_location(
    *,
    db: Session = Depends(deps.get_db),
    location_id: int,
    current_user: models.User = Depends(deps.get_current_user)):

    view_location={}
    list_mapping=[]
    pump_control_data={}
    
    today = datetime.now(settings.tz_NY)

    today_date=today.strftime("%Y-%m-%d %H:%M:%S")

    get_location=crud.location.get(db,id=location_id)
    if get_location:
        last_data=db.query(LocationData).filter(LocationData.location_id==location_id,LocationData.status==1).order_by(LocationData.created_at.desc()).first()
        capacity=0
        if last_data:
            capacity=(float(last_data.water_volume_liter)/float(get_location.total_capacity))*100
            if capacity > 100:
                capacity=100
        view_location.update({"last_data":common_date(last_data.created_at) if last_data else '',
                    "capacity":capacity,})
        view_location.update({"location_id":get_location.id if get_location.id else "",
                        "location_code":get_location.location_code if get_location.location_code else "",
                        "name":get_location.name if get_location.name else "",
                        "time_frequency":str(get_location.time_frequency) if str(get_location.time_frequency) else "",
                        "tank_type":get_location.tank_type if get_location.tank_type else "",
                        "radius":get_location.radius if get_location.radius else 0,
                        "height":get_location.height if get_location.height else 0,
                        "length":get_location.length if get_location.length else 0,
                        "width":get_location.width if get_location.width else 0,
                        "tank_capacity":get_location.total_capacity,
                        "last_data":common_date(last_data.created_at) if last_data else '',
                        "capacity":capacity,
                         "scheme_name":get_location.scheme_name if get_location.scheme_name else "", 
                        "district":get_location.district if get_location.district else "", 
                        "block":get_location.block if get_location.block else "", 
                        "panchayat":get_location.panchayat if get_location.panchayat else "", 
                        "ear_marked_qty":get_location.ear_marked_qty if get_location.ear_marked_qty else 0, 
                        "field_1":get_location.field_1 if get_location.field_1 else "", 
                        "field_2":get_location.field_2 if get_location.field_2 else "", 
                        "field_3":get_location.field_3 if get_location.field_3 else "", 
                        "created_at":common_date(get_location.created_at) if get_location.created_at else "",
                        "status":get_location.status if get_location.status else 0
                        })

        
        current_date=datetime.strptime(today_date, '%Y-%m-%d %H:%M:%S')
        if last_data:
            cal_date=current_date - last_data.created_at
            print(cal_date.seconds)
            view_location.update({"time_diff":str(cal_date.seconds)})
        

        if last_data:
            # capacity=0
            # capacity=(float(last_data.water_volume_liter)/float(get_location.total_capacity))*100
            running_duration=last_data.pump_running_hrs
            water_volume=last_data.water_volume_liter
            if last_data.pump_status == 1:
                run_report=db.query(ReportPumpRunTime).filter(ReportPumpRunTime.location_id==last_data.location.id).order_by(ReportPumpRunTime.created_at.desc()).first()
                if run_report:
                    if run_report.pump_on_time and run_report.pump_off_time==None:
                        running_duration=(datetime.now(settings.tz_NY).replace(microsecond=0,tzinfo=None)).timestamp() - (run_report.pump_on_time).timestamp()
                        
                        setting_data=db.query(LocationSetting).filter(LocationSetting.location_id==last_data.location.id,LocationSetting.status==1).first()
                        
                        if last_data.running_pump==1:
                            pump_waterflow_per_min=setting_data.pump1_waterflow_per_min
                        if last_data.running_pump==2:
                            pump_waterflow_per_min=setting_data.pump2_waterflow_per_min
                        
                        minutes= float(running_duration/60)                        
                        water_volume=float(format(float(pump_waterflow_per_min)*float(minutes), ".3f"))          
            pump_control_data.update({"location_data_id":last_data.id if last_data.id else 0,
                "location_id":last_data.location.id if last_data.location.id else 0,
                "location_code":str(last_data.location.location_code) if str(last_data.location.location_code) else "", 
                "location_name":str(last_data.location.name) if str(last_data.location.name) else "", 
                
                "power_voltage_l1":str(last_data.power_voltage_l1) if str(last_data.power_voltage_l1) else '', 
                
                "power_voltage_l2":str(last_data.power_voltage_l2) if str(last_data.power_voltage_l2) else '', 
                "power_voltage_l3":str(last_data.power_voltage_l3) if str(last_data.power_voltage_l3) else '', 
                "power_status":str(last_data.power_status) if str(last_data.power_status) else '', 
                "water_level_meter":str(last_data.water_level_meter) if str(last_data.water_level_meter) else '', 
                "water_volume_liter":str(last_data.water_volume_liter) if str(last_data.water_volume_liter) else '', 
                "pump_status":last_data.pump_status if last_data.pump_status else 0, 
                "battery_voltage":str(last_data.battery_voltage) if str(last_data.battery_voltage) else '', 
                "valve_status":last_data.valve_status if last_data.valve_status else 0, 
                "pump_running_hrs":datetime.fromtimestamp(running_duration).strftime("%H:%M:%S") if running_duration else datetime.fromtimestamp(0).strftime("%H:%M:%S"), 
                "pump_running_hrs_day":datetime.fromtimestamp(last_data.pump_running_hrs_day).strftime("%H:%M:%S") if last_data.pump_running_hrs_day else datetime.fromtimestamp(0).strftime("%H:%M:%S"), 
                "running_pump":last_data.running_pump if last_data.running_pump else 0, 
                "discharged_water_volume":str(water_volume) if str(water_volume) else '', 
                "discharged_water_volume_in_day":str(last_data.discharged_water_volume_in_day) if str(last_data.discharged_water_volume_in_day) else '', 
                "signal_strength":last_data.signal_strength if last_data.signal_strength else 0, 
                "pump1_failure_status":last_data.pump1_failure_status if last_data.pump1_failure_status else 0, 
                "pump2_failure_status":last_data.pump2_failure_status if last_data.pump2_failure_status else 0, 
                "valve_open_fail":last_data.valve_open_fail if last_data.valve_open_fail else 0, 
                "valve_close_fail":last_data.valve_close_fail if last_data.valve_close_fail else 0, 
                "mode":last_data.mode if last_data.mode else 0, 
                "battery_status":last_data.battery_status if last_data.battery_status else 0, 
                "running_beyond_set_time":str(last_data.running_beyond_set_time) if str(last_data.running_beyond_set_time) else '', 
                "pump1_waterflow_per_min":str(last_data.pump1_waterflow_per_min) if str(last_data.pump1_waterflow_per_min) else '', 
                "pump2_waterflow_per_min":str(last_data.pump2_waterflow_per_min) if str(last_data.pump2_waterflow_per_min) else '', 
                "allowed_running_hrs":str(last_data.allowed_running_hrs) if str(last_data.allowed_running_hrs) else '', 
                "low_water_level":str(last_data.low_water_level) if str(last_data.low_water_level) else '', 
                "high_water_level":str(last_data.high_water_level) if str(last_data.high_water_level) else '', 
                "preset_water_level":str(last_data.preset_water_level) if str(last_data.preset_water_level) else '', 
                "pump1_failure_alarm":last_data.pump1_failure_alarm if last_data.pump1_failure_alarm else 0, 
                "pump2_failure_alarm":last_data.pump2_failure_alarm if last_data.pump2_failure_alarm else 0, 
                "valve_open_failure_alarm":last_data.valve_open_failure_alarm if last_data.valve_open_failure_alarm else 0, 
                "valve_close_failure_alarm":last_data.valve_close_failure_alarm if last_data.valve_close_failure_alarm else 0, 
                "battery_low_alarm":last_data.battery_low_alarm if last_data.battery_low_alarm else 0, 
                "pump_run_beyond_running_hours_alarm":last_data.pump_run_beyond_running_hours_alarm if last_data.pump_run_beyond_running_hours_alarm else 0,  
                
                "data_date":((last_data.created_at).strftime("%d-%m-%Y")) if last_data.created_at else "",
                "data_time":(last_data.created_at.time()).strftime("%I:%M %p") if last_data.created_at else "",
                "created_at": common_date(last_data.created_at) if last_data.created_at else "",
                "status":last_data.status if last_data.status else 0
                })
            view_location.update({"pump_control_data":pump_control_data})
        if current_user.user_type == 1:
            view_location.update({"access_type":1})
            get_mapping_details=db.query(UserLocationMapping).filter_by(location_id =get_location.id,status =1).all()
        
            for row in get_mapping_details:
                list_mapping.append({"mapping_id":row.id,"user_id":row.user_id,"username":row.user.username,"user_type":row.user.user_type,"access_type":row.access_type,"created_at":common_date(row.user.created_at)})
            
            view_location.update({"users":list_mapping})
            reply=view_location
        
        elif current_user.user_type ==2:
            mapping_all_locations=db.query(UserLocationMapping.location_id).filter(UserLocationMapping.status == 1,UserLocationMapping.user_id==current_user.id).all()
            if mapping_all_locations:
                mapping=db.query(UserLocationMapping).filter(UserLocationMapping.location_id==location_id,UserLocationMapping.status != -1).filter(UserLocationMapping.user_id!=current_user.id)
                get_mapping_details=db.query(UserLocationMapping).filter_by(user_id=current_user.id,status =1,location_id=location_id).first()
                # return get_mapping_details
                if get_mapping_details:
                    view_location.update({"access_type":get_mapping_details.access_type})  
                else:
                    view_location.update({"access_type":0})  

                if mapping:
                    for row in mapping:
                        list_mapping.append({"mapping_id":row.id,"user_id":row.user_id,"username":row.user.username,"user_type":row.user.user_type,"access_type":row.access_type,"created_at":common_date(row.user.created_at)})
                    view_location.update({"users":list_mapping})
                    reply=view_location
                else:
                    view_location.update({"users":list_mapping})
                    reply=view_location
            else:
                view_location.update({"users":list_mapping})
                reply=view_location
    else:
            
        raise HTTPException(
        status_code=404,
        detail="Location not found",
        )

    return reply
# Change status
@router.post("/change_status")
def change_status(
        *,
        db: Session = Depends(deps.get_db),
        location_id: int,
        status:int,
        current_user: models.User = Depends(deps.get_current_user)):
    
    if current_user.user_type == 1:
        get_location = crud.location.get(db, id=location_id)
        
        if get_location:
            check_location= crud.location.change_status(db=db, id=location_id,status=status)
            # Change status Mapping
            # change_status_mapping=db.query(UserLocationMapping).filter(UserLocationMapping.location_id== location_id,UserLocationMapping.status != -1).update({"status":status})

            db.commit()

            reply="Status changed succesfully"
        else:
            raise HTTPException(status_code=404, detail="Location not found")

    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    
    return reply

#delete device
@router.delete("/delete_location")
def delete_location(
        *,
        db: Session = Depends(deps.get_db),
        location_id: int,
        current_user: models.User = Depends(deps.get_current_user)):
    
    if current_user.user_type == 1: 

        get_location = crud.location.get(db, id=location_id)

        if not get_location:

            raise HTTPException(status_code=404, detail="Location not found")

        check_location= crud.location.delete(db=db, id=location_id)
        # Delete Mapped Location
        change_status_mapping=db.query(UserLocationMapping).filter(UserLocationMapping.location_id== location_id,UserLocationMapping.status != -1).update({"status":-1})

        db.commit()
        reply= "Deleted succesfully"
       
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    
    return reply

add_pagination(router)
