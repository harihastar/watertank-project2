from fastapi import FastAPI,APIRouter,Form
# from fastapi_mqtt import FastMQTT, MQTTConfig
# import binascii



# mqtt_config = MQTTConfig(host ='3.108.33.243',
#     port= 1883,
#     keepalive = 60,
#     username="username",
#     password="strong_password")


# mqtt = FastMQTT(
#     config=mqtt_config
# )
# mqtt.init_app(router)

# @mqtt.on_connect()
# def connect(client, flags, rc, properties):
#     print("Connected")

# # @mqtt.on_message()
# # async def message(client, topic, payload, qos, properties):
# #     print("Received message: ",topic, payload)
# #     # binascii.hexlify(payload).decode()
# #     return 0

# @mqtt.on_disconnect()
# def disconnect(client, packet, exc=None):
#     print("Disconnected")

# # @mqtt.on_subscribe()
# # def subscribe(client, mid, qos, properties):
# #     print("subscribed", client, mid, qos, properties)

# # @router.post("/mqtt_publish_1")
# # async def mqtt_publish_1(msg:str=Form(...)):
# #     mqtt.publish("perioddatatest", f"{msg}",qos = 1) #publishing mqtt topic 

# #     return {"result": True,"message":"Published" }
# async def publishtodevice(msg):
#     mqtt.publish("servercmds", f"{msg}") #publishing mqtt topic 
#     print({"result": True,"message":"Published"})

import random
import binascii

from paho.mqtt import client as mqtt_client
import requests
# from datetime import time
import time

router = APIRouter()

broker = '3.108.33.243'
# broker = '54.70.253.233'

port = 1883
# port = 1993

topic = "servercmds"
# topic = "testingdata"

# data = b'h#\x05\x00\x00ffh\x05\x18\xc3\xc3\x83gE33333333333He;I7T\x83\x16'
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'
# username = 'emqx'
# password = 'public'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    # client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish(client,msg):
    msg = f"messages: {msg}"
    result = client.publish(topic, msg)
    # result: [0, 1]
    status = result[0]
    if status == 0:
        print(f"Send `{msg}` to topic `{topic}`")
    else:
        print(f"Failed to send message to topic {topic}")

@router.get("/")
def run(msg):
    client = connect_mqtt()
    # client.loop()
    publish(client,msg)
