from typing import Any, List, Mapping,Optional
from sqlalchemy import Boolean, Column, Integer, String, DateTime,text,or_,and_, desc, asc
from fastapi import APIRouter, Body, Depends, HTTPException,Form,File,UploadFile,Response,Request
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session,load_only
from starlette.types import Message
from app.models import User,Location,UserLocationMapping,DeviceSetting,DeviceTimeSetting,LocationData,UserLocationMappingLog,MqttLog,TimeSetting,LocationSetting
from app.schemas import ListMapping
from app.schemas import ListUser,UserCreate,ViewByUser,ListUserDrop
from app.core.config import settings
from fastapi.responses import JSONResponse
import requests
from app import crud, models, schemas
from app.api import deps
from datetime import datetime
from fastapi_pagination import Page, add_pagination, paginate
import hashlib


router = APIRouter()

@router.get("/mqtt_call")
async def mqtt_call(*,db:Session=Depends(deps.get_db),request: Request):
    api= str(request.url)
    ip=request.client.host

    today = datetime.utcnow().date()
    from_date=today.strftime("%Y-%m-%d 00:00:00")
    to_date=today.strftime("%Y-%m-%d 23:59:59")
   
    call_method=request.method

    if call_method=="GET":
        data=request.query_params
    else:
        data=await request.form()
    error = 0 
    msg = ""
    auth_code=data.get("auth_code") if data.get("auth_code") and data.get("auth_code") != None else None
    received_msg=data.get("received_msg") if data.get("received_msg") and data.get("received_msg") != None else None

    if auth_code == None:
        error = 1
        msg = "Auth_code is missing"
    if received_msg == None:
        error = 1
        msg = "received_msg is missing"

    if error==0:
        data=(received_msg).split(',')
        # return data
        if len(data)>1:
            values=[]
            dict_value={}
            for row in data:
                field,value=row.split('=')
                dict_value.update({f"{field}":value})

            if ('f0' in dict_value) and dict_value['f0']!=None:
                operation=dict_value['f0']
            else:
                error=1
                msg="Operation(f0) value missing"
            if ('f1' in dict_value) and dict_value['f1']!=None:
                device_id=dict_value['f1']
            else:
                error=1
                msg="location id (f1) value missing"


            if error == 0 :
                check_location=db.query(Location).filter(Location.location_code==device_id,Location.status==1).first()
                if check_location:


                    auth_text=hashlib.sha1((str(received_msg)+settings.SALT_KEY).encode()).hexdigest()
                    if auth_code==auth_text:
                        if operation=='devicedata':
                            keys=["f2","f3","f4","f5","f6","f7","f8","f9","f10","f11","f12","f13","f14","f15","f16","f17","f18","f19","f20","f21","f22"]

                            for row in keys:
                                if error==0:
                                    error,msg=deps.check_keys(row,dict_value)
                                    error=error
                                else:
                                    return JSONResponse(content=msg)
                            if error==0:

                                response=deps.location_data_readings(db,location_code=dict_value['f1'],power_voltage_l1=dict_value['f2'],power_voltage_l2=dict_value['f3'],power_voltage_l3=dict_value['f4']
                                            ,power_status=dict_value['f5'],water_level_meter=dict_value['f6'],pump_status=dict_value['f7'],battery_voltage=dict_value['f8'],valve_status=dict_value['f9'],
                                            pump_running_hrs=dict_value['f10'],pump_running_hrs_day=dict_value['f11'],running_pump=dict_value['f12'],discharged_water_volume=dict_value['f13'],
                                            signal_strength=dict_value['f14'],pump1_failure_status=dict_value['f15'],pump2_failure_status=dict_value['f16'],valve_open_fail=dict_value['f17'],valve_close_fail=dict_value['f18'],
                                            mode=dict_value['f19'],battery_status=dict_value['f20'],running_beyond_set_time=dict_value['f21'],error_codes=dict_value['f22'])
                                msg=response
                            else:
                                return JSONResponse(content=msg)


                    
                        
                        elif operation=='getdevicesettings':
                            locatn_setting = db.query(LocationSetting).filter_by(status = 1, location_id = check_location.id).scalar()
                            time_setting = db.query(TimeSetting).filter_by(status = 1, location_id = check_location.id).all()

                            al_run=[]
                            if locatn_setting or time_setting:
                                val7,val8,val9=(str(locatn_setting.allowed_running_hrs)).split(':')
                                al_run.append(f'{val7}{val8}')
                                
                                location_setting={}
                                time_slot=[]

                                for slot in time_setting:
                                    val1,val2,val3=(str(slot.from_time)).split(':')
                                    val4,val5,val6=(str(slot.to_time)).split(':')


                                    time_slot.append(f'{val1}{val2}{val4}{val5}')

                                my_lst_str = ''+':'.join(map(str, time_slot))
                                
                                f102=locatn_setting.low_water_level if locatn_setting.low_water_level else 0
                                f103=locatn_setting.high_water_level if locatn_setting.high_water_level else 0
                                f104=locatn_setting.preset_water_level if locatn_setting.preset_water_level else 0
                                f105=locatn_setting.pump1_waterflow_per_min if locatn_setting.pump1_waterflow_per_min else 0
                                f106=locatn_setting.pump2_waterflow_per_min if locatn_setting.pump2_waterflow_per_min else 0
                                f107=locatn_setting.lpp if locatn_setting.lpp else 0
                                f108=locatn_setting.amps1 if locatn_setting.amps1 else 0
                                f109=locatn_setting.amps2 if locatn_setting.amps2 else 0
                                f110=check_location.time_frequency if check_location else 0
                                
                                topic='devicesetting'
                                device_id=device_id
                                message=f"f0={topic},f1={device_id},f100={my_lst_str},f101={al_run[0]},f102={f102},f103={f103},f104={f104},f105={f105},f106={f106},f107={f107},f108={f108},f109={f109},f110={f110}"
                                print({"msg":message})
                                auth_code=hashlib.sha1((message+settings.SALT_KEY).encode()).hexdigest()
                                print({"auth_code":auth_code})
                                deps.send_device_request(topic,auth_code,device_id,message)
                                # msg= response
                                msg="Success"
                            
                        elif operation=='senddevicesetting':
                            keys=["f100","f101","f102","f103","f104","f105","f106","f107","f108","f109","f110"]

                            for row in keys:
                                if error==0:
                                    error,msg=deps.check_keys(row,dict_value)
                                    error=error
                                else:
                                    return JSONResponse(content=msg)
                            if error==0:
                                locatn_setting = db.query(DeviceSetting).filter(DeviceSetting.status!=-1,DeviceSetting.location_id == check_location.id).update({"status":-1})
                                time_setting = db.query(DeviceTimeSetting).filter(DeviceTimeSetting.status!=-1, DeviceTimeSetting.location_id == check_location.id).update({"status":-1})
                
                                a=[]
                                i=0
                                all_hrs=list(str(dict_value['f101']))
                                for r in all_hrs:
                                    i+=1
                                    a.append(r)
                                    if i==2:
                                        a.append(':')
                                a.append(':00')
                                
                                hrs_str = ''.join(map(str, a))


                                create_setting = DeviceSetting(low_water_level=dict_value['f102'],
                                                    high_water_level=dict_value['f103'],
                                                    allowed_running_hrs=hrs_str,
                                                    preset_water_level=dict_value['f104'],
                                                    pump1_waterflow_per_min=dict_value['f105'],
                                                    pump2_waterflow_per_min=dict_value['f106'],
                                                    lpp=dict_value['f107'],
                                                    amps1=dict_value['f108'],
                                                    amps2=dict_value['f109'],
                                                    location_id = check_location.id, 
                                                    status = 1, 
                                                    created_at=datetime.now(settings.tz_NY))
                                db.add(create_setting)
                                db.commit()
                                t=[]
                                i=0
                                time_values=str(dict_value['f100']).split(":")
                                for row in time_values:
                                    t1=[]
                                    t2=[]
                                    i=0
                                    time_slot=list(str(row))
                                    for r in time_slot:
                                        i+=1
                                        if i<=4:
                                            t1.append(r)
                                            if i==2:
                                                t1.append(':')
                                            if i==4:
                                                t1.append(':00')
                                        if i>=5:
                                            t2.append(r)
                                            if i==6:
                                                t2.append(':')
                                            if i==8:
                                                t2.append(':00')
                                    time1 = ''.join(map(str, t1))
                                    time2 = ''.join(map(str, t2))

                                    create_new_time = DeviceTimeSetting(location_id=check_location.id, 
                                                            from_time = time1, 
                                                            to_time = time2, 
                                                            created_at=datetime.now(settings.tz_NY), 
                                                            status = 1)
                                    db.add(create_new_time)
                                    db.commit()
                                    mes="success"
                            else:
                                return JSONResponse(content=msg)

                        else:
                            error = 1
                            msg = "Operation is wrong"
                    else:
                        error = 1
                        msg = "Authcode is wrong"
                else:
                    error = 1
                    msg = "Device id is wrong"
        else:
            error = 1
            msg = f"data field is missing the lenth of ta received message: {len(data)} "
        
        
    return JSONResponse(content=msg)


@router.get("/refresh_data/{location_id}")
async def refresh_data(db: Session = Depends(deps.get_db), *,location_id:int,
    current_user: User = Depends(deps.get_current_user)):
    location=db.query(Location).filter(Location.id==location_id,Location.status==1)
    if current_user.user_type==2:
        check_location=db.query(UserLocationMapping).filter(UserLocationMapping.user_id==current_user.id,UserLocationMapping.location_id==location_id,UserLocationMapping.status==1).first()
        if check_location:
            location=location
        else:
            raise HTTPException(
                status_code=404,
                detail="Location not found",
            )

    location=location.first()
    topic='senddevicedata'
    device_id=location.location_code
    message=f"f0={topic},f1={location.location_code}"
    auth_code=hashlib.sha1((str(message)+settings.SALT_KEY).encode()).hexdigest()

    response=deps.send_device_request(topic,auth_code,device_id,message)
    msg = response
    return JSONResponse(content=msg)



@router.get("/refresh_device_setting/{location_id}")
async def refresh_device_setting(db: Session = Depends(deps.get_db), *,location_id:int,
    current_user: User = Depends(deps.get_current_user)):
    location=db.query(Location).filter(Location.id==location_id,Location.status==1)
    if current_user.user_type==2:
        check_location=db.query(UserLocationMapping).filter(UserLocationMapping.user_id==current_user.id,UserLocationMapping.location_id==location_id,UserLocationMapping.status==1).first()
        if check_location:
            location=location
        else:
            raise HTTPException(
                status_code=404,
                detail="Location not found",
            )
    location=location.first()
    topic='senddevicesetting'
    device_id=location.location_code
    message=f"f0={topic},f1={location.location_code}"
    auth_code=hashlib.sha1((str(message)+settings.SALT_KEY).encode()).hexdigest()

    response=deps.send_device_request(topic,auth_code,device_id,message)
    msg = response
    return JSONResponse(content=msg)


      
@router.get("/mqtt_call_log")
async def mqtt_call_log(*,db:Session=Depends(deps.get_db),request: Request):
    api= str(request.url)
    ip=request.client.host

    today = datetime.utcnow().date()
    from_date=today.strftime("%Y-%m-%d 00:00:00")
    to_date=today.strftime("%Y-%m-%d 23:59:59")
   
    call_method=request.method

    if call_method=="GET":
        data=request.query_params
    else:
        data=await request.form()
    print({"log_data":data})
    sender=data.get("sender") if data.get("sender") and data.get("sender") != None else None
    receiver=data.get("receiver") if data.get("receiver") and data.get("receiver") != None else None
    device_id=data.get("device_id") if data.get("device_id") and data.get("device_id") != None else None
    topic=data.get("received_topic") if data.get("received_topic") and data.get("received_topic") != None else None
    message=data.get("received_msg") if data.get("received_msg") and data.get("received_msg") != None else None

    api_response=''

    mqtt_log=MqttLog(api=api,call_method=call_method,sender=sender,topic=topic,message=message,reciver=receiver,device_id=device_id,params=data,ip=ip,status=1,api_response=api_response,datetime=datetime.now(settings.tz_NY))
    db.add(mqtt_log)
    db.commit()

    return "Success"
from datetime import timedelta
import datetime as dt

@router.post("/mqtt_call_log1")
async def mqtt_call_log():
    # pump_off_time=("2021-08-19 10:00:00").strptime("%Y-%m-%d %H:%M:%S")
    # t2 = dt.datetime.strptime(dt.datetime.strftime(pump_off_time, "%H:%M:%S"),"%H:%M:%S")
    # return t2
    # h=("00:15:57")
    # delta = timedelta(hours=int(h.split(':')[0]), minutes=int(h.split(':')[1]), seconds=int(h.split(':')[2]))
    # minutes = delta.total_seconds()/60
    # capacity=float(format(float("30")*float(minutes), ".3f"))/60

    # return capacity
    dict_value="11111111,22222222,12121212"
    time_values=str(dict_value).split(",")
    for row in time_values:
        t1=[]
        t2=[]
        i=0
        time_slot=list(str(row))
        for r in time_slot:
            i+=1
            if i<=4:
                t1.append(r)
                if i==2:
                    t1.append(':')
                if i==4:
                    t1.append(':00')
            if i>=5:
                t2.append(r)
                if i==6:
                    t2.append(':')
                if i==8:
                    t2.append(':00')
        time1 = ''.join(map(str, t1))
        time2 = ''.join(map(str, t2))

        print(time1,time2)

@router.post("/time_test")
async def time_test():
    t1=datetime.strptime("2021-11-28 00:01:10", '%Y-%m-%d %H:%M:%S')
    t2=datetime.now(settings.tz_NY)
    duration=datetime.now(settings.tz_NY).timestamp() - t1.timestamp()
    return duration



