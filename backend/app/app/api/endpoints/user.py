from typing import Any, List,Optional
from sqlalchemy import Boolean, Column, Integer, String, DateTime,text,or_,and_, desc, asc
from fastapi import APIRouter, Body, Depends, HTTPException,Form,File,UploadFile,Response
from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session,load_only
from app.utils import create_reset_key,check_phone,digit_check, profile_image_upload, common_date
from app.models import User,Location,UserLocationMapping
from app.schemas import ListUser,UserCreate,ViewByUser,ListUserDrop
from app import crud, models, schemas
from app.api import deps
from app.core.config import settings
from datetime import datetime
from fastapi_pagination import Page, add_pagination, paginate
from app.core.config import settings, Page as Bpage


router = APIRouter()

# list customer, response_model = Page[ListUser]
@router.post("/list_user", response_model = Bpage[ListUser]) 
async def list_user(db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user), 
    username :Optional[str] = Form(None),mobile_no :Optional[str] = Form(None),
    status:Optional[int]=Form(None),order_by:Optional[str]=Form(None), order:Optional[str]=Form(None)):
    if current_user:
        if current_user.user_type==1:
            user=db.query(User).filter(User.status != -1, User.user_type != 1)
        if current_user.user_type==2:
            mapping=db.query(UserLocationMapping.location_id).filter(UserLocationMapping.status == 1,UserLocationMapping.user_id==current_user.id)
            r=[]
            for row in mapping:
                r.append(row[0])
            # return r
            user=db.query(User).outerjoin(UserLocationMapping).filter(UserLocationMapping.location_id.in_(r),UserLocationMapping.status != -1)
      
        data=[]
        if username:  
            user=user.filter(models.User.username.like("%"+username+"%"))
            
        if mobile_no:    
            user=user.filter(models.User.mobile_no.like("%"+mobile_no+"%"))

        if status == 0 or status == 1:
            user=user.filter(models.User.status == status)

        if order_by:

            if order == "desc":
                user = user.order_by(desc(getattr(User, order_by)))
            else:
                user = user.order_by(asc(getattr(User, order_by)))


        else:
            user = user.order_by(User.id.desc())
        
        
        user=user.all()

        for row in user:
            values = {"user_id": row.id,"username": row.username if row.username else "", "mobile_no": row.mobile_no if row.mobile_no else None,"user_type": row.user_type if row.user_type else None, "status": row.status if row.status else 0 }
            
            data.append(values)
        
        reply=paginate(data)

    else:
        raise HTTPException(
                status_code=400,
                detail="Invalid request",
            )

    return reply

@router.post("/list_user_dropdown", response_model = List[ListUserDrop]) 
async def list_user_dropdown(db: Session = Depends(deps.get_db),current_user: User = Depends(deps.get_current_user)):

    if current_user:
        if current_user.user_type==1:
            user=db.query(User).filter(User.status != -1, User.user_type != 1)
        if current_user.user_type==2:
            mapping=db.query(UserLocationMapping.location_id).filter(UserLocationMapping.status == 1,UserLocationMapping.user_id==current_user.id)
            r=[]
            for row in mapping:
                r.append(row[0])
            # return r
            user=db.query(User).outerjoin(UserLocationMapping).filter(UserLocationMapping.location_id.in_(r),UserLocationMapping.status != -1)
      
      
        data=[]
        user = user.order_by(User.id.desc())
    
        
        
        user=user.all()

        for row in user:
            values = {"user_id": row.id,"username": row.username if row.username else ""}
            
            data.append(values)
        
        reply=data

    else:
        raise HTTPException(
                status_code=400,
                detail="Invalid request",
            )

    return reply

# create user  
@router.post("/create_user", response_model=ListUser)
async def create_user(
    
    db: Session = Depends(deps.get_db), *,
    current_user: User = Depends(deps.get_current_user),
    user_in: schemas.UserCreate = None,

) -> Any:
    """
    Create new User.
    """    
    if current_user.user_type == 1:

        check_user_mail = crud.user.get_by_email(db, email=user_in.email)

        check_mobile = crud.user.get_by_mobile(db, mobile_no=user_in.mobile_no)
        
        
        if check_user_mail:
            raise HTTPException(
                status_code=400,
                detail="Email already exists",
            )

        elif check_mobile:
            raise HTTPException(
                status_code=400,
                detail="Mobile No already exists",
            )

        else:
            user = jsonable_encoder(user_in)

            user = crud.user.create(db, obj_in=user)
                                                                                                                         

            data = {"user_id": user.id,"username": user.username if user.username else "","mobile_no": user.mobile_no if user.mobile_no else None,"user_type": user.user_type if user.user_type else None, "status": user.status if user.status else None }
        
        return data
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )



# create user  
@router.post("/signup", response_model=ListUser)
async def signup(
    
    db: Session = Depends(deps.get_db), *,
    user_in: schemas.UserCreate = None,

) -> Any:
    """
    Create new User.
    """    

    check_user_mail = crud.user.get_by_email(db, email=user_in.email)

    check_mobile = crud.user.get_by_mobile(db, mobile_no=user_in.mobile_no)
    
    
    if check_user_mail:
        raise HTTPException(
            status_code=400,
            detail="Email already exists",
        )

    elif check_mobile:
        raise HTTPException(
            status_code=400,
            detail="Mobile No already exists",
        )

    else:
        user = jsonable_encoder(user_in)

        user = crud.user.create(db, obj_in=user)
                                                                                                                        

        data = {"user_id": user.id,"username": user.username if user.username else "","mobile_no": user.mobile_no if user.mobile_no else None,"user_type": user.user_type if user.user_type else None, "status": user.status if user.status else None }
    
    return data

#View Customer, response_model=ViewByUser
@router.get("/view_user/{user_id}", response_model=ViewByUser)
def view_user(*,db: Session = Depends(deps.get_db),user_id: int,
                current_user: models.User = Depends(deps.get_current_user)):
  
    user = crud.user.get(db=db, id=user_id)

    if user:
        data={}
        location=[]
        data.update({"user_id": user.id,
                        "username": user.username if user.username else "",
                        "mobile_no": user.mobile_no if user.mobile_no else 0,
                        "email": user.email if user.email else "",
                        "user_type": user.user_type if user.user_type else 0,
                        "created_at":common_date(user.created_at) if user.created_at else "",
                        "status": user.status if user.status else 0 })
        mapping=db.query(UserLocationMapping).filter(UserLocationMapping.user_id==user.id,UserLocationMapping.status!=-1).all()
        if mapping:
            for row in mapping:
                location.append({"location_id": row.location.id if row.location.id else "",
                            "mapping_id":row.id if row.id else 0,

                            "access_type":row.access_type if row.access_type else 0,

                            "location_code": row.location.location_code if row.location.location_code else "",
                            "name": row.location.name if row.location.name else '',
                            "time_frequency": str(row.location.time_frequency) if row.location.time_frequency else '',
                            "tank_type": row.location.tank_type if row.location.tank_type else '',
                            "radius": row.location.radius if row.location.radius else 0,
                            "height": row.location.height if row.location.height else 0,
                            "width": row.location.width if row.location.width else 0,
                            "length": row.location.length if row.location.length else 0,
                            "created_at": common_date(row.location.created_at) if row.location.created_at else '',
                            "updated_at": common_date(row.location.updated_at) if row.location.updated_at else '',
                            "updated_by": row.location.updated_by if row.location.updated_by else 0,
                            "status": row.location.status if row.location.status else 0})
            data.update({"Location":location})
    else:
        raise HTTPException(status_code=404, detail="User not found")

    return data


# update user , response_model = ListUser

@router.put("/update_user/{user_id}", response_model = ListUser)
async def update_user(
    *,
    db: Session = Depends(deps.get_db),
    user_id: int,
    user_in: schemas.UserUpdate,
    current_user: User = Depends(deps.get_current_user),
) -> Any:
    """
    Update customer.
    """
    
    if current_user.user_type == 1 or current_user.user_type==2:
        user = crud.user.get(db, id=user_id)
        
        if not user:
            raise HTTPException(
                status_code=404,
                detail="The user does not exist",
            )

        check_user_mail = crud.user.get_by_email_update(db, email=user_in.email, user_id=user.id)

        check_mobile = crud.user.get_by_mobile_update(db, mobile_no=user_in.mobile_no, user_id=user.id)
        
        
        if check_user_mail:
            raise HTTPException(
                status_code=400,
                detail="Email already exists",
            )

        elif check_mobile:
            raise HTTPException(
                status_code=400,
                detail="Mobile No already exists",
            )
        else:
                     
            user_data = jsonable_encoder(user_in)
    
            user = crud.user.update(db, db_obj=user, obj_in=user_data)
    
            data = {"user_id": user.id,"username": user.username if user.username else "","mobile_no": user.mobile_no if user.mobile_no else None,"user_type":user.user_type if user.user_type else None , "status": user.status if user.status else 0 }
    
    else:
        raise HTTPException(
                status_code=400,
                detail="Invalid request")

   
    return data

#Change status
@router.post("/change_status")
def change_status(
        *,
        db: Session = Depends(deps.get_db),
        user_id: int,
        status:int,
        current_user: models.User = Depends(deps.get_current_user)):
    
    if current_user.user_type != 3:
        user = crud.user.get(db, id=user_id)        
        if user:
            if (status==1 or status==0):
                check_user= crud.user.change_status(db=db, id=user_id,status=status)
                user=db.query(User).filter(User.id==user.id).update({"status":status})
                # mapping=db.query(UserLocationMapping).filter(UserLocationMapping.user_id==user.id,UserLocationMapping.status!=-1).update({"status":status})

                db.commit()
                
                reply="Status changed succesfully"
            else:
                raise HTTPException(status_code=400, detail="Invalid request")
        else:
            raise HTTPException(status_code=404, detail="User not found")
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    
    return reply


# delete user
@router.delete("/delete_user/{user_id}")
def delete_user(
        *,
        db: Session = Depends(deps.get_db),
        user_id: int,
        current_user: models.User = Depends(deps.get_current_user)):
    
    if current_user.user_type != 3: 
        user = crud.user.get(db, id=user_id)    
        if user:
            user=db.query(User).filter(User.id==user_id).update({"status":-1})
            # mapping=db.query(UserLocationMapping).filter(UserLocationMapping.user_id==user.id).update({"status":-1})
            db.commit()
            reply="Deleted succesfully"
        else:
            raise HTTPException(status_code=404, detail="User not found")
    else:
        raise HTTPException(
            status_code=400,
            detail="Invalid request",
        )
    
    return reply


add_pagination(router)