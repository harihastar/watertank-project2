import random
import hashlib
import time
from fastapi.responses import HTMLResponse, RedirectResponse, JSONResponse

import logging
from datetime import datetime, timedelta
from fastapi import HTTPException
from pathlib import Path
from typing import Any, Dict, Optional
from pydantic import errors
from sqlalchemy import or_
import datetime
import emails
from emails.template import JinjaTemplate
from jose import jwt
from app import models
import math
import string
from app.core.config import settings
from pathlib import Path
from app.models import User
import os
import sys
import smtplib
from datetime import datetime, time, date
import time
# from app.schemas import *

from fastapi_mail import FastMail, MessageSchema, ConnectionConfig
import asyncio

# For firebase push notification
from pyfcm import FCMNotification
import struct


def check_phone(db, phone):
    user = db.query(models.User).filter(or_(models.User.primary_phone ==
                                            phone, models.User.secondary_phone == phone)).scalar()
    # return user
    token_user = db.query(models.ApiToken).filter_by(user_id=user.id).scalar()
    if user:
        reply = {"status": 0, "msg": "the user already exist"}
    elif not user:
        reply = {'status': 0, 'msg': 'Invalid Username or Password'}

    elif user.password != password:
        reply = {'status': 0, 'msg': 'Username  or Password'}

    elif user.status == 0:
        reply = {
            'status': 0, 'msg': 'Your account is currently inactive, Please contact administrator'}


def cal_tot_hrs(timelist):
    timeList = timelist
    totalSecs = 0
    tm=0
    for tm in timeList:
        tm+=float(tm)
    return tm
    #     timeParts = [int(s) for s in tm.split(':')]
    #     totalSecs += (timeParts[0] * 60 + timeParts[1]) * 60 + timeParts[2]
    # totalSecs, sec = divmod(totalSecs, 60)
    # hr, min = divmod(totalSecs, 60)
    # return("%d:%02d:%02d" % (hr, min, sec))


def create_reset_key(db):
    N = 15
    res = ''.join(random.choices(string.ascii_lowercase +
                                 string.digits, k=N))

    return (str(res))


def check_unique_id(db, unique_id):
    unique_user_id = 0
    unique_id = unique_id.strip()
    unique_id = db.query(models.Cart).filter(
        models.Cart.unique_id == unique_id).first()

    if unique_id:
        unique_user_id = unique_id.id
    return unique_user_id


def get_pagination(row_count=0, current_page_no = 1, default_page_size=10):
    current_page_no = current_page_no if current_page_no >= 1 else 1

    total_pages = math.ceil(row_count / default_page_size)

    if current_page_no > total_pages:
        current_page_no = total_pages
    
    limit =  current_page_no * default_page_size
    offset = limit - default_page_size

    if limit > row_count:
        limit = offset + (row_count % default_page_size)
    
    limit = limit - offset

    if offset < 0:
        offset = 0
    
    return [total_pages, offset, limit]


def isInteger(data):
    try:
        if data.isdigit():
            data = int(data)
        else:
            data = float(data)
    except:
        return False
    return True



def get_sec(upload_time):
    upload_time = time(upload_time)
    h, m, s = upload_time.split(':')
    print(int(datetime.timedelta(hours=int(h),
          minutes=int(m), seconds=int(s)).total_seconds()))


def common_date(date, without_time=None):

    datetime = date.strftime("%b %d,%Y %I:%M:%S %p")

    if without_time == 1:
        datetime = date.strftime("%d-%m-%Y")

    return datetime
def common_date_only(date, without_time=None):

    datetime = date.strftime("%d-%m-%y")

    if without_time == 1:
        datetime = date.strftime("%d-%m-%y")

    return datetime
def common_time_only(date, without_time=None):

    datetime = date.strftime("%H:%M:%S")

    return datetime

def digit_check(phone):
    if len(str(phone)) != 10:
        return False
    
    else:
        return phone


def digit_check2(phone):
    if len(str(phone)) == 10 or len(str(phone)) == 13:
        return phone
    else:
        return False

    


async def send_mail(from_mail, to_mail, subject, message):
    conf = ConnectionConfig(
        MAIL_USERNAME="priyapriya2457@gmail.com",  # "om@themaestro.in",
        MAIL_PASSWORD="",  # "cxbfeymklorfetji",
        MAIL_FROM="priyapriya2457@gmail.com",  # from_mail,
        MAIL_PORT=587,
        MAIL_SERVER="smtp.gmail.com",  # "smtp.gmail.com",
        MAIL_FROM_NAME="Water level Montitoring",  # from_mail,
        MAIL_TLS=True,
        MAIL_SSL=False,
        USE_CREDENTIALS=True
    )
    message = MessageSchema(
        subject=subject,
        recipients=[to_mail],
        body=message,
    )

    fm = FastMail(conf)
    await fm.send_message(message)
    return ({"msg": "Email has been sent"})



def send_push_notification(db, user_ids, message):
    # print(user_ids)
    android_ids = []
    ios_ids = []

    usr = models.User
    get_users = db.query(usr.device_type, usr.push_id).filter(
        usr.id.in_(user_ids), usr.status == 1).all()

    # print(get_users)

    for x in get_users:
        if x.device_type == 1:
            android_ids.append(x.push_id)
        elif x.device_type == 2:
            ios_ids.append(x.push_id)

    # print(android_ids)

    push_service = FCMNotification(
        api_key="AAAAHL1UNd8:APA91bE6sLocQUJ5XXHeRIGVrpnWTB3FhHZj_1LhRWkziMAoT3pKpl24HKm1NqUjrbOWBEUKwk5MI5N6Y9KSizRrpZXVF4eaSOKqIxUdZ8EPoK3gC5CqGuUGKF7R8iB8ekW8S3PJhAnH")

    registration_ids = android_ids
    message_title = message["msg_title"]
    message_body = message["msg_body"]
    result = push_service.notify_multiple_devices(
        registration_ids=registration_ids, message_title=message_title, message_body=message_body)
    # print(result) 
    return True

def profile_image_upload(profile:Any, folder_name:str):
        base_dir = settings.BASE_UPLOAD_FOLDER
        

        try:
            os.makedirs(base_dir, mode=0o777, exist_ok=True)
        except OSError as e:
            sys.exit("Can't create {dir}: {err}".format(dir=base_dir, err=e))

        output_dir = f"{base_dir}/{folder_name}"
        try:
            os.makedirs(output_dir, mode=0o777, exist_ok=True)
        except OSError as e:
            sys.exit("Can't create {dir}: {err}".format(dir=output_dir, err=e))

        
        
        filename, file_extension = os.path.splitext(profile.filename)
        
        original_path = f"{output_dir}user_image{1}_{random.randint(0, 9999)}{file_extension}"
        if profile != None and os.path.exists(f"{original_path}"):
            os.remove(f"{original_path}")
        with open(f"{original_path}", 'wb') as f:
            f.write(profile)

        return original_path


def is_time(stime):
    # time = re.search("^([01]?[0-9]|2[0-3]):[0-5][0-9]", time)
    # print(time)

    try:
        time.strptime(str(stime), '%H:%M:%S')
        return stime
    except ValueError:
        return False

def get_timestamp(time):
    get_time = is_time(time)

    if get_time:
        if type(time) == str:
            get_time = datetime.strptime(time, '%H:%M:%S').time()
        current_timestamp = datetime.combine(date.today(), get_time)
        reply = current_timestamp.timestamp()

    else:
        raise HTTPException(
                            status_code=400,
                            detail="Time format is wrong",
                        )
    return reply

def set_time_format(time):
    if len(str(time)) == 6:
        date= [time[j: j + 2] for j in range(0, len(time), 2)]
        time_format = f"{date[0]}:{date[1]}:{date[2]}"
        return time_format
        
    else:
        return False


def isTimeFormat(input):
    try:
        data=datetime.strptime(input, '%H:%M:%S').time()
        return data
    except ValueError:
        return False

def check_pump_running(data):
    if len(str(data))==6:
        if isInteger(data):
            date= [data[j: j + 2] for j in range(0, len(data), 2)]
            data = f"{date[0]}:{date[1]}:{date[2]}"
            time_data=isTimeFormat(data)
            if time_data:
                return([0,"Success",time_data])
            else:
                return([1,"Time format is wrong",data])
        else:
            return([1,"Data is not a valid integer",data])
    else:
        return([1,"Length of the data is wrong",data])

def hex_to_float(val):
    # res=struct.unpack('!f', bytes.fromhex(val))[0]
    res=struct.unpack('!f', bytes.fromhex(f'{val}'))[0]
    return res

def hex_to_double(val):
    res=struct.unpack('d', val.decode("hex"))
    return res


def hex_to_date(date_str):
    print(date_str)
    res = "{0:08b}".format(int(date_str, 16))
    lt=list(res)
    if len(lt)>25:
        print(lt)
        year_bits=int(("".join(lt[0:5:1])),2)+22
        month_bits=int(("".join(lt[5:9:1])),2)
        date_bites=int(("".join(lt[9:14:1])),2)
        hour_bits=int(("".join(lt[14:19:1])),2)
        minute_bits=int(("".join(lt[19:25:1])),2)
        seconds_bits=int(("".join(lt[25::1])),2)
        try:
            dat_str=datetime.strptime(f"{year_bits}-{month_bits}-{date_bites} {hour_bits}:{minute_bits}:{seconds_bits}","%Y-%m-%d %H:%M:%S")
            return (dat_str,None)
        except:
            return (None,f"date str is error{res}")
    else:
        return (None,f"date str is error{res}")

def check_metric(id,lt):
    metric_flow_rate=0
    flow_rate=0
    sensor_flow_rate=0
    metric_pressure=0
    pressure=0
    sensor_pressure=0
    metric_level=0
    level=0
    sensor_level=0
    metric_dc_voltage=0
    dc_voltage=0
    sensor_dc_voltage=0
    metric_ac_voltage=0
    ac_voltage=0
    sensor_ac_voltage=0
    metric_csq=0
    csq=0
    sensor_csq=0
    metric_gsm=0
    gsm=0
    sensor_gsm=0
    metric_gprs=0
    gprs=0
    sensor_gprs=0
    metric_data_transmit=0
    data_transmit=0
    sensor_data_transmit=0
    metric_total_flow=0
    total_flow=0
    sensor_total_flow=0
    metric_volume=0
    volume=0
    sensor_volume=0
    metric_quantity=0
    quantity=0
    sensor_quantity=0
    
    if "0002-m" in lt:
        metric_flow_rate="0002"
        val=lt.index("0002-m")
        flow_rate=hex_to_float(str(lt[val+1]))

        if id != "11":
            sensor_flow_rate=lt[val-1]

        # else:
        #     sensor_flow_rate=0
    # else:
    #     metric_flow_rate=0
    #     flow_rate=0
    #     sensor_flow_rate=0

    if "0003-m" in lt:
        metric_pressure="0003"
        val=lt.index("0003-m")
        pressure=hex_to_float(str(lt[val+1]))

        if id != "11":
            sensor_pressure=lt[val-1]
        # else:
        #     sensor_pressure=0
    # else:
    #     metric_pressure=0
    #     pressure=0
    #     sensor_pressure=0
    if "0007-m" in lt:
        metric_level="0007"
        val=lt.index("0007-m")
        level=hex_to_float(str(lt[val+1]))

        if id != "11":
            sensor_level=lt[val-1]
    #     else:
    #         sensor_level=0
    # else:
    #     metric_level=0
    #     level=0
    #     sensor_level=0

    if "000e-m" in lt:
        metric_dc_voltage="000e"
        val=lt.index("000e-m")
        dc_voltage=hex_to_float(str(lt[val+1]))

        if id != "11":
            sensor_dc_voltage=lt[val-1]
    #     else:
    #         sensor_dc_voltage=0
    # else:
    #     metric_dc_voltage=0
    #     dc_voltage=0
    #     sensor_dc_voltage=0
            
    if "000f-m" in lt:
        metric_ac_voltage="000f"
        val=lt.index("000f-m")
        print(str(lt[val+1]))
        ac_voltage=hex_to_float(str(lt[val+1]))

        if id != "11":
            sensor_ac_voltage=lt[val-1]
    #     else:
    #         sensor_ac_voltage=0
    # else:
    #     metric_ac_voltage=0
    #     ac_voltage=0
    #     sensor_ac_voltage=0
        
    if "0011-m" in lt:
        metric_csq="0011"
        val=lt.index("0011-m")
        csq=int(lt[val+1],16)

        if id != "11":
            sensor_csq=lt[val-1]
    #     else:
    #         sensor_csq=0
    # else:
    #     metric_csq=0
    #     csq=0
    #     sensor_csq=0
           
    if "0012-m" in lt:
        metric_gsm="0012"
        val=lt.index("0012-m")
        gsm=int(lt[val+1],16)

        if id != "11":
            sensor_gsm=lt[val-1]
    #     else:
    #         sensor_gsm=0
    # else:
    #     metric_gsm=0
    #     gsm=0
    #     sensor_gsm=0
        
    if "0013-m" in lt:
        metric_gprs="0013"
        val=lt.index("0013-m")
        gprs=int(lt[val+1],16)

        if id != "11":
            sensor_gprs=lt[val-1]
    #     else:
    #         sensor_gprs=0
    # else:
    #     metric_gprs=0
    #     gprs=0
    #     sensor_gprs=0
        

    if "0014-m" in lt:
        metric_data_transmit="0014"
        val=lt.index("0014-m")
        data_transmit=int(lt[val+1],16)

        if id != "11":
            sensor_data_transmit=lt[val-1]
    #     else:
    #         sensor_data_transmit=0
    # else:
    #     metric_data_transmit=0
    #     data_transmit=0
    #     sensor_data_transmit=0
            
    if "0017-m" in lt:
        metric_flow_rate="0017"
        val=lt.index("0017-m")
        flow_rate=hex_to_float(str(lt[val+1]))

        if id != "11":
            sensor_flow_rate=lt[val-1]
    #     else:
    #         sensor_flow_rate=0
    # else:
    #     metric_flow_rate=0
    #     flow_rate=0
    #     sensor_flow_rate=0
        
    if "0018-m" in lt:
        metric_total_flow="0018"
        val=lt.index("0018-m")
        total_flow=hex_to_double(lt[val+1])

        if id != "11":
            sensor_total_flow=lt[val-1]
    #     else:
    #         sensor_total_flow=0
    # else:
    #     metric_total_flow=0
    #     total_flow=0
    #     sensor_total_flow=0
        
    if "001e-m" in lt:
        metric_volume="001e"
        val=lt.index("001e-m")
        volume=hex_to_float(str(lt[val+1]))

        if id != "11":
            sensor_volume=lt[val-1]
        # else:
    #         sensor_volume=0
    # else:
    #     metric_volume=0
    #     volume=0
    #     sensor_volume=0
    
    if "001a-m" in lt:
        metric_quantity="001a"
        val=lt.index("001a-m")
        quantity=int(lt[val+1],16)

        if id != "11":
            sensor_quantity=lt[val-1]
    #     else:
    #         sensor_quantity=0
    # else:
    #     metric_quantity=0
    #     quantity=0
    #     sensor_quantity=0
    
    data={"metric_flow_rate":metric_flow_rate, "flow_rate":flow_rate, "sensor_flow_rate":sensor_flow_rate,
        "metric_pressure":metric_pressure, "pressure":pressure, "sensor_pressure":sensor_pressure,
        "metric_level":metric_level, "level":level, "sensor_level":sensor_level, "metric_dc_voltage":metric_dc_voltage,
        "dc_voltage":dc_voltage, "sensor_dc_voltage":sensor_dc_voltage, "metric_ac_voltage":metric_ac_voltage,
        "ac_voltage":ac_voltage, "sensor_ac_voltage":sensor_ac_voltage, "metric_csq":metric_csq,
        "csq":csq, "sensor_csq":sensor_csq, "metric_gsm":metric_gsm, "gsm":gsm, "sensor_gsm":sensor_gsm,
        "metric_gprs":metric_gprs, "gprs":gprs, "sensor_gprs":sensor_gprs, "metric_data_transmit":metric_data_transmit,
        "data_transmit":data_transmit, "sensor_data_transmit":sensor_data_transmit,
        # "metric_flow_rate" 
        # flow_rate
        #  sensor_flow_rate 
        "metric_total_flow":metric_total_flow, "total_flow":total_flow, "sensor_total_flow":sensor_total_flow,
        "metric_volume":metric_volume, "volume":volume, "sensor_volume":sensor_volume, "metric_quantity":metric_quantity,
        "quantity":quantity, "sensor_quantity":sensor_quantity} 
    #print(data)
    return data



# def check_error_code(code):


