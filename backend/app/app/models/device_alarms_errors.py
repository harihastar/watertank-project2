from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,SmallInteger,ForeignKey,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER,TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL
from app.db.base_class import Base

class DeviceAlarmsErrors(Base):
    id=Column(Integer,primary_key=True)
    alarm_id_from=Column(BigInteger,default=None,comment="start limit")
    alarm_id_to=Column(BigInteger,default=None,comment="end limit")

    error_alarm=Column(String(225),default=None)
    description=Column(String(225),default=None)
    sms=Column(String(225),default=None)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->delete")