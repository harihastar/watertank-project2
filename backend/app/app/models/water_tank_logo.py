from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,SmallInteger,ForeignKey,Text
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime
from app.db.base_class import Base

class WaterTankLogo(Base):
    __tablename__="water_tank_logo"
    id=Column(Integer,primary_key=True)
    image=Column(String(250))
    notes=Column(String(50))
    created_at=Column(DateTime)
    status=Column(TINYINT,comment="0->inactive,1->active,-1->deleted")