from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text, Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL, BigInteger, Time,Date
from app.db.base_class import Base

class ReportPumpFailedToRun(Base):
    __tablename__ = 'report_pump_failed_to_run'
    id=Column(Integer,primary_key=True)
    location_data_id=Column(Integer,comment="location_data table reference")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    # pump_fail_date=Column(DateTime)
    pump_fail_from_time=Column(DateTime)
    pump_fail_to_time=Column(DateTime)
    duration_of_pump_fail=Column(BigInteger)
    failed_pump=Column(TINYINT,comment="1->pump1,2->pump2")
    water_level_in_meter=Column(DECIMAL(10,5))
    low_level=Column(DECIMAL(10,5))
    mode=Column(TINYINT,comment="0->auto,1->manual,2->server")

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="report_pump_failed_to_run")
    # location_data=relationship("LocationData",back_populates="report_pump_failed_to_run")




