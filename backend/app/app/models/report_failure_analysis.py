from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text, Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL, BigInteger, Time,Date
from app.db.base_class import Base

class ReportFailureAnalysis(Base):
    __tablename__ = 'report_failure_analysis'
    id=Column(Integer,primary_key=True)
    location_data_id=Column(Integer,comment="location_data table reference")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    # pump_fail_date=Column(Date)
    fail_from_time=Column(DateTime)
    fail_to_time=Column(DateTime)
    duration_of_fail=Column(BigInteger)
    pump1_fail=Column(TINYINT,comment="1->yes,0->no")
    pump2_fail=Column(TINYINT,comment="1->yes,0->no")
    valve_open_fail=Column(TINYINT,comment="1->yes,0->no")
    valve_close_fail=Column(TINYINT,comment="1->yes,0->no")
    battery_voltage_level=Column(TINYINT,comment="1->low,2->normal")
    pump_status=Column(TINYINT,comment="1->on,0->off")
    power_status=Column(TINYINT,comment="1->on,0->off")
    water_level_in_meter=Column(DECIMAL(10,5))
    battery_voltage=Column(DECIMAL(10,5))
    ac_power_voltage=Column(DECIMAL(10,5))
    mode=Column(TINYINT,comment="0->auto,1->manual,2->server")

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="report_failure_analysis")
    # location_data=relationship("LocationData",back_populates="report_failure_analysis")




