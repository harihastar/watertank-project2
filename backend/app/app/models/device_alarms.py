from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL,BigInteger, String
from app.db.base_class import Base

class DeviceAlarm(Base):
    __tablename__ = 'device_alarm'
    id=Column(Integer,primary_key=True)
    start=Column(Integer, comment="start of Device alarm and Errors")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    sequence_number=Column(Integer)
    date_time=Column(DateTime)
    no_of_alarm=Column(Integer)
    alarm_status=Column(Integer ,comment="1-> Alarm/Error Start Code ,0-> Alarm/Error End Code")
    code=Column(Integer ,comment="alarm/error code refer alarm/error list")
    alarm=Column(TINYINT)
    error=Column(TINYINT)

    description=Column(String(225))
    
    battery_status=Column(TINYINT,comment="0->normal,1->low,2->high")
    pump1_failure_status=Column(TINYINT,comment="1->fail,0->ok")
    pump2_failure_status=Column(TINYINT,comment="1->fail,0->ok")
    valve_open_fail=Column(TINYINT,comment="1->fail,0->ok")
    valve_close_fail=Column(TINYINT,comment="1->fail,0->ok")
    
    sensor_level=Column(String(225))
    metric_level=Column(String(225), comment="ref metric table")
    level=Column(DECIMAL(10,5))

    sensor_ac_voltage=Column(String(225))
    metric_ac_voltage=Column(String(225), comment="ref metric table")
    ac_voltage=Column(DECIMAL(10,5))
    
    sensor_battery_voltage=Column(String(225))
    metric_battery_voltage=Column(String(225), comment="ref metric table")
    battery_voltage=Column(DECIMAL(10,5))
    
    sensor_volume=Column(String(225))
    metric_volume=Column(String(225), comment="ref metric table")
    volume=Column(DECIMAL(15,5))
    
    sensor_flow_rate=Column(String(225))
    metric_flow_rate=Column(String(225), comment="ref metric table")
    pump_run_minutes=Column(BigInteger)
    crc=Column(String(225))

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="device_alarms")





