from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,SmallInteger,ForeignKey,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER,TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL
from app.db.base_class import Base

class MetricTable(Base):
    id=Column(Integer,primary_key=True)
    metric_code=Column(String(225),default=None,comment="16 bits")
    metric=Column(String(225),default=None)
    data_types=Column(String(225),default=None)
    units=Column(String(225),default=None)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->delete")


