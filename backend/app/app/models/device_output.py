from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL,BigInteger, String
from app.db.base_class import Base

class DeviceOutput(Base):
    __tablename__ = 'device_output'
    id=Column(Integer,primary_key=True)
    start=Column(Integer, comment="start of Device Status ")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    sequence_number=Column(Integer)
    date_time=Column(DateTime)
    no_of_alarm=Column(Integer)
    on_off_status=Column(Integer ,comment="1-> ON, 0 -> OFF")
    output_port=Column(Integer ,comment="1 - pump1,2 - Pump 2 ,3 - Valve")
    
    pump_status=Column(TINYINT,comment="0->off,1->on")
    valve_status=Column(TINYINT,comment="0->close,1->open")
    running_pump=Column(TINYINT,comment="0->all_off,1->pump1,2->pump2")
    
    sensor_level=Column(String(225))
    metric_level=Column(String(225), comment="ref metric table")
    level=Column(DECIMAL(10,5))

    sensor_ac_voltage=Column(String(225))
    metric_ac_voltage=Column(String(225), comment="ref metric table")
    ac_voltage=Column(DECIMAL(10,5))
    
    sensor_battery_voltage=Column(String(225))
    metric_battery_voltage=Column(String(225), comment="ref metric table")
    battery_voltage=Column(DECIMAL(10,5))
    
    sensor_volume=Column(String(225))
    metric_volume=Column(String(225), comment="ref metric table")
    volume=Column(DECIMAL(15,5))
    
    sensor_flow_rate=Column(String(225))
    metric_flow_rate=Column(String(225), comment="ref metric table")
    pump_run_minutes=Column(Integer)
    crc=Column(String(225))

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="device_output")





