from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,SmallInteger,ForeignKey,Text
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime

from sqlalchemy.sql.sqltypes import VARCHAR
from app.db.base_class import Base

class MqttLog(Base):
    __tablename__="mqtt_log"
    id=Column(Integer,primary_key=True)
    api=Column(String(2500),comment="url")
    call_method=Column(String(25))
    params=Column(LONGTEXT)
    ip=Column(String(25),comment="api call from which ip")
    sender=Column(VARCHAR(50))
    reciver=Column(VARCHAR(50))
    device_id=Column(VARCHAR(50),comment='location_code')
    topic=Column(VARCHAR(250))
    message=Column(VARCHAR(500))
    api_response=Column(LONGTEXT)
    datetime=Column(DateTime)
    status=Column(TINYINT,comment="0->inactive,1->active,-1->delete")