from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text, Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL, Time
from app.db.base_class import Base

class DeviceTimeSetting(Base):
    __tablename__ = 'device_time_setting'
    id=Column(Integer,primary_key=True)
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    from_time=Column(Time,comment="in hours")
    to_time=Column(Time,comment="in hours")
    
    created_at=Column(DateTime)    
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")
    location=relationship("Location",back_populates="device_time_setting")
