from decimal import Decimal
from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,ForeignKey, Text,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER,TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL
from app.db.base_class import Base

class DeviceSetting(Base):
    __tablename__ = 'device_setting'
    id=Column(Integer,primary_key=True)
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    
    pump1_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    pump2_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    allowed_running_hrs=Column(Time,comment="in hours")
    low_water_level=Column(DECIMAL(10,5), comment='level in meter')
    high_water_level=Column(DECIMAL(10,5), comment='level in meter')
    preset_water_level=Column(DECIMAL(10,5), comment='level in meter')
    
    lpp=Column(DECIMAL(10,5))
    amps1=Column(DECIMAL(10,5))
    amps2=Column(DECIMAL(10,5))
    
    
    created_at=Column(DateTime)    
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="device_setting")








    