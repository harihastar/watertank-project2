from decimal import Decimal
from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,ForeignKey, Text,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER,TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL
from app.db.base_class import Base

class LocationSetting(Base):
    __tablename__ = 'location_setting'
    id=Column(Integer,primary_key=True)
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    
    pump1_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    pump2_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    allowed_running_hrs=Column(Time,comment="in hours")
    low_water_level=Column(DECIMAL(10,5), comment='level in meter')
    high_water_level=Column(DECIMAL(10,5), comment='level in meter')
    preset_water_level=Column(DECIMAL(10,5), comment='level in meter')
    
    lpp=Column(DECIMAL(10,5))
    amps1=Column(DECIMAL(10,5))
    amps2=Column(DECIMAL(10,5))
    
    remote_contact_no=Column(BigInteger, comment='remote pump contact no.')
    contact_email=Column(String(50), comment='remote pump contact email')
    
    created_at=Column(DateTime)
    created_by=Column(Integer,comment="user table reference")
    last_updated_at=Column(DateTime)
    last_updated_by=Column(Integer, comment="user reference table")
    
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="location_setting")
    location_setting_log=relationship("LocationSettingLog",back_populates="location_setting")








    