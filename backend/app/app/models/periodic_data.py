from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL,BigInteger, String
from app.db.base_class import Base

class PeriodicData(Base):
    __tablename__ = 'periodic_data'
    id=Column(Integer,primary_key=True)
    start=Column(Integer, comment="Start of Periodic Data")
    bytes_to_follow=Column(Integer)
    sequence_number=Column(Integer)
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    date_time=Column(DateTime)
    level=Column(DECIMAL(10,5))
    ac_voltage=Column(DECIMAL(10,5))
    battery_voltage=Column(DECIMAL(10,5))
    volume=Column(DECIMAL(15,5))
    flow_rate=Column(Integer)
    total_flow=Column(Integer)
    crc=Column(String(225))

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="periodic_data")
