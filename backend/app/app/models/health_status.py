from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL,BigInteger, String
from app.db.base_class import Base

class HealthStatus(Base):
    __tablename__ = 'health_status'
    id=Column(Integer,primary_key=True)
    start=Column(Integer, comment=" Start of Health Record")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    date_time=Column(DateTime)
    metric_csq=Column(String(225), comment="ref metric table")
    csq=Column(DECIMAL(10,5))
    metric_gsm=Column(String(225), comment="ref metric table")
    gsm=Column(DECIMAL(10,5))
    metric_gprs=Column(String(225), comment="ref metric table")
    gprs=Column(DECIMAL(10,5))
    metric_data_transmit_time=Column(String(225), comment="ref metric table")
    data_transmit_time=Column(Integer)
    metric_dc_voltage=Column(String(225), comment="ref metric table")
    dc_voltage=Column(DECIMAL(10,5))
    metric_ac_voltage=Column(String(225), comment="ref metric table")
    ac_voltage=Column(DECIMAL(10,5))
    crc=Column(String(225))

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="health_status")


