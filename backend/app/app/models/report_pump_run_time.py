from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text, Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL, BigInteger, Time
from app.db.base_class import Base

class ReportPumpRunTime(Base):
    __tablename__ = 'report_pump_run_time'
    id=Column(Integer,primary_key=True)
    location_data_id=Column(Integer,comment="location_data table reference")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    pump_on_time=Column(DateTime)
    water_level_in_meter=Column(DECIMAL(10,5))
    water_volume_in_liter=Column(DECIMAL(15,5))
    pump_off_time=Column(DateTime)
    pump_off_water_in_level=Column(DECIMAL(10,5),comment="meter")
    pump_off_water_volume=Column(DECIMAL(15,5),comment="liter")
    pump_running_time=Column(BigInteger,comment="timestamp")
    running_pump=Column(TINYINT,comment="1->pump1,2->pump2")
    mode=Column(TINYINT,comment="0->auto,1->manual,2->server")

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="report_pump_run_time")
    # location_data=relationship("LocationData",back_populates="report_pump_run_time")




