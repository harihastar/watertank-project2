from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text, Time,BigInteger
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL, Time,Date
from app.db.base_class import Base

class ReportDistributedWater(Base):
    __tablename__ = 'report_distributed_water'
    id=Column(Integer,primary_key=True)
    location_data_id=Column(Integer,comment="location data table reference")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    running_pump=Column(TINYINT,comment="1->pump1,2->pump2")
    pump_on_time=Column(DateTime)
    water_level_in_meter=Column(DECIMAL(10,5))
    water_volume_in_liter=Column(DECIMAL(15,5))
    pump_off_time=Column(DateTime)
    pump_off_water_level_in_meter=Column(DECIMAL(10,5))
    pump_off_water_volume_in_liter=Column(DECIMAL(15,5))
    pump_running_hours=Column(BigInteger)
    water_volume_discharged_based_on_capacity=Column(BigInteger)
    mode=Column(TINYINT,comment="0->auto,1->manual,2->server")

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="report_distributed_water")
    # location_data=relationship("LocationData",back_populates="report_distributed_water")




