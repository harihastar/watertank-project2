from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,SmallInteger,ForeignKey,Text
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime
from app.db.base_class import Base

class WebhookCallHistory(Base):
    __tablename__="webhook_call_history"
    id=Column(Integer,primary_key=True)
    api=Column(String(2500),comment="url")
    call_method=Column(String(25))
    params=Column(LONGTEXT)
    ip=Column(String(25),comment="api call from which ip")
    datetime=Column(DateTime)
    api_response=Column(LONGTEXT)
    status=Column(TINYINT,comment="0->inactive,1->active,-1->delete")