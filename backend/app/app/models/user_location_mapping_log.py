from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,ForeignKey, Text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER,TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL
from app.db.base_class import Base

class UserLocationMappingLog(Base):
    __tablename__ = 'user_location_mapping_log'
    id=Column(Integer,primary_key=True)
    user_location_mapping_id=Column(Integer,ForeignKey("user_location_mapping.id"),comment="user_location_mapping table reference")
    old_location_id=Column(Integer,comment="location table reference")
    new_location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    
    old_user_id=Column(Integer,comment="user table reference")
    new_user_id=Column(Integer,ForeignKey("user.id"),comment="user table reference")
    
    old_access_type=Column(TINYINT,comment="1-super user,2-pump operator")
    new_access_type=Column(TINYINT,comment="1-super user,2-pump operator")
    
    created_by=Column(Integer,comment="user table reference")
    updated_by=Column(Integer,comment="user table reference")
    created_at=Column(DateTime)

    message = Column(String(255),comment="changes in user location mapping")
    
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="user_location_mapping_log")
    user=relationship("User",back_populates="user_location_mapping_log")
    user_location_mapping=relationship("UserLocationMapping",back_populates="user_location_mapping_log")