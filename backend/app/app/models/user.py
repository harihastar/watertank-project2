from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,SmallInteger,ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER,TINYINT
import datetime
from app.db.base_class import Base

class User(Base):
    id=Column(Integer,primary_key=True)
    user_type=Column(TINYINT,comment="1->Super Admin,2->Users")
    username=Column(String(50))
    mobile_no=Column(BigInteger)
    email=Column(String(50))
    password=Column(String(150))
    otp=Column(Integer)
    otp_created_at=Column(DateTime)
    otp_expired_at=Column(DateTime)
    reset_key=Column(String(150))
    otp_verified_status=Column(TINYINT, default=False, comment="0->not verified,1->verified")
    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")
    user_location_mapping=relationship("UserLocationMapping",back_populates="user")
    user_location_mapping_log=relationship("UserLocationMappingLog",back_populates="user") 