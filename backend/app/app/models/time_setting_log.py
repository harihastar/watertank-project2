from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text, Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL, Time
from app.db.base_class import Base

class TimeSettingLog(Base):
    __tablename__ = 'time_setting_log'
    id=Column(Integer,primary_key=True)
    time_setting_id=Column(Integer,ForeignKey("time_setting.id"),comment="time_setting table reference")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    
    old_from_time=Column(Time,comment="in hours")
    new_from_time=Column(Time,comment="in hours")
    
    old_to_time=Column(Time,comment="in hours")
    new_to_time=Column(Time,comment="in hours")
    
    created_by=Column(Integer,comment="user table reference")
    updated_by=Column(Integer,comment="user table reference")
    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="time_setting_log")
    time_setting=relationship("TimeSetting",back_populates="time_setting_log")




