from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL
from app.db.base_class import Base

class LocationDataTimeSettingLog(Base):
    __tablename__ = 'location_data_time_setting_log'
    id=Column(Integer,primary_key=True)
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    time_setting_id=Column(Integer,ForeignKey("time_setting.id"),comment="time setting table reference")
    location_data_id=Column(Integer,comment="location_data setting table reference")
    from_time=Column(Time,comment="in hours")
    to_time=Column(Time,comment="in hours")
    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->delete")

    location=relationship("Location",back_populates="location_data_time_setting_log")
    time_setting=relationship("TimeSetting",back_populates="location_data_time_setting_log")
    # location_data=relationship("LocationData",back_populates="location_data_time_setting_log")