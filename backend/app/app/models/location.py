from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,SmallInteger,ForeignKey,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER,TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL
from app.db.base_class import Base

class Location(Base):
    id=Column(Integer,primary_key=True)
    location_code=Column(String(20))
    name=Column(String(50))
    time_frequency=Column(Integer,comment="in hours")
    tank_type=Column(TINYINT,comment="1-cylindrical,2-cuboid")
    radius=Column(DECIMAL(15,5), comment="in meter ,required only for cylindrical")
    height=Column(DECIMAL(15,5),comment="in meter")
    width=Column(DECIMAL(15,5), comment="in meter ,required only for cuboid")
    length=Column(DECIMAL(15,5),comment="in meter,required only for cuboid")
    total_capacity=Column(DECIMAL(15,5),comment="in liter")
    scheme_name=Column(String(255))
    district=Column(String(255))
    block=Column(String(255))
    panchayat=Column(String(255))
    ear_marked_qty=Column(Integer,comment="in m3")
    field_1=Column(String(255))
    field_2=Column(String(255))
    field_3=Column(String(255))

    created_at=Column(DateTime)
    updated_at=Column(DateTime)
    updated_by=Column(Integer, comment="user reference table")
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->delete")

    location_data=relationship("LocationData",back_populates="location")
    location_setting=relationship("LocationSetting",back_populates="location")
    location_setting_log=relationship("LocationSettingLog",back_populates="location")
    time_setting=relationship("TimeSetting",back_populates="location")
    time_setting_log=relationship("TimeSettingLog",back_populates="location")
    user_location_mapping=relationship("UserLocationMapping",back_populates="location")
    user_location_mapping_log=relationship("UserLocationMappingLog",back_populates="location")
    location_data_time_setting_log=relationship("LocationDataTimeSettingLog",back_populates="location")
    report_pump_run_time=relationship("ReportPumpRunTime",back_populates="location")
    report_pump_failed_to_run=relationship("ReportPumpFailedToRun",back_populates="location")
    report_failure_analysis=relationship("ReportFailureAnalysis",back_populates="location")
    report_supplied_water=relationship("ReportSuppliedWater",back_populates="location")
    report_distributed_water=relationship("ReportDistributedWater",back_populates="location")
    device_setting=relationship("DeviceSetting",back_populates="location")
    device_time_setting=relationship("DeviceTimeSetting",back_populates="location")

    device_alarms=relationship("DeviceAlarm",back_populates="location")
    device_output=relationship("DeviceOutput",back_populates="location")
    health_status=relationship("HealthStatus",back_populates="location")
    network_status=relationship("NetworkStatus",back_populates="location")
    periodic_header=relationship("PeriodicHeader",back_populates="location")
    periodic_data=relationship("PeriodicData",back_populates="location")






    








    