from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL,BigInteger, String
from app.db.base_class import Base

class PeriodicHeader(Base):
    __tablename__ = 'periodic_header'
    id=Column(Integer,primary_key=True)
    start=Column(Integer, comment="Start of Periodic Header")
    bytes_to_follow=Column(Integer)
    sequence_number=Column(Integer)
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    metric_date=Column(String(225), comment="ref metric table")
    sensor_level=Column(String(225))
    metric_level=Column(String(225), comment="ref metric table")
    sensor_ac_voltage=Column(String(225))
    metric_ac_voltage=Column(String(225), comment="ref metric table")
    sensor_battery_voltage=Column(String(225))
    metric_battery_voltage=Column(String(225), comment="ref metric table")
    sensor_volume=Column(String(225))
    metric_volume=Column(String(225), comment="ref metric table")
    sensor_flow_rate=Column(String(225))
    metric_flow_rate=Column(String(225), comment="ref metric table")
    crc=Column(String(225))

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="periodic_header")

