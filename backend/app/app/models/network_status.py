from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text,Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL,BigInteger, String
from app.db.base_class import Base

class NetworkStatus(Base):
    __tablename__ = 'network_status'
    id=Column(Integer,primary_key=True)
    start=Column(Integer, comment="start of Device Status ")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    sequence_number=Column(Integer)
    date_time=Column(DateTime)
    mode=Column(TINYINT , comment="0 - Manual mode, 1 - Auto mode, 2 - Server mode")
    crc=Column(String(225))

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")

    location=relationship("Location",back_populates="network_status")
