from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,ForeignKey, Time
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER,TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL
from app.db.base_class import Base

class LocationSettingLog(Base):
    __tablename__ = 'location_setting_log'

    id=Column(Integer,primary_key=True)
    location_setting_id=Column(Integer,ForeignKey("location_setting.id"),comment="location_setting table reference")
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    
    old_pump1_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    new_pump1_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    
    old_pump2_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    new_pump2_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    
    old_allowed_running_hrs=Column(Time,comment="in hours")
    new_allowed_running_hrs=Column(Time,comment="in hours")
    
    old_low_water_level=Column(DECIMAL(10,5), comment='level in meter')
    new_low_water_level=Column(DECIMAL(10,5), comment='level in meter')
    
    old_high_water_level=Column(DECIMAL(10,5), comment='level in meter')
    new_high_water_level=Column(DECIMAL(10,5), comment='level in meter')
    
    old_preset_water_level=Column(DECIMAL(10,5), comment='level in meter')
    new_preset_water_level=Column(DECIMAL(10,5), comment='level in meter')
    
    old_lpp=Column(DECIMAL(10,5))
    new_lpp=Column(DECIMAL(10,5))
    
    old_amps1=Column(DECIMAL(10,5))
    new_amps1=Column(DECIMAL(10,5))
    
    old_amps2=Column(DECIMAL(10,5))
    new_amps2=Column(DECIMAL(10,5))
    
    old_remote_contact_no=Column(BigInteger, comment='remote pump contact no.')
    new_remote_contact_no=Column(BigInteger, comment='remote pump contact no.')
    
    old_contact_email=Column(String(50), comment='remote pump contact email')
    new_contact_email=Column(String(50), comment='remote pump contact email')
    
    created_by=Column(Integer,comment="user table reference")
    updated_by=Column(Integer,comment="user table reference")
    created_at=Column(DateTime)
    
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")
    location=relationship("Location",back_populates="location_setting_log")
    location_setting=relationship("LocationSetting",back_populates="location_setting_log")








    