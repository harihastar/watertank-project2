from typing import TYPE_CHECKING
from sqlalchemy import Boolean, Column, Integer, String, DateTime, BigInteger,ForeignKey, Text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import INTEGER,TINYINT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL
from app.db.base_class import Base

class UserLocationMapping(Base):
    __tablename__ = 'user_location_mapping'
    id=Column(Integer,primary_key=True)
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    user_id=Column(Integer,ForeignKey("user.id"),comment="user table reference")
    access_type=Column(TINYINT,comment="1-super user,2-pump operator")
    created_at=Column(DateTime)
    created_by=Column(Integer,comment="user table reference")
    last_updated_by=Column(Integer,comment="user table reference")
    last_updated_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->deleted")
    location=relationship("Location",back_populates="user_location_mapping")
    user=relationship("User",back_populates="user_location_mapping")
    user_location_mapping_log=relationship("UserLocationMappingLog",back_populates="user_location_mapping")
    

