from sqlalchemy import Column, Integer, DateTime,ForeignKey, Text,Time,String
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.mysql import TINYINT,LONGTEXT
import datetime
from sqlalchemy.sql.sqltypes import DECIMAL,BigInteger
from app.db.base_class import Base

class LocationData(Base):
    __tablename__ = 'location_data'
    id=Column(Integer,primary_key=True)
    location_id=Column(Integer,ForeignKey("location.id"),comment="location table reference")
    
    table_id=Column(TINYINT,comment="1->peiodic_header,2->periodic_data,3->device_output,4->health_status,5->device_alarm,6->network_status")
    table_data_id=Column(Integer,comment="table for table_id reference")
    
    power_voltage_l1=Column(DECIMAL(10,5),comment="l1 phase voltage")
    power_voltage_l2=Column(DECIMAL(10,5),comment="l2 phase voltage")
    power_voltage_l3=Column(DECIMAL(10,5),comment="l3 phase voltage")
    power_status=Column(TINYINT,comment="0->off,1->on")
    water_level_meter=Column(DECIMAL(10,5),comment="meter")
    water_volume_liter=Column(DECIMAL(15,5),comment="liter")
    pump_status=Column(TINYINT,comment="0->off,1->on")
    
    pump_on_time=Column(DateTime)
    pump_off_time=Column(DateTime)

    battery_voltage=Column(DECIMAL(10,5))
    valve_status=Column(TINYINT,comment="0->close,1->open")
    
    pump_running_hrs=Column(BigInteger, comment="pump running hours for the run")
    pump_running_hrs_day=Column(BigInteger, comment="cumulative for each day")
    
    running_pump=Column(TINYINT,comment="0->all_off,1->pump1,2->pump2")
    discharged_water_volume=Column(DECIMAL(15,5),comment="cumulative for each day")
    
    discharged_water_volume_in_day=Column(DECIMAL(15,5),comment="cumulative for a day")
    
    signal_strength=Column(Integer)
    pump1_failure_status=Column(TINYINT,comment="1->fail,0->ok")
    pump2_failure_status=Column(TINYINT,comment="1->fail,0->ok")
    valve_open_fail=Column(TINYINT,comment="1->fail,0->ok")
    valve_close_fail=Column(TINYINT,comment="1->fail,0->ok")
    mode=Column(TINYINT,comment="0->auto,1->manual,2->server")
    error_codes=Column(LONGTEXT)

    battery_status=Column(TINYINT,comment="0->normal,1->low,2->high")
    running_beyond_set_time=Column(TINYINT,comment="0->within set time,1->beyond set time")

# Location setting fields
    pump1_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    pump2_waterflow_per_min=Column(DECIMAL(10,5),comment="in liters")
    allowed_running_hrs=Column(Time,comment="in hours")
    low_water_level=Column(DECIMAL(10,5), comment='level in meter')
    high_water_level=Column(DECIMAL(10,5), comment='level in meter')
    preset_water_level=Column(DECIMAL(10,5), comment='level in meter')

    pump1_failure_alarm=Column(TINYINT,comment="0->no,1->yes") 
    pump2_failure_alarm=Column(TINYINT,comment="0->no,1->yes")
    valve_open_failure_alarm=Column(TINYINT,comment="0->no,1->yes")
    valve_close_failure_alarm=Column(TINYINT,comment="0->no,1->yes")
    battery_low_alarm=Column(TINYINT,comment="0->no,1->yes")
    pump_run_beyond_running_hours_alarm=Column(TINYINT,comment="0->no,1->yes")
    
    description=Column(String(225),comment="error or alarm description")

    created_at=Column(DateTime)
    status=Column(TINYINT, default=True, comment="0->inactive,1->active,-1->delete")

    location=relationship("Location",back_populates="location_data")
    # location_data_time_setting_log=relationship("LocationDataTimeSettingLog",back_populates="location_data")
    # report_pump_run_time=relationship("ReportPumpRunTime",back_populates="location_data")
    
    # report_pump_failed_to_run=relationship("ReportPumpFailedToRun",back_populates="location_data")
    # report_failure_analysis=relationship("ReportFailureAnalysis",back_populates="location_data")
    # report_supplied_water=relationship("ReportSuppliedWater",back_populates="location_data")
    # report_distributed_water=relationship("ReportDistributedWater",back_populates="location_data")





    




   
